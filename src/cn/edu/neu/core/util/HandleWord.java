package cn.edu.neu.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

public class HandleWord {
  //回车ACSII码
  private static final short ENTER_ASCII = 13;

  //空格ACSII码
  private static final short SPACE_ASCII = 32;

  //左圆括号ACSII码
  private static final short LEFTPARE_ASCII = 40;

  //右圆括号ACSII码
  private static final short RIGTHPARE_ASCII = 40;

  //垂直线ACSII码
  private static final short VERTICALLINE_ASCII = 124;

  //构造函数
  public HandleWord() throws Exception {
  }

  //读取word
  public XWPFDocument readWord(File fileservlet) throws IOException {
    InputStream is = new FileInputStream(fileservlet);
    XWPFDocument document = new XWPFDocument(is);
    is.close();
    return document;
  }

  //获取word中的所有段落
  public List<XWPFParagraph> getAllParagraph(XWPFDocument document) {
    List<XWPFParagraph> para = new ArrayList<XWPFParagraph>();
    para.addAll(document.getParagraphs());
    return para;
  }

  //获取段落中的内容
  public String getParaText(XWPFParagraph para) throws IOException {
    return para.getText();
  }

  //循环段落，提取单选题数据
  public List<List<Map<String, String>>> getQuestionDataSC(List<XWPFParagraph> paras) {
    List<Map<String, String>> question;
    Map<String, String> sc_d;
    List<List<Map<String, String>>> questionlist = new ArrayList<List<Map<String, String>>>();
    for (int i = 0; i < paras.size(); i++) {
      //存放选题
      question = new ArrayList<Map<String, String>>();
      sc_d = new HashMap<String, String>();
      String text = paras.get(i).getText();
      System.out.println(text);
      if (!text.equals(" ") & text != null) {
        //匹配第一个字符为（时
        if (text.indexOf("(") == 0) {
          //判断题型
          if (text.substring(4, 7).equals("单选题")) {

            sc_d.put("题型", text.substring(4, 7));
            //System.out.println("---" + text.substring(4, 7));

            sc_d.put("答案", text.substring(text.indexOf("答案") + 3, text.indexOf("分值") - 2));
            //System.out.println("答案" + "-----" + text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 2));

            sc_d.put("分值", text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));
            //System.out.println("分值" + "-----" + text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));

            //提取单选题选项
            paras.get(i + 1).getText();

            sc_d.put("题目", paras.get(i + 1).getText());

            //选项A
            sc_d.put("选项A", paras.get(i + 2).getText().substring(2));

            //选项B
            sc_d.put("选项B", paras.get(i + 3).getText().substring(2));

            //选项C
            sc_d.put("选项C", paras.get(i + 4).getText().substring(2));

            //选项D
            sc_d.put("选项D", paras.get(i + 5).getText().substring(2));

            //判断试题分类是否存在
            if (text.indexOf("试题分类") != -1) {
              sc_d.put("试题分类", text.substring(text.indexOf("试题分类") + 5, text.lastIndexOf(")")));

            }
            question.add(sc_d);
            System.out.println(question);
            questionlist.add(question);
            System.out.println(questionlist);
          }

          //          if (text.substring(4, 7).equals("多选题")) {
          //            subject.put("题型", text.substring(4, 7));
          //            question.add(subject);
          //            System.out.println("---" + text.substring(4, 7));
          //            answer.put("答案", text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 1));
          //            System.out.println("答案" + "-----" + text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 2));
          //            question.add(answer);
          //            score.put("分数", text.substring(text.indexOf("分数") + 3, text.indexOf("分数") + 4));
          //            System.out.println("分数" + "-----" + text.substring(text.indexOf("分数") + 3, text.indexOf("分数") + 4));
          //            question.add(score);
          //          }
          //
        }
        //        //提取填空题题目
        //        else if (text.indexOf("___") == 1) {
        //          System.out.println("填空题题目为" + text);
        //        }
        //
      }
    }
    System.out.println(questionlist.size());
    return questionlist;

  }

  //多选题
  public List<List<Map<String, String>>> getQuestionDataMC(List<XWPFParagraph> paras) {
    List<Map<String, String>> question;
    Map<String, String> mc_d;
    List<List<Map<String, String>>> questionlist = new ArrayList<List<Map<String, String>>>();
    for (int i = 0; i < paras.size(); i++) {
      question = new ArrayList<Map<String, String>>();
      mc_d = new HashMap<String, String>();
      String text = paras.get(i).getText();
      System.out.println(text);
      if (!text.equals(" ") & text != null) {
        //匹配第一个字符为(时
        if (text.indexOf("(") == 0) {
          //判断题型
          if (text.substring(4, 7).equals("多选题")) {
            mc_d.put("题型", text.substring(4, 7));
            //System.out.println("---" + text.substring(4, 7));

            mc_d.put("答案1", text.substring(text.indexOf("答案") + 3, text.indexOf("答案") + 4));
            //System.out.println("答案" + "-----" + text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 2));
            mc_d.put("答案2", text.substring(text.indexOf("答案") + 5, text.indexOf("答案") + 6));
            if (text.substring(text.indexOf("答案") + 6, text.indexOf("答案") + 7).equals(",")) {
              mc_d.put("答案3", text.substring(text.indexOf("答案") + 7, text.indexOf("答案") + 8));
              System.out.println(text.substring(text.indexOf("答案") + 7, text.indexOf("答案") + 8));
            }
            mc_d.put("分值", text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));
            //System.out.println("分值" + "-----" + text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));

            //提取单选题选项
            paras.get(i + 1).getText();

            mc_d.put("题目", paras.get(i + 1).getText());

            //选项A
            mc_d.put("选项A", paras.get(i + 2).getText().substring(2));

            //选项B
            mc_d.put("选项B", paras.get(i + 3).getText().substring(2));

            //选项C
            mc_d.put("选项C", paras.get(i + 4).getText().substring(2));

            //选项D
            mc_d.put("选项D", paras.get(i + 5).getText().substring(2));

            //判断选项E是否存在
            if (!paras.get(i + 6).getText().equals("")) {
              mc_d.put("选项E", paras.get(i + 6).getText());
              System.out.println("选项E-----" + paras.get(i + 6).getText().substring(1));
            }

            //判断试题分类是否存在
            if (text.indexOf("试题分类") != -1) {
              mc_d.put("试题分类", text.substring(text.indexOf("试题分类") + 5, text.lastIndexOf(")")));

            }
            question.add(mc_d);
            System.out.println(question);
            questionlist.add(question);
            System.out.println(questionlist);

          }
        }
      }
    }
    System.out.println(questionlist.size());
    return questionlist;
  }

  //填空题
  public List<List<Map<String, String>>> getQuestionDataFA(List<XWPFParagraph> paras) {
    List<Map<String, String>> question;
    Map<String, String> mc_d;
    List<List<Map<String, String>>> questionlist = new ArrayList<List<Map<String, String>>>();
    for (int i = 0; i < paras.size(); i++) {
      question = new ArrayList<Map<String, String>>();
      mc_d = new HashMap<String, String>();
      String text = paras.get(i).getText();
      System.out.println(text);
      if (!text.equals(" ") & text != null) {
        //匹配第一个字符为(时
        if (text.indexOf("(") == 0) {
          //判填空型
          if (text.substring(4, 7).equals("填空题")) {
            mc_d.put("题型", text.substring(4, 7));
            //System.out.println("---" + text.substring(4, 7));

            if (text.indexOf("&") != -1) {
              int a = text.indexOf("&");//&第一个出现的索引位置
              while (a != -1) {
                System.out.print(a + "\t");
                a = text.indexOf("&", a + 1);//&从这个索引往后开始第一个出现的位置
              }

              text.replace("&", "amp");

            }
            //判断是否有多个答案
            if (text.indexOf("|") != -1) {
              mc_d.put("答案1", text.substring(text.indexOf("答案") + 3, text.indexOf("|")));
              System.out.println(text.substring(text.indexOf("答案") + 3, text.indexOf("|")));
              //System.out.println("答案" + "-----" + text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 2));

              mc_d.put("答案2", text.substring(text.indexOf("|") + 1, text.indexOf("分值") - 2));
              System.out.println(text.substring(text.indexOf("|") + 1, text.indexOf("分值") - 2));
            }

            if (text.indexOf("|") == -1) {
              mc_d.put("答案1", text.substring(text.indexOf("答案") + 3, text.indexOf("分值") - 2));
            }

            mc_d.put("分值", text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));
            //System.out.println("分值" + "-----" + text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));

            //提取填空题选项
            paras.get(i + 1).getText();

            mc_d.put("题目", paras.get(i + 1).getText());

            //判断试题分类是否存在
            if (text.indexOf("试题分类") != -1) {
              mc_d.put("试题分类", text.substring(text.indexOf("试题分类") + 5, text.lastIndexOf(")")));

            }
            question.add(mc_d);
            System.out.println(question);
            questionlist.add(question);
            System.out.println(questionlist);

          }
        }
      }
    }
    System.out.println(questionlist.size());
    return questionlist;
  }

  //判断
  public List<List<Map<String, String>>> getQuestionDataJG(List<XWPFParagraph> paras) {
    List<Map<String, String>> question;
    Map<String, String> mc_d;
    List<List<Map<String, String>>> questionlist = new ArrayList<List<Map<String, String>>>();
    for (int i = 0; i < paras.size(); i++) {
      question = new ArrayList<Map<String, String>>();
      mc_d = new HashMap<String, String>();
      String text = paras.get(i).getText();
      System.out.println(text);
      if (!text.equals(" ") & text != null) {
        //匹配第一个字符为(时
        if (text.indexOf("(") == 0) {
          //判断题型
          if (text.substring(4, 7).equals("判断题")) {
            mc_d.put("题型", text.substring(4, 7));
            //System.out.println("---" + text.substring(4, 7));

            mc_d.put("答案", text.substring(text.indexOf("答案") + 3, text.indexOf("答案") + 4));
            //System.out.println("答案" + "-----" + text.substring(text.indexOf("答案") + 3, text.indexOf("分数") - 2));
            mc_d.put("分值", text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));
            //System.out.println("分值" + "-----" + text.substring(text.indexOf("分值") + 3, text.indexOf("分值") + 4));

            //提取判断选项
            paras.get(i + 1).getText();

            mc_d.put("题目", paras.get(i + 1).getText());

            //判断试题分类是否存在
            if (text.indexOf("试题分类") != -1) {
              mc_d.put("试题分类", text.substring(text.indexOf("试题分类") + 5, text.lastIndexOf(")")));

            }
            question.add(mc_d);
            System.out.println(question);
            questionlist.add(question);
            System.out.println(questionlist);

          }
        }
      }
    }
    System.out.println(questionlist.size());
    return questionlist;
  }
}
