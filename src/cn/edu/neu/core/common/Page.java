package cn.edu.neu.core.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Page<T> {

  private int pageNo = 1;

  private int pageSize = 15;

  private int totalRecord;

  private int totalPage;

  private List<T> list;

  private String actionUrl;

  private Map<String, Object> params = new HashMap<String, Object>();

  public Page() {
    super();
    // TODO Auto-generated constructor stub
    String pn = CommonBaseAction.getRequest().getParameter("pageNum");
    pageNo = pn == null ? 1 : Integer.parseInt(pn);

    actionUrl = CommonBaseAction.getRequest().getServletPath();
  }

  public Page(int pageSize) {
    super();
    // TODO Auto-generated constructor stub
    String pn = CommonBaseAction.getRequest().getParameter("pageNum");
    pageNo = pn == null ? 1 : Integer.parseInt(pn);
    this.pageSize = pageSize;
    actionUrl = CommonBaseAction.getRequest().getServletPath();
  }

  public int getPageNo() {
    return pageNo;
  }

  public void setPageNo(int pageNo) {
    this.pageNo = pageNo;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getTotalRecord() {
    return totalRecord;
  }

  public void setTotalRecord(int totalRecord) {
    this.totalRecord = totalRecord;

    int totalPage = totalRecord % pageSize == 0 ? totalRecord / pageSize : totalRecord / pageSize + 1;
    this.setTotalPage(totalPage);
  }

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public List<T> getList() {
    return list;
  }

  public void setList(List<T> list) {
    this.list = list;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public void setParams(Map<String, Object> params) {
    this.params = params;
  }

  public String getActionUrl() {
    return actionUrl;
  }

  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Page [pageNo=").append(pageNo).append(", pageSize=").append(pageSize).append(", list=").append(list).append(", totalPage=")
      .append(totalPage).append(", totalRecord=").append(totalRecord).append("]");
    return builder.toString();
  }

}
