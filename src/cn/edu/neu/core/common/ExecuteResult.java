package cn.edu.neu.core.common;

import java.util.HashMap;
import java.util.Map;

public class ExecuteResult {

  private String result = "success";//�������

  private String message = "";//��ʾ��Ϣ

  private Map redirURLs = new HashMap();//��һ������

  public void setResult(String result) {
    this.result = result;
  }

  public void addRedirURL(String desc, String url) {
    this.redirURLs.put(desc, url);
  }

  public void addMessage(String msg) {
    this.message = (msg);
  }

  public String getResult() {
    return result;
  }

  public Map getRedirURLs() {
    return redirURLs;
  }

  public String getMessage() {
    return message;
  }
}
