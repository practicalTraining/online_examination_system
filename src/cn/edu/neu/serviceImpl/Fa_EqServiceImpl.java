package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Fa_EqMapper;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Fa_Eq;
import cn.edu.neu.model.FillAnswer;
import cn.edu.neu.service.Fa_EqService;

@Service
public class Fa_EqServiceImpl implements Fa_EqService {
  @Autowired
  private Fa_EqMapper mapper;

  @Override
  public void saveFA(Fa_Eq faeq) {
    mapper.saveFA(faeq);

  }

  @Override
  public void delFA(Fa_Eq faeq) {
    mapper.delFA(faeq);

  }

  @Override
  public List<Equestion> getEqByFaId(String faId) {
    // TODO Auto-generated method stub
    return mapper.getEqByFaId(faId);
  }

  @Override
  public List<FillAnswer> getFaByEqId(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getFaByEqId(equestionId);
  }
}
