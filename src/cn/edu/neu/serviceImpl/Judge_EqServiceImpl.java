package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Judge_EqMapper;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.JudgeMent;
import cn.edu.neu.model.Judge_Eq;

@Service
public class Judge_EqServiceImpl implements cn.edu.neu.service.Judge_EqService {
  @Autowired
  private Judge_EqMapper mapper;

  @Override
  public void saveJG(Judge_Eq jgeq) {
    mapper.saveJG(jgeq);

  }

  @Override
  public void delJG(Judge_Eq jgeq) {
    mapper.delJG(jgeq);

  }

  @Override
  public List<JudgeMent> getJgByEqId(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getJgByEqId(equestionId);
  }

  @Override
  public List<Equestion> getEqByJgId(String jgId) {
    // TODO Auto-generated method stub
    return mapper.getEqByJgId(jgId);
  }
}
