package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.mapper.ExamPaperMapper;
import cn.edu.neu.model.ExamPaper;
import cn.edu.neu.service.ExamPaperService;

@Service
public class ExamPaperServiceImpl implements ExamPaperService {

  @Autowired
  private ExamPaperMapper mapper;

  @Override
  public Page<ExamPaper> findExamPapersBySearchKeyword(String keyword, String sort) {
    Page<ExamPaper> page = new Page<ExamPaper>(15);
    List<ExamPaper> list = mapper.findExamPapersBySearchKeyword(page);
    page.setList(list);
    return page;
  }

  @Override
  public Page<ExamPaper> findExamPapersByCourseId(String courseId, String sort) {
    Page<ExamPaper> page = new Page<ExamPaper>(15);
    List<ExamPaper> list = mapper.findExamPapersByCourseId(page);
    page.setList(list);
    return page;
  }

  @Override
  public void addExamPaper(ExamPaper epaper) {
    mapper.addExamPaper(epaper);

  }

  @Override
  public void editExamPaper(ExamPaper epaper) {
    mapper.editExamPaper(epaper);

  }

  @Override
  public void addExamPaperWithoutExamId(ExamPaper epaper) {
    mapper.addExamPaperWithoutExamId(epaper);

  }

  @Override
  public Page<ExamPaper> getexamPaperListwithCourse() {
    Page<ExamPaper> page = new Page<ExamPaper>(15);
    List<ExamPaper> list = mapper.getexamPaperListwithCourse(page);
    page.setList(list);
    return page;
  }

  @Override
  public ExamPaper getExamPaperById(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getExamPaperById(epaperId);
  }

  @Override
  public ExamPaper examPaperAndSubjectListByExamId(String examId) {
    // TODO Auto-generated method stub
    return mapper.examPaperAndSubjectListByExamId(examId);
  }

  @Override
  public List<ExamPaper> getEpaperbyCourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperbyCourseId(courseId);
  }

  @Override
  public void editExamPaperWithoutExamId(ExamPaper epaper) {
    mapper.editExamPaperWithoutExamId(epaper);
  }

  @Override
  public void delExamPaper(String epaperId) {
    mapper.delExamPaper(epaperId);

  }

  @Override
  public void editExamPaperExamId(String examId, String epaperId) {
    // TODO Auto-generated method stub
    mapper.editExamPaperExamId(examId, epaperId);
  }

  @Override
  public void delExam_Epaper(String epaperId) {
    // TODO Auto-generated method stub
    mapper.delExam_Epaper(epaperId);
  }

}
