package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.mapper.EquestionMapper;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.service.EquestionService;

@Service
public class EquestionServiceImpl implements EquestionService {

  @Autowired
  private EquestionMapper mapper;

  @Override
  public Equestion getEQById(String id) {

    return mapper.getEQById(id);
  }

  @Override
  public Page<Equestion> getAdminEQList() {
    Page<Equestion> page = new Page<Equestion>(10);
    List<Equestion> list = mapper.getEQList(page);
    page.setList(list);
    return page;
  }

  @Override
  public void addEQ(Equestion eq) {
    mapper.addEQ(eq);

  }

  @Override
  public void editEQ(Equestion eq) {
    mapper.editEQ(eq);

  }

  @Override
  public void delEQ(String id) {
    mapper.delEQ(id);

  }

  @Override
  public List<Equestion> getEQINList() {
    // TODO Auto-generated method stub
    return mapper.getEQINList();
  }

  @Override
  public List<Equestion> getEQAllSCById(String equestionId) {

    return mapper.getEQAllSCById(equestionId);
  }

  @Override
  public Page<Equestion> getEQAllSCById() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<Equestion> getEQAllAndSC(String courseId) {
    return mapper.getEQAllAndSC(courseId);
  }

  @Override
  //添加试卷页面中所有单选题方法
  public List<Equestion> getEpaperEQAllAndSC(String euqestionId) {
    return mapper.getEpaperEQAllAndSC(euqestionId);
  }

  @Override
  public List<Equestion> getEQAllMCById(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllMCById(equestionId);
  }

  @Override
  public Page<Equestion> getEQAllMCById() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<Equestion> getEQAllAndMC(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllAndMC(courseId);
  }

  @Override
  public List<Equestion> getEQAllFAById(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllFAById(equestionId);
  }

  @Override
  public List<Equestion> getEQAllJGById(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllJGById(equestionId);
  }

  @Override
  public List<Equestion> getEQAllAndFA(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllAndFA(courseId);
  }

  @Override
  public List<Equestion> getEQAllAndJG(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getEQAllAndJG(courseId);
  }

}
