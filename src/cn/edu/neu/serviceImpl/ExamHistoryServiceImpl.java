package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.ExamHistoryMapper;
import cn.edu.neu.model.ExamHistory;
import cn.edu.neu.service.ExamHistoryService;

@Service
public class ExamHistoryServiceImpl implements ExamHistoryService {
  @Autowired
  private ExamHistoryMapper mapper;

  @Override
  public void addExamHistory(ExamHistory examHistory) {
    // TODO Auto-generated method stub
    mapper.addExamHistory(examHistory);
  }

  @Override
  public List<ExamHistory> getExistExamHistory(String studentId) {
    // TODO Auto-generated method stub
    return mapper.getExistExamHistory(studentId);
  }
}
