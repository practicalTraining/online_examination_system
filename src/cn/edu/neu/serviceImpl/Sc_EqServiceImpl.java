package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Sc_EqMapper;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Sc_Eq;
import cn.edu.neu.model.SingleChoice;
import cn.edu.neu.service.Sc_EqService;

@Service
public class Sc_EqServiceImpl implements Sc_EqService {
  @Autowired
  private Sc_EqMapper mapper;

  @Override
  public void saveSC(Sc_Eq sceq) {
    mapper.saveSC(sceq);

  }

  @Override
  public void delSC(Sc_Eq sceq) {
    mapper.delSC(sceq);

  }

  @Override
  public List<Equestion> getEqByScId(String scId) {
    // TODO Auto-generated method stub
    return mapper.getEqByScId(scId);
  }

  @Override
  public List<SingleChoice> getScByEqId(String equestionId) {
    // TODO Auto-generated method stub
    return getScByEqId(equestionId);
  }

}
