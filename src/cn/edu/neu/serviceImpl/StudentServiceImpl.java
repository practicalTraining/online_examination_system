package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.edu.neu.mapper.StudentMapper;
import cn.edu.neu.model.Student;
import cn.edu.neu.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

  @Autowired
  private StudentMapper mapper;

  @Override
  public List<Student> getStudentToClass(String classId) {
    // TODO Auto-generated method stub
    return mapper.getStudentToClass(classId);
  }

  @Override
  public Student checkStuLogin(Student student) {
    // TODO Auto-generated method stub
    return mapper.checkStuLogin(student);
  }

  @Override
  public Student getStudentById(String studentId) {
    // TODO Auto-generated method stub
    return mapper.getStudentById(studentId);
  }
}
