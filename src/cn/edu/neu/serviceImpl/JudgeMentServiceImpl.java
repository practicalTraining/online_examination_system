package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.JudgeMentMapper;
import cn.edu.neu.model.JudgeMent;
import cn.edu.neu.service.JudgeMentService;

@Service
public class JudgeMentServiceImpl implements JudgeMentService {
  @Autowired
  private JudgeMentMapper mapper;

  @Override
  public List<JudgeMent> getJudgeMentList() {
    // TODO Auto-generated method stub
    return mapper.getJudgeMentList();
  }

  @Override
  public JudgeMent getJGById(String id) {
    // TODO Auto-generated method stub
    return mapper.getJGById(id);
  }

  @Override
  public List<JudgeMent> getJgALLById(String jgId) {
    // TODO Auto-generated method stub
    return mapper.getJgALLById(jgId);
  }

  @Override
  public List<JudgeMent> getJgUnAddEpaper(int courseId, int epaperId) {
    // TODO Auto-generated method stub
    return mapper.getJgUnAddEpaper(courseId, epaperId);
  }

  @Override
  public void addJG(JudgeMent jg) {
    // TODO Auto-generated method stub
    mapper.addJG(jg);

  }

  @Override
  public void editJG(JudgeMent jg) {
    // TODO Auto-generated method stub
    mapper.editJG(jg);

  }

  @Override
  public void delJG(String id) {
    // TODO Auto-generated method stub
    mapper.delJG(id);
    ;

  }
}
