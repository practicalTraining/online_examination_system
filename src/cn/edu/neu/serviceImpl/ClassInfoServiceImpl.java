package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.ClassInfoMapper;
import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.service.ClassInfoService;

@Service
public class ClassInfoServiceImpl implements ClassInfoService {
  @Autowired
  private ClassInfoMapper mapper;

  @Override
  public List<ClassInfo> getclassInfoList() {
    // TODO Auto-generated method stub
    return mapper.getclassInfoList();
  }

  @Override
  public ClassInfo getClassInfoById(String classId) {
    // TODO Auto-generated method stub
    return mapper.getClassInfoById(classId);
  }

  @Override
  public void addClassInfo(ClassInfo classInfo) {
    // TODO Auto-generated method stub
    mapper.addClassInfo(classInfo);
  }

  @Override
  public void editClassInfo(ClassInfo classInfo) {
    // TODO Auto-generated method stub
    mapper.editClassInfo(classInfo);
  }

  @Override
  public List<ClassInfo> getClassExamInfoById(String classId) {
    // TODO Auto-generated method stub
    return mapper.getClassExamInfoById(classId);
  }

  @Override
  public List<ClassInfo> getUnAddInExamClass(int courseId, int examId) {
    // TODO Auto-generated method stub
    return mapper.getUnAddInExamClass(courseId, examId);
  }

  @Override
  public List<ClassInfo> getExistExamClass(int examId) {
    // TODO Auto-generated method stub
    return mapper.getExistExamClass(examId);
  }
}
