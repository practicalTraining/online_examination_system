package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Exam_ClassMapper;
import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.Exam;
import cn.edu.neu.model.Exam_Class;
import cn.edu.neu.service.Exam_ClassService;

@Service
public class Exam_ClassServiceImpl implements Exam_ClassService {
  @Autowired
  private Exam_ClassMapper mapper;

  @Override
  public List<Exam> getClassToExam(String classId) {
    // TODO Auto-generated method stub
    return mapper.getClassToExam(classId);
  }

  @Override
  public List<ClassInfo> getExamToClass(String examId) {
    // TODO Auto-generated method stub
    return mapper.getExamToClass(examId);
  }

  @Override
  public List<Exam_Class> getExistClassIdByExamId(String examId) {
    // TODO Auto-generated method stub
    return mapper.getExistClassIdByExamId(examId);
  }

  @Override
  public void addExam_Class(Exam_Class exam_Class) {
    mapper.addExam_Class(exam_Class);

  }

  @Override
  public void delExam_Class(String examId, String classId) {
    mapper.delExam_Class(examId, classId);

  }
}
