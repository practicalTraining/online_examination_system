package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.ExamMapper;
import cn.edu.neu.model.Exam;
import cn.edu.neu.service.ExamService;

@Service
public class ExamServiceImpl implements ExamService {
  @Autowired
  private ExamMapper mapper;

  @Override
  public Exam getExamById(String examId) {
    // TODO Auto-generated method stub
    return mapper.getExamById(examId);
  }

  @Override
  public List<Exam> getExamList() {
    // TODO Auto-generated method stub
    return mapper.getExamList();
  }

  @Override
  public void addExam(Exam exam) {
    mapper.addExam(exam);

  }

  @Override
  public void editExam(Exam exam) {
    mapper.editExam(exam);

  }

  @Override
  public List<Exam> getExamByCourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getExamByCourseId(courseId);
  }

  @Override
  public Exam getExamScByExamId(String examId) {
    // TODO Auto-generated method stub
    return mapper.getExamScByExamId(examId);
  }

  @Override
  public Exam getEpaperByExamId(String examId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperByExamId(examId);
  }

  @Override
  public void editExamStatus(int examId, int examStatus) {
    mapper.editExamStatus(examId, examStatus);

  }
}
