package cn.edu.neu.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.AdminMapper;
import cn.edu.neu.model.Admin;
import cn.edu.neu.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {
  @Autowired
  private AdminMapper mapper;

  @Override
  public Admin adminLogin(Admin admin) {

    return mapper.adminLogin(admin);
  }

}
