package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.CounsellorMapper;
import cn.edu.neu.model.Counsellor;
import cn.edu.neu.service.CounsellorService;

@Service
public class CounsellorServiceImpl implements CounsellorService {
  @Autowired
  private CounsellorMapper mapper;

  @Override
  public List<Counsellor> getCounsellorList() {
    // TODO Auto-generated method stub
    return mapper.getCounsellorList();
  }
}
