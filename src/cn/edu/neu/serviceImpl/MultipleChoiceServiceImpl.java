package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.MultipleChoiceMapper;
import cn.edu.neu.model.MultipleChoice;
import cn.edu.neu.service.MultipleChoiceService;

@Service
public class MultipleChoiceServiceImpl implements MultipleChoiceService {
  @Autowired
  private MultipleChoiceMapper mapper;

  @Override
  public List<MultipleChoice> getMcList() {
    // TODO Auto-generated method stub
    return mapper.getMcList();
  }

  @Override
  public void addMc(MultipleChoice mc) {
    mapper.addMc(mc);

  }

  @Override
  public void editMc(MultipleChoice mc) {
    mapper.editMc(mc);

  }

  @Override
  public void delMc(String id) {
    mapper.delMc(id);

  }

  @Override
  public MultipleChoice getMcById(String mcId) {
    // TODO Auto-generated method stub
    return mapper.getMcById(mcId);
  }

  @Override
  public List<MultipleChoice> getMcUnAddEpaper(int courseId, int epaperId) {
    // TODO Auto-generated method stub
    return mapper.getMcUnAddEpaper(courseId, epaperId);
  }
}
