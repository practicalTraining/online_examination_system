package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.mapper.SingleChoiceMapper;
import cn.edu.neu.model.SingleChoice;
import cn.edu.neu.service.SingleChoiceService;

@Service
public class SingleChoiceServiceImpl implements SingleChoiceService {
  @Autowired
  private SingleChoiceMapper mapper;

  @Override
  public Page<SingleChoice> getAdminSingleChoiceList() {
    Page<SingleChoice> page = new Page<SingleChoice>(10);
    List<SingleChoice> list = mapper.getSingleChoiceList(page);
    page.setList(list);
    return page;
  }

  @Override
  public SingleChoice getSCById(String id) {
    SingleChoice sc = mapper.getSCById(id);
    return sc;
  }

  @Override
  public void addSC(SingleChoice sc) {
    mapper.addSC(sc);

  }

  @Override
  public void editSC(SingleChoice sc) {
    mapper.editSC(sc);

  }

  @Override
  public void delSC(String id) {
    mapper.delSC(id);

  }

  @Override
  public List<SingleChoice> getScALLById(String scId) {

    return mapper.getScALLById(scId);
  }

  @Override
  public Page<SingleChoice> getScALLById() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<SingleChoice> getScUnAddEpaper(int courseId, int epaperId) {
    // TODO Auto-generated method stub
    return mapper.getScUnAddEpaper(courseId, epaperId);
  }

}
