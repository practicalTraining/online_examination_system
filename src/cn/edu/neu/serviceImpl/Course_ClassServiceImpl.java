package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Course_ClassMapper;
import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.Course;
import cn.edu.neu.model.Course_Class;
import cn.edu.neu.service.Course_ClassService;

@Service
public class Course_ClassServiceImpl implements Course_ClassService {
  @Autowired
  private Course_ClassMapper mapper;

  @Override
  public List<Course> getCourseIdToCourseName(String classId) {
    // TODO Auto-generated method stub
    return mapper.getCourseIdToCourseName(classId);
  }

  @Override
  public List<ClassInfo> getClassIdToClassName(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getClassIdToClassName(courseId);
  }

  @Override
  public List<Course_Class> getClass_CourseIdByClassId(String classId) {
    // TODO Auto-generated method stub
    return mapper.getClass_CourseIdByClassId(classId);
  }

  @Override
  public void addClass_Course(Course_Class course_Class) {
    mapper.addClass_Course(course_Class);

  }
}
