package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.FillAnswerMapper;
import cn.edu.neu.model.FillAnswer;
import cn.edu.neu.service.FillAnswerService;

@Service
public class FillAnswerServiceImpl implements FillAnswerService {
  @Autowired
  private FillAnswerMapper mapper;

  @Override
  public List<FillAnswer> getFillAnswerList() {
    // TODO Auto-generated method stub
    return mapper.getFillAnswerList();
  }

  @Override
  public FillAnswer getFAById(String id) {
    // TODO Auto-generated method stub
    return mapper.getFAById(id);
  }

  @Override
  public List<FillAnswer> getFaUnAddEpaper(int courseId, int epaperId) {
    // TODO Auto-generated method stub
    return mapper.getFaUnAddEpaper(courseId, epaperId);
  }

  @Override
  public void addFA(FillAnswer fa) {
    mapper.addFA(fa);

  }

  @Override
  public void editFA(FillAnswer fa) {
    mapper.editFA(fa);

  }

  @Override
  public void delFA(String id) {
    mapper.delFA(id);

  }

  @Override
  public List<FillAnswer> getFaALLById(String faId) {

    return mapper.getFaALLById(faId);
  }

}
