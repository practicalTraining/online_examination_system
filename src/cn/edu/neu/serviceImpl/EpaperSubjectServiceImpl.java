package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.EpaperSubjectMapper;
import cn.edu.neu.model.EpaperSubject;
import cn.edu.neu.service.EpaperSubjectService;

@Service
public class EpaperSubjectServiceImpl implements EpaperSubjectService {

  @Autowired
  private EpaperSubjectMapper mapper;

  @Override
  public EpaperSubject getEpaperSubjectByepaperId(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperSubjectByepaperId(epaperId);
  }

  @Override
  public void addEpaperEQSC(EpaperSubject epaperSubject) {
    mapper.addEpaperEQSC(epaperSubject);

  }

  @Override
  public List<EpaperSubject> getEpaperSubjectByepaperIdwihtScId(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperSubjectByepaperIdwihtScId(epaperId);
  }

  @Override
  public List<EpaperSubject> getEpaperSubjectByepaperIdwihtMcId(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperSubjectByepaperIdwihtMcId(epaperId);
  }

  @Override
  public void delEpaperSubject(EpaperSubject epaperSubject) {
    mapper.delEpaperSubject(epaperSubject);

  }

  @Override
  public void addEpaperEQMC(EpaperSubject epaperSubject) {
    mapper.addEpaperEQMC(epaperSubject);

  }

  @Override
  public void addEpaperEQFA(EpaperSubject epaperSubject) {
    mapper.addEpaperEQFA(epaperSubject);

  }

  @Override
  public void addEpaperEQJG(EpaperSubject epaperSubject) {
    mapper.addEpaperEQJG(epaperSubject);

  }

  @Override
  public List<EpaperSubject> getEpaperSubjectByepaperIdwihtFaId(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperSubjectByepaperIdwihtFaId(epaperId);
  }

  @Override
  public List<EpaperSubject> getEpaperSubjectByepaperIdwihtJgId(String epaperId) {
    // TODO Auto-generated method stub
    return mapper.getEpaperSubjectByepaperIdwihtJgId(epaperId);
  }
}
