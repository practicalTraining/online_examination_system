package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.Mc_EqMapper;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Mc_Eq;
import cn.edu.neu.model.MultipleChoice;
import cn.edu.neu.service.Mc_EqService;

@Service
public class Mc_EqServiceImpl implements Mc_EqService {
  @Autowired
  private Mc_EqMapper mapper;

  @Override
  public void saveMC(Mc_Eq mceq) {
    mapper.saveMC(mceq);

  }

  @Override
  public void delMC(Mc_Eq mceq) {
    mapper.delMC(mceq);

  }

  @Override
  public List<Equestion> getEqByMcId(String mcId) {
    // TODO Auto-generated method stub
    return mapper.getEqByMcId(mcId);
  }

  @Override
  public List<MultipleChoice> getMcByEqId(String equestionId) {
    // TODO Auto-generated method stub
    return mapper.getMcByEqId(equestionId);
  }
}
