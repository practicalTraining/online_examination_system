package cn.edu.neu.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.mapper.CourseMapper;
import cn.edu.neu.model.Course;
import cn.edu.neu.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
  @Autowired
  private CourseMapper mapper;

  @Override
  public List<Course> getCourseList() {
    return mapper.getCourseList();
  }

  @Override
  public Course getCourseById(String id) {
    return mapper.getCourseById(id);
  }

  @Override
  public void addCourse(Course course) {
    mapper.addCourse(course);

  }

  @Override
  public void editCourse(Course course) {
    mapper.editCourse(course);

  }

  @Override
  public void delCourse(String id) {
    mapper.delCourse(id);

  }

  @Override
  public List<Course> getCourseAndEQSCBycourseId(String courseId) {
    return mapper.getCourseAndEQSCBycourseId(courseId);
  }

  @Override
  public Course getCourse_ClassByCourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getCourse_ClassByCourseId(courseId);
  }

  @Override
  public Course getCourse_EpaperByCourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getCourse_EpaperByCourseId(courseId);
  }

  @Override
  public List<Course> getCourseAndEQMCBycourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getCourseAndEQMCBycourseId(courseId);
  }

  @Override
  public List<Course> getCourseAndEQFABycourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getCourseAndEQFABycourseId(courseId);
  }

  @Override
  public List<Course> getCourseAndEQJGBycourseId(String courseId) {
    // TODO Auto-generated method stub
    return mapper.getCourseAndEQJGBycourseId(courseId);
  }

}
