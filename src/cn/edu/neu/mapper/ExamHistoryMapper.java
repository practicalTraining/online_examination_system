package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.ExamHistory;

public interface ExamHistoryMapper {
  void addExamHistory(ExamHistory examHistory);

  List<ExamHistory> getExistExamHistory(String studentId);
}
