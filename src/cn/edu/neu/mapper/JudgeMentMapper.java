package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.model.JudgeMent;

public interface JudgeMentMapper {
  List<JudgeMent> getJudgeMentList();

  JudgeMent getJGById(String id);

  List<JudgeMent> getJgALLById(String jgId);

  //获取没添加到试卷中的单选题列表
  List<JudgeMent> getJgUnAddEpaper(@Param("courseId") int courseId, @Param("epaperId") int epaperId);

  void addJG(JudgeMent jg);

  void editJG(JudgeMent jg);

  void delJG(String id);

}
