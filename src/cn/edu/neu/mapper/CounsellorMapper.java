package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Counsellor;

public interface CounsellorMapper {

  List<Counsellor> getCounsellorList();
}
