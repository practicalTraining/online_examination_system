package cn.edu.neu.mapper;

import cn.edu.neu.model.Admin;

public interface AdminMapper {
  Admin adminLogin(Admin admin);
}
