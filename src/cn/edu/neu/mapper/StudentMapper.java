package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Student;

public interface StudentMapper {
  List<Student> getStudentToClass(String classId);

  Student checkStuLogin(Student student);

  Student getStudentById(String studentId);
}
