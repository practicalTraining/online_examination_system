package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.Equestion;

public interface EquestionMapper {

  Equestion getEQById(String id);

  List<Equestion> getEpaperEQAllAndSC(String euqestionId);//添加试卷页面中所有单选题方法

  List<Equestion> getEQList(Page<Equestion> page);

  List<Equestion> getEQINList();//id 和 name
  // -- questionDetails

  List<Equestion> getEQAllSCById(String equestionId);

  List<Equestion> getEQAllSCById(Page<Equestion> page, String equestionId);

  List<Equestion> getEQAllMCById(String equestionId);

  List<Equestion> getEQAllMCById(Page<Equestion> page, String equestionId);

  List<Equestion> getEQAllFAById(String equestionId);

  List<Equestion> getEQAllJGById(String equestionId);
  //

  //查询试卷页面中的的题目
  List<Equestion> getEQAllAndMC(String courseId);

  List<Equestion> getEQAllAndSC(String courseId);

  List<Equestion> getEQAllAndFA(String courseId);

  List<Equestion> getEQAllAndJG(String courseId);
  //

  void addEQ(Equestion eq);

  void editEQ(Equestion eq);

  void delEQ(String id);

}
