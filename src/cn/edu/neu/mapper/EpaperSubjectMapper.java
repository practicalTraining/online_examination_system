package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.EpaperSubject;

public interface EpaperSubjectMapper {

  EpaperSubject getEpaperSubjectByepaperId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtScId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtFaId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtJgId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtMcId(String epaperId);

  void addEpaperEQSC(EpaperSubject epaperSubject);

  void addEpaperEQMC(EpaperSubject epaperSubject);

  void delEpaperSubject(EpaperSubject epaperSubject);

  void addEpaperEQFA(EpaperSubject epaperSubject);

  void addEpaperEQJG(EpaperSubject epaperSubject);

}
