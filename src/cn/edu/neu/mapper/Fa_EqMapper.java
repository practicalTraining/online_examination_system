package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Fa_Eq;
import cn.edu.neu.model.FillAnswer;

public interface Fa_EqMapper {

  void delFA(Fa_Eq faeq);

  List<Equestion> getEqByFaId(String faId);

  List<FillAnswer> getFaByEqId(String equestionId);

  void saveFA(Fa_Eq faeq);

}
