package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.SingleChoice;

public interface SingleChoiceMapper {

  List<SingleChoice> getSingleChoiceList(Page<SingleChoice> page);

  SingleChoice getSCById(String id);

  List<SingleChoice> getScALLById(String scId);

  List<SingleChoice> getScALLById(Page<SingleChoice> page, String scId);

  //获取没添加到试卷中的单选题列表
  List<SingleChoice> getScUnAddEpaper(@Param("courseId") int courseId, @Param("epaperId") int epaperId);

  void addSC(SingleChoice sc);

  void editSC(SingleChoice sc);

  void delSC(String id);
}
