package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.model.Exam;

public interface ExamMapper {

  Exam getExamScByExamId(String examId);

  Exam getExamById(String examId);

  Exam getEpaperByExamId(String examId);

  List<Exam> getExamByCourseId(String courseId);

  List<Exam> getExamList();

  void addExam(Exam exam);

  void editExam(Exam exam);

  void editExamStatus(@Param("examId") int examId, @Param("examStatus") int examStatus);

}
