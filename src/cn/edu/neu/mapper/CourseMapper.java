package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Course;

public interface CourseMapper {
  List<Course> getCourseList();

  Course getCourseById(String id);

  void addCourse(Course course);

  void editCourse(Course course);

  void delCourse(String id);

  //查询课程下所有题库中的 题目  用于添加到试卷页面
  List<Course> getCourseAndEQSCBycourseId(String courseId);

  List<Course> getCourseAndEQMCBycourseId(String courseId);

  List<Course> getCourseAndEQFABycourseId(String courseId);

  List<Course> getCourseAndEQJGBycourseId(String courseId);

  //end

  Course getCourse_EpaperByCourseId(String courseId);

  Course getCourse_ClassByCourseId(String courseId);

}
