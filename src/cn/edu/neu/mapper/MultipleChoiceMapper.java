package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.model.MultipleChoice;

public interface MultipleChoiceMapper {
  List<MultipleChoice> getMcList();

  void addMc(MultipleChoice mc);

  void editMc(MultipleChoice mc);

  void delMc(String id);

  MultipleChoice getMcById(String mcId);

  //获取没添加到试卷中的多选题列表
  List<MultipleChoice> getMcUnAddEpaper(@Param("courseId") int courseId, @Param("epaperId") int epaperId);
}
