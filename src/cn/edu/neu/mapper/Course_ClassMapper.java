package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.Course;
import cn.edu.neu.model.Course_Class;

public interface Course_ClassMapper {
  List<Course> getCourseIdToCourseName(String classId);

  List<ClassInfo> getClassIdToClassName(String courseId);

  List<Course_Class> getClass_CourseIdByClassId(String classId);

  void addClass_Course(Course_Class course_Class);
}
