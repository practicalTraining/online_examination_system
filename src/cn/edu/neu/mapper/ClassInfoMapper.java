package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.model.ClassInfo;

public interface ClassInfoMapper {
  List<ClassInfo> getclassInfoList();

  List<ClassInfo> getClassExamInfoById(String classId);

  ClassInfo getClassInfoById(String classId);

  void addClassInfo(ClassInfo classInfo);

  void editClassInfo(ClassInfo classInfo);

  List<ClassInfo> getUnAddInExamClass(@Param("courseId") int courseId, @Param("examId") int examId);//获取考试中未添加的班级 

  List<ClassInfo> getExistExamClass(int examId);//获取考试中已添加的班级 
}
