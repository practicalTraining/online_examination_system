package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.JudgeMent;
import cn.edu.neu.model.Judge_Eq;

public interface Judge_EqMapper {

  void delJG(Judge_Eq jgeq);

  List<Equestion> getEqByJgId(String jgId);

  List<JudgeMent> getJgByEqId(String equestionId);

  void saveJG(Judge_Eq jgeq);

}
