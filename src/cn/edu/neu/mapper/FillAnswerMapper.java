package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.model.FillAnswer;

public interface FillAnswerMapper {
  List<FillAnswer> getFillAnswerList();

  FillAnswer getFAById(String id);

  List<FillAnswer> getFaALLById(String faId);

  //获取没添加到试卷中的判断题列表
  List<FillAnswer> getFaUnAddEpaper(@Param("courseId") int courseId, @Param("epaperId") int epaperId);

  void addFA(FillAnswer fa);

  void editFA(FillAnswer fa);

  void delFA(String id);
}
