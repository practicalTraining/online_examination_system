package cn.edu.neu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.ExamPaper;

public interface ExamPaperMapper {

  ExamPaper examPaperAndSubjectListByExamId(String examId);

  List<ExamPaper> getEpaperbyCourseId(String courseId);

  List<ExamPaper> getExamPaperList(Page<ExamPaper> page);

  List<ExamPaper> getExamPaperList();

  List<ExamPaper> getexamPaperListwithCourse();

  List<ExamPaper> getexamPaperListwithCourse(Page<ExamPaper> page);

  List<ExamPaper> findExamPapersBySearchKeyword(Page<ExamPaper> page);

  List<ExamPaper> findExamPapersByCourseId(Page<ExamPaper> page);

  List<ExamPaper> findExamPapersBySearchKeyword();

  List<ExamPaper> findExamPapersByCourseId();

  ExamPaper getExamPaperById(String epaperId);

  void addExamPaper(ExamPaper epaper);

  void editExamPaper(ExamPaper epaper);

  void addExamPaperWithoutExamId(ExamPaper epaper);

  void editExamPaperWithoutExamId(ExamPaper epaper);

  void editExamPaperExamId(@Param("examId") String examId, @Param("epaperId") String epaperId);

  void delExamPaper(String epaperId);

  void delExam_Epaper(String epaperId);//撤销考试中的试卷
}
