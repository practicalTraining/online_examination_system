package cn.edu.neu.mapper;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Sc_Eq;
import cn.edu.neu.model.SingleChoice;

public interface Sc_EqMapper {

  void saveSC(Sc_Eq sceq);

  void delSC(Sc_Eq sceq);

  List<Equestion> getEqByScId(String scId);

  List<SingleChoice> getScByEqId(String equestionId);

}
