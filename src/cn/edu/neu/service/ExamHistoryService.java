package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.ExamHistory;

public interface ExamHistoryService {
  void addExamHistory(ExamHistory examHistory);

  List<ExamHistory> getExistExamHistory(String studentId);
}
