package cn.edu.neu.service;

import cn.edu.neu.model.Admin;

public interface AdminService {
  Admin adminLogin(Admin admin);
}
