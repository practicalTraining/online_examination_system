package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.EpaperSubject;

public interface EpaperSubjectService {
  EpaperSubject getEpaperSubjectByepaperId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtScId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtMcId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtFaId(String epaperId);

  List<EpaperSubject> getEpaperSubjectByepaperIdwihtJgId(String epaperId);

  void delEpaperSubject(EpaperSubject epaperSubject);

  void addEpaperEQSC(EpaperSubject epaperSubject);

  void addEpaperEQMC(EpaperSubject epaperSubject);

  void addEpaperEQFA(EpaperSubject epaperSubject);

  void addEpaperEQJG(EpaperSubject epaperSubject);
}
