package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Counsellor;

public interface CounsellorService {
  List<Counsellor> getCounsellorList();
}
