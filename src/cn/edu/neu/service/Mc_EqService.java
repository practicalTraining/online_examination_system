package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Mc_Eq;
import cn.edu.neu.model.MultipleChoice;

public interface Mc_EqService {

  void saveMC(Mc_Eq mceq);

  void delMC(Mc_Eq mceq);

  List<Equestion> getEqByMcId(String mcId);

  List<MultipleChoice> getMcByEqId(String equestionId);

}
