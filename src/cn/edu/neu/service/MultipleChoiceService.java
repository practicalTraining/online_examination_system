package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.MultipleChoice;

public interface MultipleChoiceService {
  List<MultipleChoice> getMcList();

  void addMc(MultipleChoice mc);

  void editMc(MultipleChoice mc);

  void delMc(String id);

  MultipleChoice getMcById(String mcId);

  //获取没添加到试卷中的多选题列表

  List<MultipleChoice> getMcUnAddEpaper(int courseId, int epaperId);

}
