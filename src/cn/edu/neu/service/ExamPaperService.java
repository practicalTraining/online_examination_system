package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.ExamPaper;

public interface ExamPaperService {
  ExamPaper examPaperAndSubjectListByExamId(String examId);

  Page<ExamPaper> findExamPapersBySearchKeyword(String keyword, String sort);

  Page<ExamPaper> findExamPapersByCourseId(String courseId, String sort);

  void addExamPaper(ExamPaper epaper);

  void editExamPaper(ExamPaper epaper);

  void addExamPaperWithoutExamId(ExamPaper epaper);

  ExamPaper getExamPaperById(String epaperId);

  Page<ExamPaper> getexamPaperListwithCourse();

  List<ExamPaper> getEpaperbyCourseId(String courseId);

  void editExamPaperWithoutExamId(ExamPaper epaper);

  void delExamPaper(String epaperId);

  void editExamPaperExamId(String examId, String epaperId);

  void delExam_Epaper(String epaperId);//撤销考试中的试卷

}
