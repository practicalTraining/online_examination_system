package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Fa_Eq;
import cn.edu.neu.model.FillAnswer;

public interface Fa_EqService {

  void saveFA(Fa_Eq faeq);

  void delFA(Fa_Eq faeq);

  List<Equestion> getEqByFaId(String faId);

  List<FillAnswer> getFaByEqId(String equestionId);
}
