package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Exam;

public interface ExamService {

  Exam getExamScByExamId(String examId);

  Exam getExamById(String examId);

  Exam getEpaperByExamId(String examId);

  List<Exam> getExamByCourseId(String courseId);

  List<Exam> getExamList();

  void addExam(Exam exam);

  void editExam(Exam exam);

  void editExamStatus(int examId, int examStatus);

}
