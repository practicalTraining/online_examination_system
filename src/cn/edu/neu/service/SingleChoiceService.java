package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.SingleChoice;

public interface SingleChoiceService {
  Page<SingleChoice> getAdminSingleChoiceList();

  SingleChoice getSCById(String id);

  void addSC(SingleChoice sc);

  void editSC(SingleChoice sc);

  void delSC(String id);

  List<SingleChoice> getScUnAddEpaper(int courseId, int epaperId); //获取没添加到试卷中的单选题列表

  List<SingleChoice> getScALLById(String scId);//获取题库下所有单选题

  Page<SingleChoice> getScALLById();

}
