package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.FillAnswer;

public interface FillAnswerService {
  List<FillAnswer> getFillAnswerList();

  FillAnswer getFAById(String id);

  List<FillAnswer> getFaALLById(String faId);

  //获取没添加到试卷中的判断题列表
  List<FillAnswer> getFaUnAddEpaper(int courseId, int epaperId);

  void addFA(FillAnswer fa);

  void editFA(FillAnswer fa);

  void delFA(String id);
}
