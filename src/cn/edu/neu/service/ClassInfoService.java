package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.ClassInfo;

public interface ClassInfoService {
  List<ClassInfo> getclassInfoList();

  List<ClassInfo> getClassExamInfoById(String classId);

  ClassInfo getClassInfoById(String classId);

  void addClassInfo(ClassInfo classInfo);

  void editClassInfo(ClassInfo classInfo);

  List<ClassInfo> getUnAddInExamClass(int courseId, int examId);

  List<ClassInfo> getExistExamClass(int examId);
}
