package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.Exam;
import cn.edu.neu.model.Exam_Class;

public interface Exam_ClassService {
  List<Exam> getClassToExam(String classId);

  List<ClassInfo> getExamToClass(String examId);

  List<Exam_Class> getExistClassIdByExamId(String examId);

  void addExam_Class(Exam_Class exam_Class);

  void delExam_Class(String examId, String classId);
}
