package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.JudgeMent;
import cn.edu.neu.model.Judge_Eq;

public interface Judge_EqService {

  void saveJG(Judge_Eq jgeq);

  void delJG(Judge_Eq jgeq);

  List<Equestion> getEqByJgId(String jgId);

  List<JudgeMent> getJgByEqId(String equestionId);
}
