package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.JudgeMent;

public interface JudgeMentService {
  List<JudgeMent> getJudgeMentList();

  JudgeMent getJGById(String id);

  List<JudgeMent> getJgALLById(String jgId);

  //获取没添加到试卷中的单选题列表
  List<JudgeMent> getJgUnAddEpaper(int courseId, int epaperId);

  void addJG(JudgeMent jg);

  void editJG(JudgeMent jg);

  void delJG(String id);

}
