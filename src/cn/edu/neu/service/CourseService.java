package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Course;

public interface CourseService {
  List<Course> getCourseList();

  Course getCourseById(String id);

  void addCourse(Course course);

  void editCourse(Course course);

  void delCourse(String id);

  List<Course> getCourseAndEQSCBycourseId(String courseId);

  List<Course> getCourseAndEQMCBycourseId(String courseId);

  List<Course> getCourseAndEQFABycourseId(String courseId);

  List<Course> getCourseAndEQJGBycourseId(String courseId);

  Course getCourse_ClassByCourseId(String courseId);

  Course getCourse_EpaperByCourseId(String courseId);
}
