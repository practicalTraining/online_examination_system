package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Sc_Eq;
import cn.edu.neu.model.SingleChoice;

public interface Sc_EqService {
  void saveSC(Sc_Eq sceq);

  void delSC(Sc_Eq sceq);

  List<Equestion> getEqByScId(String scId);

  List<SingleChoice> getScByEqId(String equestionId);
}
