package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.Equestion;

public interface EquestionService {

  Equestion getEQById(String id);

  Page<Equestion> getAdminEQList();

  void addEQ(Equestion eq);

  void editEQ(Equestion eq);

  void delEQ(String id);

  List<Equestion> getEQINList();//id 和 name

  List<Equestion> getEQAllSCById(String equestionId);//获得题库下所有单选题包含课程

  Page<Equestion> getEQAllSCById();

  Page<Equestion> getEQAllMCById();

  List<Equestion> getEQAllMCById(String equestionId);//获得题库下所有单选题包含课程

  List<Equestion> getEQAllFAById(String equestionId);

  List<Equestion> getEQAllJGById(String equestionId);

  List<Equestion> getEQAllAndFA(String courseId);

  List<Equestion> getEQAllAndJG(String courseId);

  List<Equestion> getEQAllAndMC(String courseId);

  List<Equestion> getEQAllAndSC(String courseId);//获取试卷添加单选题

  List<Equestion> getEpaperEQAllAndSC(String euqestionId);//添加试卷页面中所有单选题方法

}
