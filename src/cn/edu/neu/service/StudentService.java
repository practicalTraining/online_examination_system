package cn.edu.neu.service;

import java.util.List;

import cn.edu.neu.model.Student;

public interface StudentService {
  List<Student> getStudentToClass(String classId);

  Student checkStuLogin(Student student);

  Student getStudentById(String studentId);
}
