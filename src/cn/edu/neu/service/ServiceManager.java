package cn.edu.neu.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "servMgr")
public class ServiceManager {
  protected final Log log = LogFactory.getLog(getClass());

  @Autowired
  private StudentService studentService;

  @Autowired
  private AdminService adminService;

  @Autowired
  private SingleChoiceService singleChoiceService;

  @Autowired
  private EquestionService equestionService;

  @Autowired
  private CourseService courseService;

  @Autowired
  private Sc_EqService sc_eqService;

  @Autowired
  private ExamPaperService examPaperService;

  @Autowired
  private ExamService examService;

  @Autowired
  private ExamDetailService examDetailService;

  @Autowired
  private TeacherService teacherService;

  @Autowired
  private SubjectTypeService subjectTypeService;

  @Autowired
  private EpaperSubjectService epaperSubjectService;

  @Autowired
  private ClassInfoService classInfoService;

  @Autowired
  private Course_ClassService course_ClassService;

  @Autowired
  private CounsellorService counsellorServiece;

  @Autowired
  private Exam_ClassService exam_ClassService;

  @Autowired
  private ExamHistoryService examHistoryService;

  @Autowired
  private FillAnswerService fillAnswerService;

  @Autowired
  private MultipleChoiceService multipleChoiceService;

  @Autowired
  private JudgeMentService judgeMentService;

  @Autowired
  private Mc_EqService mc_EqService;

  @Autowired
  private Fa_EqService fa_EqService;

  @Autowired
  private Judge_EqService judge_EqService;

  @Autowired
  private Teacher_ClassService teacher_ClassService;

  public FillAnswerService getFillAnswerService() {
    return fillAnswerService;
  }

  public MultipleChoiceService getMultipleChoiceService() {
    return multipleChoiceService;
  }

  public JudgeMentService getJudgeMentService() {
    return judgeMentService;
  }

  public Mc_EqService getMc_EqService() {
    return mc_EqService;
  }

  public Fa_EqService getFa_EqService() {
    return fa_EqService;
  }

  public Judge_EqService getJudge_EqService() {
    return judge_EqService;
  }

  public Sc_EqService getSc_eqService() {
    return sc_eqService;
  }

  public StudentService getStudentService() {
    return studentService;
  }

  public AdminService getAdminService() {
    return adminService;
  }

  public SingleChoiceService getSingleChoiceService() {
    return singleChoiceService;
  }

  public EquestionService getEquestionService() {
    return equestionService;
  }

  public CourseService getCourseService() {
    return courseService;
  }

  public ExamPaperService getExamPaperService() {
    return examPaperService;
  }

  public ExamService getExamService() {
    return examService;
  }

  public ExamDetailService getExamDetailService() {
    return examDetailService;
  }

  public TeacherService getTeacherService() {
    return teacherService;
  }

  public SubjectTypeService getSubjectTypeService() {
    return subjectTypeService;
  }

  public EpaperSubjectService getEpaperSubjectService() {
    return epaperSubjectService;
  }

  public ClassInfoService getClassInfoService() {
    return classInfoService;
  }

  public Course_ClassService getCourse_ClassService() {
    return course_ClassService;
  }

  public CounsellorService getCounsellorServiece() {
    return counsellorServiece;
  }

  public Exam_ClassService getExam_ClassService() {
    return exam_ClassService;
  }

  public ExamHistoryService getExamHistoryService() {
    return examHistoryService;
  }

  public Teacher_ClassService getTeacher_ClassService() {
    return teacher_ClassService;
  }

}
