package cn.edu.neu.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.edu.neu.model.Exam;
import cn.edu.neu.model.Exam_Class;

@Controller
@RequestMapping("admin")
public class AdminExamAtion extends BaseAction {
  @RequestMapping("adminExamIndex")
  public String adminExamIndex(Map<String, Object> m) {
    Date examdate = null;
    Date sysdate = new Date();
    Date sysdatefinish = null;
    int examStatus = 0;
    int examId = 0;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    System.out.println(df.format(new Date()));
    List<Exam> time = this.getServMgr().getExamService().getExamList();
    Exam exam = new Exam();

    for (int i = 0; i < time.size(); i++) {
      examdate = time.get(i).getExamDate();
      sysdatefinish = new Date((examdate.getTime() + time.get(i).getExamDuration() * 1000 * 60));
      examId = time.get(i).getExamId();
      System.out.println("当前系统时间为" + df.format(sysdate));
      System.out.println("当前系统时间推后考试持续时间为" + df.format(sysdatefinish));
      System.out.println("考试时间" + df.format(examdate));
      if (sysdate.after(examdate) && sysdate.before(sysdatefinish)) {
        if (examdate.before(sysdatefinish)) {
          System.out.println(1);
          examStatus = 1;
          this.getServMgr().getExamService().editExamStatus(examId, examStatus);
        } else {
          System.out.println(2);
          examStatus = 2;
          this.getServMgr().getExamService().editExamStatus(examId, examStatus);
        }

      } else if (sysdate.after(sysdatefinish)) {
        System.out.println(2);
        examStatus = 2;
        this.getServMgr().getExamService().editExamStatus(examId, examStatus);
      } else {
        System.out.println(0);
        examStatus = 0;
        this.getServMgr().getExamService().editExamStatus(examId, examStatus);
      }
    }

    m.put("exam", this.getServMgr().getExamService().getExamList());
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminExamIndex";
  }

  @RequestMapping("adminHandleExam")
  public String adminHanleExam(@RequestParam(required = false) String examId, Map<String, Object> m) {
    if (examId != null) {
      m.put("exam", this.getServMgr().getExamService().getExamById(examId));
    }
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminHandleExam";
  }

  @RequestMapping("doAdminHandleExam")
  public String adminHanleExam(Exam exam, Map<String, Object> m) {
    if (exam.getExamId() != 0) {
      try {
        this.getServMgr().getExamService().editExam(exam);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failure";
      }
    } else {
      if (exam.getTeacherId() == 0) {
        exam.setTeacherId(1);
      }
      try {
        this.getServMgr().getExamService().addExam(exam);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failure";
      }

    }
  }

  @RequestMapping("checkExamDetail")
  public String checkExamDetail(@RequestParam String examId, @RequestParam String courseId, Map<String, Object> m) {

    m.put("examinfo", this.getServMgr().getExamService().getExamById(examId));

    return "admin/adminExam/adminExamDetail";
  }

  @RequestMapping("adminHandleExam_Class")
  public String adminHandleExam_Class(Exam_Class exam_Class, @RequestParam String courseId) {
    try {
      this.getServMgr().getExam_ClassService().addExam_Class(exam_Class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "admin/adminExam/adminExam_Class";

  }

  @RequestMapping("getAdminExam_Class")
  public String getAdminExam_Class(@RequestParam String examId, @RequestParam String courseId, Map<String, Object> m) {
    m.put("unAddClass", this.getServMgr().getClassInfoService().getUnAddInExamClass(Integer.parseInt(courseId), Integer.parseInt(examId)));
    System.out.println(this.getServMgr().getClassInfoService().getUnAddInExamClass(Integer.parseInt(courseId), Integer.parseInt(examId)));
    m.put("existClass", this.getServMgr().getClassInfoService().getExistExamClass(Integer.parseInt(examId)));
    m.put("classinfo", this.getServMgr().getCourseService().getCourse_ClassByCourseId(courseId));

    return "admin/adminExam/adminExam_Class";

  }

  @RequestMapping("getAdminExam_Epaper")
  public String getAdminExam_Epaper(@RequestParam String examId, @RequestParam String courseId, Map<String, Object> m) {
    Exam epaperinfo = this.getServMgr().getExamService().getEpaperByExamId(examId);
    if (epaperinfo != null) {
      m.put("epaperinfo", this.getServMgr().getExamService().getEpaperByExamId(examId));
      System.out.println(epaperinfo);
    } else {
      m.put("CourseEpaperInfo", this.getServMgr().getCourseService().getCourse_EpaperByCourseId(courseId));
      System.out.println(this.getServMgr().getCourseService().getCourse_EpaperByCourseId(courseId));
    }
    return "admin/adminExam/adminExam_Epaper";

  }

  @ResponseBody
  @RequestMapping("adminDelExam_Class")
  public String adminDelExam_Class(@RequestParam String examId, @RequestParam String classId, Map<String, String> m) {

    try {
      this.getServMgr().getExam_ClassService().delExam_Class(examId, classId);
      return "success";
    } catch (Exception e) {
      e.printStackTrace();
      return "failure";
    }
  }

  @ResponseBody
  @RequestMapping("adminDelExam_Epaper")
  public String adminDelExam_Epaper(@RequestParam String epaperId, Map<String, String> m) {

    try {
      this.getServMgr().getExamPaperService().delExam_Epaper(epaperId);
      return "success";
    } catch (Exception e) {
      e.printStackTrace();
      return "failure";
    }
  }

  @ResponseBody
  @RequestMapping("adminHandleExam_Epaper")
  public String adminHandleExam_Epaper(@RequestParam String examId, @RequestParam String epaperId) {
    if (examId != "") {
      this.getServMgr().getExamPaperService().editExamPaperExamId(examId, epaperId);
      return "success";
    } else {
      return "failure";
    }
  }

}
