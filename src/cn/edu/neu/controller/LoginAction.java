package cn.edu.neu.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.model.Student;

@Controller
@RequestMapping("login")
public class LoginAction extends BaseAction {
  @RequestMapping("stuloginIndex")
  public String loginIndex() {

    return "/loginIndex";
  }

  @RequestMapping("stulogin")
  public String stuLogin(Student student, HttpSession session) {
    System.out.println(student);
    Student dbstu = this.getServMgr().getStudentService().checkStuLogin(student);
    System.out.println(dbstu);
    if (dbstu != null) {
      session.setAttribute("student_Name", student.getStudentName());
      session.setAttribute("student_Id", dbstu.getStudentId());
      return "redirect:/student/studentIndex";
    } else {
      return "/loginIndex";
    }
  }

}
