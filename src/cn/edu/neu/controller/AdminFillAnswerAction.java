package cn.edu.neu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.core.util.HandleWord;
import cn.edu.neu.model.Fa_Eq;
import cn.edu.neu.model.FillAnswer;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("admin")
public class AdminFillAnswerAction extends BaseAction {
  @RequestMapping("adminFillAnswer")
  public String adminFillAnswer(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "填空题");
    return "/admin/adminSubject/adminFillAnswer/adminFillAnswerIndex";
  }

  @RequestMapping("getAdminFillAnswerList")
  public String getAdminFAList(Map<String, Object> m) {
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    List<FillAnswer> fa = this.getServMgr().getFillAnswerService().getFillAnswerList();

    m.put("course", this.getServMgr().getCourseService().getCourseList());
    m.put("fa", fa);
    System.out.println(fa);
    return "/admin/adminSubject/adminFillAnswer/adminFillAnswerList";

  }

  @RequestMapping("adminHandleFA")
  public String getAdminFaList(@RequestParam(required = false) String faId, Map<String, Object> m) {
    setReferUrl();
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    if (faId != null && faId != "") {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "修改单选题");
      m.put("fa", this.getServMgr().getFillAnswerService().getFAById(faId));
    } else {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "添加单选题");
    }
    return "/admin/adminSubject/adminFillAnswer/adminHandleFA";

  }

  @RequestMapping("doAdminHandleFA")
  public String doHandle(FillAnswer fa) {
    System.out.println(fa);
    if (fa.getFaId() == 0) {
      try {
        this.getServMgr().getFillAnswerService().addFA(fa);
        this.addMessage("添加多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getFillAnswerService().editFA(fa);
        this.addMessage("修改多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("修改多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @ResponseBody
  @RequestMapping("doAdminHandleFAAUTO")
  public String doAdminHandleFaAUTO(FillAnswer fa, @RequestParam MultipartFile[] Auto_FA, @RequestParam String courseId, HttpServletResponse response,
    HttpServletRequest request) throws Exception {
    JSONObject obj = new JSONObject();
    System.out.println("上传文件");
    Calendar now = Calendar.getInstance();
    System.out.println(now);
    System.out.println(courseId);
    String OriName = "";
    String DocName = "";
    String Docpath = "";
    String path = "";

    for (int i = 0; i < Auto_FA.length; i++) {
      System.out.println(Auto_FA[i]);
      OriName = Auto_FA[i].getOriginalFilename();
      System.out.println(OriName);
      DocName = "" + now.get(Calendar.YEAR) + (now.get(Calendar.MONTH) + 1) + now.get(Calendar.DAY_OF_MONTH) + OriName;
      System.out.println(DocName);
      Docpath = "/doc/" + DocName;
      System.out.println(Docpath);
      path = request.getServletContext().getRealPath(Docpath);
      System.out.println(path);
      File fileservlet = new File(path);
      System.out.println(fileservlet);
      String filelocal = "D://j2ee-project//Online_Examing_System//WebContent//doc//" + DocName;
      try {
        Auto_FA[i].transferTo(fileservlet);
        FileInputStream fi = new FileInputStream(fileservlet);
        FileOutputStream fo = new FileOutputStream(filelocal);
        byte[] b = new byte[fi.available()];//创建byte类型的数组
        fi.read(b);//将图形文件读入b数组    
        fo.write(b);//将b数组的数据写入新文件'copyScenery.jpg'
        fi.close();
        fo.close();
        System.out.println("上传文件成功，准备分析文件");
        //操作word
        if (OriName.endsWith("docx") || OriName.endsWith("doc")) {
          System.out.println("解析word开始--------");
          HandleWord hw = new HandleWord();

          //          for (XWPFParagraph para : hw.getAllParagraph(hw.readWord(fileservlet))) {
          //            System.out.println(hw.getParaText(para));
          //          }

          if (hw.getQuestionDataFA(hw.getAllParagraph(hw.readWord(fileservlet))).size() != 0) {
            System.out.println("word分析完成-----");
            List<List<Map<String, String>>> fadata = hw.getQuestionDataFA(hw.getAllParagraph(hw.readWord(fileservlet)));
            System.out.println(fadata);
            System.out.println("开始添加数据库------");
            for (int j = 0; j < fadata.size(); j++) {
              System.out.println("第" + (j + 1) + "道填空题-----");

              //题目 题型 填空1 2 答案1 2 分值 试题分类  
              fa.setFaName(fadata.get(j).get(0).get("题目"));
              fa.setFaResult1(fadata.get(j).get(0).get("答案1"));
              fa.setFaResult2(fadata.get(j).get(0).get("答案2"));
              fa.setFaScore(Integer.parseInt(fadata.get(j).get(0).get("分值")));
              //添加课程分类有问题 待解决  需要自动分配？ 缺失自动添加？ 目前是固定为前端传递的值

              //end
              try {
                this.getServMgr().getFillAnswerService().addFA(fa);
              } catch (Exception e) {
                e.printStackTrace();
              }

            }
          }

        } else {

        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    obj.put("status", "1");
    obj.put("id", "1");

    return obj.toString();

  }

  @RequestMapping("delFA")
  public String delFA(String faId) {
    try {
      this.getServMgr().getFillAnswerService().delFA(faId);
      this.addMessage("删除多选题成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("删除多选题失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("adminHandleFATOEQ")
  public String adminHandleSCTOEQ(String faId, Map<String, Object> m) {
    m.put("faId", faId);
    m.put("EQ", this.getServMgr().getEquestionService().getEQINList());
    return "/admin/adminSubject/adminFillAnswer/adminHandleFATOEQ";

  }

  @RequestMapping("doAdminHandleFATOEQ")
  public String doAdminHandleFATOEQ(Fa_Eq faeq) {
    try {
      System.out.println(faeq);
      this.getServMgr().getFa_EqService().saveFA(faeq);
      this.addMessage("添加单选题到题库成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("添加单选题到题库失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;

  }

}
