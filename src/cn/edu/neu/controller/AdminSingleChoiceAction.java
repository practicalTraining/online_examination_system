package cn.edu.neu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.core.util.HandleWord;
import cn.edu.neu.model.Sc_Eq;
import cn.edu.neu.model.SingleChoice;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("admin")
public class AdminSingleChoiceAction extends BaseAction {
  @RequestMapping("adminSingleChoice")
  public String adminSingleChoice(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "单选题");
    return "/admin/adminSubject/adminSingleChoice/adminSingleChoiceIndex";
  }

  @RequestMapping("getAdminSingleChoiceList")
  public String getAdminSCList(Map<String, Object> m) {
    Page<SingleChoice> sc = this.getServMgr().getSingleChoiceService().getAdminSingleChoiceList();
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    m.put("sc", sc);
    return "/admin/adminSubject/adminSingleChoice/adminSingleChoiceList";

  }

  @RequestMapping("adminHandleSC")
  public String handle(@RequestParam(required = false) String scId, Map<String, Object> m) {
    setReferUrl();
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    if (scId != null && scId != "") {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "修改单选题");
      m.put("sc", this.getServMgr().getSingleChoiceService().getSCById(scId));
    } else {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "添加单选题");
    }
    return "/admin/adminSubject/adminSingleChoice/adminHandleSC";
  }

  @RequestMapping("doAdminHandleSC")
  public String doHandle(SingleChoice sc) {
    System.out.println(sc);
    if (sc.getScId() == 0) {
      try {
        this.getServMgr().getSingleChoiceService().addSC(sc);
        this.addMessage("添加单选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加单选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getSingleChoiceService().editSC(sc);
        this.addMessage("修改单选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("修改单选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @ResponseBody
  @RequestMapping("doAdminHandleSCAUTO")
  public String doAdminHandleScAUTO(SingleChoice sc, @RequestParam MultipartFile[] Auto_SC, @RequestParam String courseId,
    HttpServletResponse response, HttpServletRequest request) throws Exception {
    JSONObject obj = new JSONObject();
    System.out.println("上传文件");
    Calendar now = Calendar.getInstance();
    System.out.println(now);
    System.out.println(courseId);
    String OriName = "";
    String DocName = "";
    String Docpath = "";
    String path = "";

    for (int i = 0; i < Auto_SC.length; i++) {
      System.out.println(Auto_SC[i]);
      OriName = Auto_SC[i].getOriginalFilename();
      System.out.println(OriName);
      DocName = "" + now.get(Calendar.YEAR) + (now.get(Calendar.MONTH) + 1) + now.get(Calendar.DAY_OF_MONTH) + OriName;
      System.out.println(DocName);
      Docpath = "/doc/" + DocName;
      System.out.println(Docpath);
      path = request.getServletContext().getRealPath(Docpath);
      System.out.println(path);
      File fileservlet = new File(path);
      System.out.println(fileservlet);
      String filelocal = "D://j2ee-project//Online_Examing_System//WebContent//doc//" + DocName;
      try {
        Auto_SC[i].transferTo(fileservlet);
        FileInputStream fi = new FileInputStream(fileservlet);
        FileOutputStream fo = new FileOutputStream(filelocal);
        byte[] b = new byte[fi.available()];//创建byte类型的数组
        fi.read(b);//将图形文件读入b数组    
        fo.write(b);//将b数组的数据写入新文件'copyScenery.jpg'
        fi.close();
        fo.close();
        System.out.println("上传文件成功，准备分析文件");
        //操作word
        if (OriName.endsWith("docx") || OriName.endsWith("doc")) {
          System.out.println("解析word开始--------");
          HandleWord hw = new HandleWord();

          //          for (XWPFParagraph para : hw.getAllParagraph(hw.readWord(fileservlet))) {
          //            System.out.println(hw.getParaText(para));
          //          }

          if (hw.getQuestionDataSC(hw.getAllParagraph(hw.readWord(fileservlet))).size() != 0) {
            System.out.println("word分析完成-----");
            List<List<Map<String, String>>> scdata = hw.getQuestionDataSC(hw.getAllParagraph(hw.readWord(fileservlet)));
            System.out.println("开始添加数据库------");
            for (int j = 0; j < scdata.size(); j++) {
              System.out.println("第" + (j + 1) + "道单选题-----");

              //题目 题型 答案 分值 试题分类  选项A-D
              sc.setScName(scdata.get(j).get(0).get("题目"));

              sc.setScResult(scdata.get(j).get(0).get("答案"));

              sc.setScScore(Integer.parseInt(scdata.get(j).get(0).get("分值")));

              //添加课程分类有问题 待解决  需要自动分配？ 缺失自动添加？ 目前是固定为前端传递的值

              //end
              sc.setOptionA(scdata.get(j).get(0).get("选项A"));
              sc.setOptionB(scdata.get(j).get(0).get("选项B"));
              sc.setOptionC(scdata.get(j).get(0).get("选项C"));
              sc.setOptionD(scdata.get(j).get(0).get("选项D"));
              try {
                this.getServMgr().getSingleChoiceService().addSC(sc);
              } catch (Exception e) {
                e.printStackTrace();
              }

            }
          }

        } else {

        }

      } catch (IOException e) {
        System.out.println(e.getMessage());
      }
    }

    obj.put("status", "1");
    obj.put("id", "1");

    return obj.toString();

  }

  @RequestMapping("delSC")
  public String delSC(String scId) {
    try {
      this.getServMgr().getSingleChoiceService().delSC(scId);
      this.addMessage("删除单选题成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("删除单选题失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("adminHandleSCTOEQ")
  public String adminHandleSCTOEQ(String scId, Map<String, Object> m) {
    m.put("scId", scId);
    m.put("EQ", this.getServMgr().getEquestionService().getEQINList());
    return "/admin/adminSubject/adminSingleChoice/adminHandleSCTOEQ";

  }

  @RequestMapping("doAdminHandleSCTOEQ")
  public String doAdminHandleSCTOEQ(Sc_Eq sceq) {
    try {
      System.out.println(sceq);
      this.getServMgr().getSc_eqService().saveSC(sceq);
      this.addMessage("添加单选题到题库成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("添加单选题到题库失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;

  }
}
