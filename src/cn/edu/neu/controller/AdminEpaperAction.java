package cn.edu.neu.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.EpaperSubject;
import cn.edu.neu.model.ExamPaper;

@Controller
@RequestMapping("admin")
public class AdminEpaperAction extends BaseAction {
  @RequestMapping("adminEpaper")
  public String getAdminEpaperIndex(Map<String, Object> m) {
    setReferUrl();

    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminEPaper/adminEpaperIndex";
  }

  @RequestMapping("adminGetEpaperList")
  public String adminGetEpaperList(Map<String, Object> m) {
    Page<ExamPaper> page = this.getServMgr().getExamPaperService().getexamPaperListwithCourse();
    m.put("Epaper", page);
    return "admin/adminExam/adminEPaper/adminEpaperList";
  }

  @RequestMapping("getAdminEpaperByCourseId")
  public String getAdminEpaperByCourseId(@RequestParam String courseId, @RequestParam(required = false) String sort, Map<String, Object> m) {
    m.put("Epaper", this.getServMgr().getExamPaperService().findExamPapersByCourseId(courseId, sort));
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminEPaper/adminEpaperIndex";
  }

  @RequestMapping("getAdminEpaperByKeyword")
  public String getAdminEpaperByKeyword(@RequestParam String keyword, @RequestParam(required = false) String sort, Map<String, Object> m) {
    m.put("Epaper", this.getServMgr().getExamPaperService().findExamPapersByCourseId(keyword, sort));
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminEPaper/adminEpaperIndex";
  }

  @RequestMapping("adminHandleEpaper")
  public String adminHandelEpaper(@RequestParam(required = false) String epaperId, @RequestParam(required = false) String courseId,
    Map<String, Object> m) {

    if (epaperId != null) {
      m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
      m.put("course", this.getServMgr().getCourseService().getCourseList());

    } else {
      if (courseId != null) {
        m.put("exam", this.getServMgr().getExamService().getExamByCourseId(courseId));
        return "admin/adminExam/adminEPaper/adminEpaperExam";
      }
      m.put("course", this.getServMgr().getCourseService().getCourseList());
    }

    return "admin/adminExam/adminEPaper/adminHandleEpaper";

  }

  @RequestMapping("adminSubjectList")
  public String adminSubjectList(@RequestParam(required = false) String epaperId, Map<String, Object> m) {
    int scScoreSUM = 0;
    int mcScoreSUM = 0;
    int jgScoreSUM = 0;
    int faScoreSUM = 0;
    m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
    m.put("epsc", this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtScId(epaperId));
    m.put("epmc", this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtMcId(epaperId));
    m.put("epjg", this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtJgId(epaperId));
    m.put("epfa", this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtFaId(epaperId));
    System.out.println(this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtScId(epaperId));
    System.out.println(this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtMcId(epaperId));
    for (int i = 0; i < this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtScId(epaperId).size(); i++) {
      scScoreSUM += this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtScId(epaperId).get(i).getSinglechoice().getScScore();
      if (i == this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtScId(epaperId).size() - 1) {
        m.put("scn", i + 1);
        System.out.println(i);
      }
    }

    for (int i = 0; i < this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtMcId(epaperId).size(); i++) {
      mcScoreSUM += this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtMcId(epaperId).get(i).getMultiplechoice().getMcScore();
      if (i == this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtMcId(epaperId).size() - 1) {
        m.put("mcn", i + 1);
        System.out.println(i);
      }
    }

    for (int i = 0; i < this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtJgId(epaperId).size(); i++) {
      jgScoreSUM += this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtJgId(epaperId).get(i).getJudgement().getJgScore();
      if (i == this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtJgId(epaperId).size() - 1) {
        m.put("jgn", i + 1);
        System.out.println(i);
      }
    }

    for (int i = 0; i < this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtFaId(epaperId).size(); i++) {
      faScoreSUM += this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtFaId(epaperId).get(i).getFillanswer().getFaScore();
      if (i == this.getServMgr().getEpaperSubjectService().getEpaperSubjectByepaperIdwihtFaId(epaperId).size() - 1) {
        m.put("fan", i + 1);
        System.out.println(i);
      }
    }
    m.put("scScoreSUM", scScoreSUM);
    m.put("mcScoreSUM", mcScoreSUM);
    m.put("jgScoreSUM", jgScoreSUM);
    m.put("faScoreSUM", faScoreSUM);

    return "admin/adminExam/adminEPaper/adminSubjectList";
  }

  @RequestMapping("adminHandleEpaperSC")
  public String adminHandleEpaperSC(@RequestParam(required = false) String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (epaperId != null) {
      m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
    }
    m.put("SC", this.getServMgr().getCourseService().getCourseAndEQSCBycourseId(courseId));
    return "admin/adminExam/adminEPaper/adminHandleEpaperSC";

  }

  @RequestMapping("adminHandleUnAddSc")
  public String adminHandelUnAddSc(@RequestParam String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (courseId != "" && epaperId != "") {
      m.put("scList", this.getServMgr().getSingleChoiceService().getScUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    }
    System.out.println(this.getServMgr().getSingleChoiceService().getScUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    return "admin/adminExam/adminEPaper/UnAddScList";
  }

  @ResponseBody
  @RequestMapping("doAdminHandleEpaperSC")
  public String doAdminHandleEpaperSC(@RequestParam String epaperId, EpaperSubject epaperSubject, Map<String, Object> m) {
    if (epaperSubject != null) {
      try {
        this.getServMgr().getEpaperSubjectService().addEpaperEQSC(epaperSubject);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failuer";
      }
    } else {
      return "failuer";
    }
  }

  @RequestMapping("adminHandleEpaperMC")
  public String adminHandleEpaperMC(@RequestParam(required = false) String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (epaperId != null) {
      m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
    }
    m.put("MC", this.getServMgr().getCourseService().getCourseAndEQMCBycourseId(courseId));
    return "admin/adminExam/adminEPaper/adminHandleEpaperMC";
  }

  @RequestMapping("adminHandleUnAddMc")
  public String adminHandelUnAddMc(@RequestParam String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (courseId != "" && epaperId != "") {
      m.put("mcList", this.getServMgr().getMultipleChoiceService().getMcUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    }
    System.out.println(this.getServMgr().getMultipleChoiceService().getMcUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    return "admin/adminExam/adminEPaper/UnAddMcList";
  }

  @ResponseBody
  @RequestMapping("doAdminHandleEpaperMC")
  public String doAdminHandleEpaperMc(@RequestParam String epaperId, EpaperSubject epaperSubject, Map<String, Object> m) {
    if (epaperSubject != null) {
      try {
        this.getServMgr().getEpaperSubjectService().addEpaperEQMC(epaperSubject);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failuer";
      }
    } else {
      return "failuer";
    }
  }

  @RequestMapping("adminHandleEpaperJG")
  public String adminHandleEpaperJG(@RequestParam(required = false) String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (epaperId != null) {
      m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
    }
    m.put("JG", this.getServMgr().getCourseService().getCourseAndEQJGBycourseId(courseId));
    return "admin/adminExam/adminEPaper/adminHandleEpaperJG";
  }

  @RequestMapping("adminHandleUnAddJg")
  public String adminHandelUnAddJg(@RequestParam String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (courseId != "" && epaperId != "") {
      m.put("jgList", this.getServMgr().getJudgeMentService().getJgUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    }
    System.out.println(this.getServMgr().getJudgeMentService().getJgUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    return "admin/adminExam/adminEPaper/UnAddJgList";
  }

  @ResponseBody
  @RequestMapping("doAdminHandleEpaperJG")
  public String doAdminHandleEpaperJG(@RequestParam String epaperId, EpaperSubject epaperSubject, Map<String, Object> m) {
    if (epaperSubject != null) {
      try {
        this.getServMgr().getEpaperSubjectService().addEpaperEQJG(epaperSubject);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failuer";
      }
    } else {
      return "failuer";
    }
  }

  @RequestMapping("adminHandleEpaperFA")
  public String adminHandleEpaperFA(@RequestParam(required = false) String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (epaperId != null) {
      m.put("ep", this.getServMgr().getExamPaperService().getExamPaperById(epaperId));
    }
    m.put("FA", this.getServMgr().getCourseService().getCourseAndEQFABycourseId(courseId));
    return "admin/adminExam/adminEPaper/adminHandleEpaperFA";
  }

  @RequestMapping("adminHandleUnAddFa")
  public String adminHandelUnAddFa(@RequestParam String epaperId, @RequestParam String courseId, Map<String, Object> m) {
    if (courseId != "" && epaperId != "") {
      m.put("faList", this.getServMgr().getFillAnswerService().getFaUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    }
    System.out.println(this.getServMgr().getFillAnswerService().getFaUnAddEpaper(Integer.parseInt(courseId), Integer.parseInt(epaperId)));
    return "admin/adminExam/adminEPaper/UnAddFaList";
  }

  @ResponseBody
  @RequestMapping("doAdminHandleEpaperFA")
  public String doAdminHandleEpaperFA(@RequestParam String epaperId, EpaperSubject epaperSubject, Map<String, Object> m) {
    if (epaperSubject != null) {
      try {
        this.getServMgr().getEpaperSubjectService().addEpaperEQFA(epaperSubject);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failuer";
      }
    } else {
      return "failuer";
    }
  }

  @ResponseBody
  @RequestMapping("delEpaperSubject")
  public String delEpaperSubject(EpaperSubject epaperSubject) {
    if (epaperSubject != null) {

      try {
        this.getServMgr().getEpaperSubjectService().delEpaperSubject(epaperSubject);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failure";
      }
    } else {
      return "failure";
    }
  }

  @ResponseBody
  @RequestMapping("doAdminHandleEpaper")
  public String doAdminHandelEpaper(ExamPaper epaper, Map<String, Object> m) {
    System.out.println(epaper);
    String flag = "";
    if (epaper.getEpaperId() != 0) {
      if (epaper.getExamId() != 0) {
        try {
          this.getServMgr().getExamPaperService().editExamPaper(epaper);
          flag = "success";
        } catch (Exception e) {
          e.printStackTrace();
          flag = "failure";
        }
      } else {
        try {
          this.getServMgr().getExamPaperService().editExamPaperWithoutExamId(epaper);
          flag = "success";
        } catch (Exception e) {
          e.printStackTrace();
          flag = "failure";
        }
      }
    }

    if (epaper.getEpaperId() == 0) {
      if (epaper.getExamId() != 0) {
        try {
          this.getServMgr().getExamPaperService().addExamPaper(epaper);
          flag = "success";
        } catch (Exception e) {
          e.printStackTrace();
          flag = "failure";
        }
      } else {
        try {
          this.getServMgr().getExamPaperService().addExamPaperWithoutExamId(epaper);
          flag = "success";
        } catch (Exception e) {
          e.printStackTrace();
          flag = "failure";
        }
      }
    }

    return flag;

  }

  @ResponseBody
  @RequestMapping("adminDelEpaper")
  public String adminDelEpaper(@RequestParam("epaperId") String epaperId) {
    if (epaperId != "") {
      try {
        this.getServMgr().getExamPaperService().delExamPaper(epaperId);
        return "success";
      } catch (Exception e) {
        e.printStackTrace();
        return "failure";
      }

    } else {
      return "failure";
    }
  }
}
