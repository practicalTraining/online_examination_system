package cn.edu.neu.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.Course_Class;

@Controller
@RequestMapping("admin")
public class AdminClassInfoAction extends BaseAction {
  @RequestMapping("adminClass")
  public String adminClassInfoIndex(Map<String, Object> m) {
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    System.out.println(this.getServMgr().getClassInfoService().getclassInfoList());
    m.put("classinfo", this.getServMgr().getClassInfoService().getclassInfoList());

    return "admin/adminClass/adminClassInfoIndex";
  }

  @RequestMapping("getAdminClassInfoDetail")
  public String getAdminClassInfoDetail(@RequestParam String classId, Map<String, Object> m) {
    System.out.println(this.getServMgr().getClassInfoService().getClassInfoById(classId));
    m.put("classinfo", this.getServMgr().getClassInfoService().getClassInfoById(classId));
    System.out.println(this.getServMgr().getClassInfoService().getClassExamInfoById(classId));
    m.put("examinfo", this.getServMgr().getClassInfoService().getClassExamInfoById(classId));
    return "admin/adminClass/adminClassInfoDetail";

  }

  @RequestMapping("adminHandleClass")
  public String adminHandleClass(@RequestParam(required = false) String classId, Map<String, Object> m) {
    if (classId != null) {

    }
    m.put("counsellor", this.getServMgr().getCounsellorServiece().getCounsellorList());
    return "admin/adminClass/adminHandleClass";

  }

  @RequestMapping("doAdminHandleClass")
  public String doAdminHandleClass(ClassInfo classInfo) {
    if (classInfo.getClassId() == 0) {
      try {
        this.getServMgr().getClassInfoService().addClassInfo(classInfo);
        this.addMessage("添加班级成功");
        this.addRedirURL("返回", "/admin/adminClass");
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加班级失败");
        this.addRedirURL("返回", "/admin/adminHandleClass");
      }
    } else {

    }
    return EXECUTE_RESULT;

  }

  @RequestMapping("adminHandleCourseToClass")
  public String handleCourseToClass(@RequestParam String classId, Map<String, Object> m) {
    m.put("classinfo", this.getServMgr().getClassInfoService().getClassInfoById(classId));
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    m.put("ClassCourse", this.getServMgr().getCourse_ClassService().getClass_CourseIdByClassId(classId));
    return "admin/adminClass/adminHandleCourseToClass";
  }

  @RequestMapping("adminhandelClass_Course")
  public String adminhandelClass_Course(Course_Class course_Class) {
    System.out.println(course_Class);
    try {
      this.getServMgr().getCourse_ClassService().addClass_Course(course_Class);
      this.addMessage("添加成功");
      this.addRedirURL("返回", "/admin/adminHandleCourseToClass?classId=" + course_Class.getClassId());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("添加失败");
      this.addRedirURL("返回", "/admin/adminHandleCourseToClass?classId=" + course_Class.getClassId());
    }
    return EXECUTE_RESULT;

  }
}
