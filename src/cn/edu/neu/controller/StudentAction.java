package cn.edu.neu.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.neu.model.ClassInfo;
import cn.edu.neu.model.ExamHistory;
import cn.edu.neu.model.Student;

@Controller
@RequestMapping("/student")
public class StudentAction extends BaseAction {
  @RequestMapping("/studentIndex")
  private String studentIndex(Map<String, Object> m, HttpSession session) {
    String studentId = session.getAttribute("student_Id").toString();
    Student stu = this.getServMgr().getStudentService().getStudentById(studentId);
    m.put("stuinfo", stu);

    if (stu.getClassId() != 0) {
      List<ExamHistory> eh = this.getServMgr().getExamHistoryService().getExistExamHistory(studentId);
      List<ClassInfo> exam = this.getServMgr().getClassInfoService().getClassExamInfoById(stu.getClassId() + "");
      m.put("examinfo", exam);
      m.put("existExaminfo", eh);
      System.out.println(exam);
    }
    return "student/studentIndex";
  }

  @RequestMapping("/handelStuExam")
  public String handelStuExam(@RequestParam String studentId, @RequestParam String examId, Map<String, Object> m) {
    System.out.println(this.getServMgr().getExamService().getExamScByExamId(examId).getEpaper().getEpaperSubject().get(0).getSinglechoice());
    m.put("examSC", this.getServMgr().getExamService().getExamScByExamId(examId));
    return "student/studentExamInfo";
  }

  //管理员
  @RequestMapping("adminStudent")
  public String getAdminStudentIndex(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "查看学生");
    return "admin/adminStudent/adminStudentIndex";
  }
}
