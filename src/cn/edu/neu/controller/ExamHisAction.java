package cn.edu.neu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.neu.model.ExamHisSCResultList;
import cn.edu.neu.model.ExamHistory;

@Controller
@RequestMapping("examHis")
public class ExamHisAction extends BaseAction {
  @RequestMapping("handleExamHis")
  public String handleExamHis(ExamHisSCResultList sc, @RequestParam String[] T_scResult, ExamHistory examHistory) {
    int score = 0;
    for (int i = 0; i < T_scResult.length; i++) {
      if (sc.getSc().get(i).getScResult().equals(T_scResult[i])) {
        score += 2;
        examHistory.setExamHisScore(score);
      }
    }

    this.getServMgr().getExamHistoryService().addExamHistory(examHistory);
    return "/exam/ExamSuccess";
  }
}
