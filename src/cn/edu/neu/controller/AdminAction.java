package cn.edu.neu.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.model.Admin;

@Controller
@RequestMapping("admin")
public class AdminAction extends BaseAction {
  @RequestMapping("login")
  public String adminLogin(Admin admin, HttpSession session) {
    if (this.getServMgr().getAdminService().adminLogin(admin) != null) {
      session.setAttribute("admin_Name", admin.getAdminName());
      return "redirect:/admin/home";
    } else {
      this.addMessage("登陆失败，请检查用户名密码是否正确！");
      this.addRedirURL("返回", getReferUrl());
      System.out.println(getReferUrl() + "------" + EXECUTE_RESULT);
      return EXECUTE_RESULT;
    }

  }

  @RequestMapping("logout")
  public String adminLogout(HttpSession session) {
    session.invalidate();
    return "redirect:/admin/index";

  }

  @RequestMapping("home")
  public String adminHome(Map<String, Object> m) {
    setReferUrl();
    return "admin/home";
  }
}
