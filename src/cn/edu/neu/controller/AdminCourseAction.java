package cn.edu.neu.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.edu.neu.model.Course;

@Controller
@RequestMapping("admin")
public class AdminCourseAction extends BaseAction {
  @RequestMapping("adminCourse")
  public String getAdminCourseIndex(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "课程");
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminCourse/adminCourseIndex";
  }

  @RequestMapping("adminHandleCourse")
  public String adminHandleCourse(@RequestParam(required = false) String courseId, Map<String, Object> m) {
    if (courseId != null & courseId != " ") {
      m.put("course", this.getServMgr().getCourseService().getCourseById(courseId));
    }
    return "admin/adminCourse/adminHandleCourse";
  }

  @RequestMapping("doAdminHandleCourse")
  public String doAdminHandleCourse(Course course) {
    if (course.getCourseId() == 0) {
      try {
        this.getServMgr().getCourseService().addCourse(course);
        this.addMessage("添加课程成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        this.addMessage("添加课程失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getCourseService().editCourse(course);
        this.addMessage("修改课程成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        this.addMessage("修改课程失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("delCourse")
  public String delCourse(String courseId) {
    try {
      this.getServMgr().getCourseService().delCourse(courseId);
      this.addMessage("删除课程成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      this.addMessage("删除课程失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;
  }
}
