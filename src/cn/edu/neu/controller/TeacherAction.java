package cn.edu.neu.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TeacherAction extends BaseAction {
  @RequestMapping("adminTeacher")
  public String getAdminTeacherIndex(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "查看老师");
    return "admin/adminTeacher/adminTeacherIndex";
  }
}
