package cn.edu.neu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.core.util.HandleWord;
import cn.edu.neu.model.Mc_Eq;
import cn.edu.neu.model.MultipleChoice;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("admin")
public class AdminMultipleChoiceAction extends BaseAction {
  @RequestMapping("adminMultipleChoice")
  public String adminMultipleChoice(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "多选题");
    return "/admin/adminSubject/adminMultipleChoice/adminMultipleChoiceIndex";
  }

  @RequestMapping("getAdminMultipleChoiceList")
  public String getAdminMCList(Map<String, Object> m) {
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    List<MultipleChoice> mc = this.getServMgr().getMultipleChoiceService().getMcList();
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    m.put("mc", mc);
    System.out.println(mc);
    return "/admin/adminSubject/adminMultipleChoice/adminMultipleChoiceList";

  }

  @RequestMapping("adminHandleMC")
  public String getAdminMcList(@RequestParam(required = false) String mcId, Map<String, Object> m) {
    setReferUrl();
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    if (mcId != null && mcId != "") {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "修改单选题");
      m.put("mc", this.getServMgr().getMultipleChoiceService().getMcById(mcId));
    } else {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "添加单选题");
    }
    return "/admin/adminSubject/adminMultipleChoice/adminHandleMC";

  }

  @RequestMapping("doAdminHandleMC")
  public String doHandle(MultipleChoice mc) {
    System.out.println(mc);
    if (mc.getMcId() == 0) {
      try {
        this.getServMgr().getMultipleChoiceService().addMc(mc);
        this.addMessage("添加多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getMultipleChoiceService().editMc(mc);
        this.addMessage("修改多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("修改多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @ResponseBody
  @RequestMapping("doAdminHandleMCAUTO")
  public String doAdminHandleMcAUTO(MultipleChoice mc, @RequestParam MultipartFile[] Auto_MC, @RequestParam String courseId,
    HttpServletResponse response, HttpServletRequest request) throws Exception {
    JSONObject obj = new JSONObject();
    System.out.println("上传文件");
    Calendar now = Calendar.getInstance();
    System.out.println(now);
    System.out.println(courseId);
    String OriName = "";
    String DocName = "";
    String Docpath = "";
    String path = "";

    for (int i = 0; i < Auto_MC.length; i++) {
      System.out.println(Auto_MC[i]);
      OriName = Auto_MC[i].getOriginalFilename();
      System.out.println(OriName);
      DocName = "" + now.get(Calendar.YEAR) + (now.get(Calendar.MONTH) + 1) + now.get(Calendar.DAY_OF_MONTH) + OriName;
      System.out.println(DocName);
      Docpath = "/doc/" + DocName;
      System.out.println(Docpath);
      path = request.getServletContext().getRealPath(Docpath);
      System.out.println(path);
      File fileservlet = new File(path);
      System.out.println(fileservlet);
      String filelocal = "D://j2ee-project//Online_Examing_System//WebContent//doc//" + DocName;
      try {
        Auto_MC[i].transferTo(fileservlet);
        FileInputStream fi = new FileInputStream(fileservlet);
        FileOutputStream fo = new FileOutputStream(filelocal);
        byte[] b = new byte[fi.available()];//创建byte类型的数组
        fi.read(b);//将图形文件读入b数组    
        fo.write(b);//将b数组的数据写入新文件'copyScenery.jpg'
        fi.close();
        fo.close();
        System.out.println("上传文件成功，准备分析文件");
        //操作word
        if (OriName.endsWith("docx") || OriName.endsWith("doc")) {
          System.out.println("解析word开始--------");
          HandleWord hw = new HandleWord();

          //          for (XWPFParagraph para : hw.getAllParagraph(hw.readWord(fileservlet))) {
          //            System.out.println(hw.getParaText(para));
          //          }

          if (hw.getQuestionDataMC(hw.getAllParagraph(hw.readWord(fileservlet))).size() != 0) {
            System.out.println("word分析完成-----");
            List<List<Map<String, String>>> mcdata = hw.getQuestionDataMC(hw.getAllParagraph(hw.readWord(fileservlet)));
            System.out.println(mcdata);
            System.out.println("开始添加数据库------");
            for (int j = 0; j < mcdata.size(); j++) {
              System.out.println("第" + (j + 1) + "道多选题-----");

              //题目 题型 答案A-C c可以为空 分值 试题分类  选项A-E e可以为空
              mc.setMcName(mcdata.get(j).get(0).get("题目"));
              mc.setMcResult1(mcdata.get(j).get(0).get("答案1"));
              mc.setMcResult2(mcdata.get(j).get(0).get("答案2"));
              mc.setMcResult3(mcdata.get(j).get(0).get("答案3"));
              mc.setOptionA(mcdata.get(j).get(0).get("选项A"));
              mc.setOptionB(mcdata.get(j).get(0).get("选项B"));
              mc.setOptionC(mcdata.get(j).get(0).get("选项C"));
              mc.setOptionD(mcdata.get(j).get(0).get("选项D"));
              mc.setOptionE(mcdata.get(j).get(0).get("选项E"));
              mc.setMcScore(Integer.parseInt(mcdata.get(j).get(0).get("分值")));
              //添加课程分类有问题 待解决  需要自动分配？ 缺失自动添加？ 目前是固定为前端传递的值

              //end
              try {
                this.getServMgr().getMultipleChoiceService().addMc(mc);
              } catch (Exception e) {
                e.printStackTrace();
              }

            }
          }

        } else {

        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    obj.put("status", "1");
    obj.put("id", "1");

    return obj.toString();

  }

  @RequestMapping("delMC")
  public String delMC(String mcId) {
    try {
      this.getServMgr().getMultipleChoiceService().delMc(mcId);
      this.addMessage("删除多选题成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("删除多选题失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("adminHandleMCTOEQ")
  public String adminHandleSCTOEQ(String mcId, Map<String, Object> m) {
    m.put("mcId", mcId);
    m.put("EQ", this.getServMgr().getEquestionService().getEQINList());
    return "/admin/adminSubject/adminMultipleChoice/adminHandleMCTOEQ";

  }

  @RequestMapping("doAdminHandleMCTOEQ")
  public String doAdminHandleMCTOEQ(Mc_Eq mceq) {
    try {
      System.out.println(mceq);
      this.getServMgr().getMc_EqService().saveMC(mceq);
      this.addMessage("添加单选题到题库成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("添加单选题到题库失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;

  }
}
