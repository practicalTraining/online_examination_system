package cn.edu.neu.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.edu.neu.core.common.Page;
import cn.edu.neu.model.Equestion;
import cn.edu.neu.model.Fa_Eq;
import cn.edu.neu.model.Judge_Eq;
import cn.edu.neu.model.Mc_Eq;
import cn.edu.neu.model.Sc_Eq;

@Controller
@RequestMapping("admin")
public class AdminEquestionAction extends BaseAction {
  @RequestMapping("adminEquestion")
  public String adminEquestion(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "题库");
    Page<Equestion> list = this.getServMgr().getEquestionService().getAdminEQList();
    m.put("EQ", list);
    return "admin/adminExam/adminEquestion/adminEquestionIndex";
  }

  @RequestMapping("adminHandleEQ")
  public String adminHandleEQ(@RequestParam(required = false) String equestionId, Map<String, Object> m) {
    if (equestionId != null & equestionId != " ") {
      m.put("EQ", this.getServMgr().getEquestionService().getEQById(equestionId));
    }
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    return "admin/adminExam/adminEquestion/adminHandleEQ";
  }

  @RequestMapping("doAdminHandleEQ")
  public String doAdminHandleEQ(Equestion eq) {
    if (eq.getEquestionId() == 0) {
      try {
        this.getServMgr().getEquestionService().addEQ(eq);
        this.addMessage("添加题库成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加题库失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getEquestionService().editEQ(eq);
        this.addMessage("修改题库成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("修改题库失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("delEQ")
  public String delEQ(String equestionId) {
    try {
      this.getServMgr().getEquestionService().delEQ(equestionId);
      this.addMessage("删除题库成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("删除题库失败");
      this.addRedirURL("返回", getReferUrl());
    }

    return EXECUTE_RESULT;
  }

  @RequestMapping("adminEquestionDetails")
  public String adminEquestionDetails(String equestionId, Map<String, Object> m) {
    if (equestionId != null) {
      return "admin/adminExam/adminEquestion/adminEquestionDetails";
    } else {
      return "failure";
    }
  }

  @RequestMapping("getEQSCList")
  public String getEQSCList(String equestionId, Map<String, Object> m) {
    if (equestionId != null) {
      m.put("eqallsc", this.getServMgr().getEquestionService().getEQAllSCById(equestionId));
      return "admin/adminExam/adminEquestion/adminSingleChoiceList";
    } else
      return "failure";
  }

  @RequestMapping("getEQMCList")
  public String getEQMCList(String equestionId, Map<String, Object> m) {
    if (equestionId != null) {
      m.put("eqallmc", this.getServMgr().getEquestionService().getEQAllMCById(equestionId));
      return "admin/adminExam/adminEquestion/adminMultipleChoiceList";
    } else
      return "failure";
  }

  @RequestMapping("getEQFAList")
  public String getEQFAList(String equestionId, Map<String, Object> m) {
    if (equestionId != null) {
      m.put("eqallfa", this.getServMgr().getEquestionService().getEQAllFAById(equestionId));
      return "admin/adminExam/adminEquestion/adminFillAnswerList";
    } else
      return "failure";
  }

  @RequestMapping("getEQJGList")
  public String getEQJGList(String equestionId, Map<String, Object> m) {
    if (equestionId != null) {
      m.put("eqalljg", this.getServMgr().getEquestionService().getEQAllJGById(equestionId));
      return "admin/adminExam/adminEquestion/adminJudgeMentList";
    } else
      return "failure";
  }

  @ResponseBody
  @RequestMapping("delScInEQ")
  public String delScInEQ(Sc_Eq sceq) {
    if (sceq != null) {
      this.getServMgr().getSc_eqService().delSC(sceq);
      return "success";
    } else {
      return "failure";
    }

  }

  @ResponseBody
  @RequestMapping("delMcInEQ")
  public String delMcInEQ(Mc_Eq mceq) {
    if (mceq != null) {
      this.getServMgr().getMc_EqService().delMC(mceq);
      return "success";
    } else {
      return "failure";
    }

  }

  @ResponseBody
  @RequestMapping("delFaInEQ")
  public String delFaInEQ(Fa_Eq faeq) {
    if (faeq != null) {
      this.getServMgr().getFa_EqService().delFA(faeq);
      return "success";
    } else {
      return "failure";
    }

  }

  @ResponseBody
  @RequestMapping("delJgInEQ")
  public String delJgInEQ(Judge_Eq jgeq) {
    if (jgeq != null) {
      this.getServMgr().getJudge_EqService().delJG(jgeq);
      return "success";
    } else {
      return "failure";
    }

  }
}
