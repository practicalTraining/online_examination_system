package cn.edu.neu.controller;

import javax.annotation.Resource;

import cn.edu.neu.core.common.CommonBaseAction;
import cn.edu.neu.service.ServiceManager;

public class BaseAction extends CommonBaseAction {
  @Resource(name = "servMgr")
  private ServiceManager servMgr;

  public ServiceManager getServMgr() {
    return servMgr;
  }

}
