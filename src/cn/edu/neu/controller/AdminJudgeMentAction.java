package cn.edu.neu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.core.util.HandleWord;
import cn.edu.neu.model.JudgeMent;
import cn.edu.neu.model.Judge_Eq;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("admin")
public class AdminJudgeMentAction extends BaseAction {
  @RequestMapping("adminJudgeMent")
  public String adminJudgeMent(Map<String, Object> m) {
    setReferUrl();
    m.put("breadcrumb_1", "判断题");
    return "/admin/adminSubject/adminJudgeMent/adminJudgeMentIndex";
  }

  @RequestMapping("getAdminJudgeMentList")
  public String getAdminJGList(Map<String, Object> m) {
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    List<JudgeMent> jg = this.getServMgr().getJudgeMentService().getJudgeMentList();

    m.put("course", this.getServMgr().getCourseService().getCourseList());
    m.put("jg", jg);
    System.out.println(jg);
    return "/admin/adminSubject/adminJudgeMent/adminJudgeMentList";

  }

  @RequestMapping("adminHandleJG")
  public String getAdminJgList(@RequestParam(required = false) String jgId, Map<String, Object> m) {
    setReferUrl();
    System.out.println(this.getServMgr().getCourseService().getCourseList().toString());
    m.put("course", this.getServMgr().getCourseService().getCourseList());
    if (jgId != null && jgId != "") {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "修改单选题");
      m.put("jg", this.getServMgr().getJudgeMentService().getJGById(jgId));
    } else {
      m.put("breadcrumb_1", "单选题");
      m.put("breadcrumb_2", "添加单选题");
    }
    return "/admin/adminSubject/adminJudgeMent/adminHandleJG";

  }

  @RequestMapping("doAdminHandleJG")
  public String doHandle(JudgeMent jg) {
    System.out.println(jg);
    if (jg.getJgId() == 0) {
      try {
        this.getServMgr().getJudgeMentService().addJG(jg);
        this.addMessage("添加多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("添加多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    } else {
      try {
        this.getServMgr().getJudgeMentService().editJG(jg);
        this.addMessage("修改多选题成功");
        this.addRedirURL("返回", getReferUrl());
      } catch (Exception e) {
        e.printStackTrace();
        this.addMessage("修改多选题失败");
        this.addRedirURL("返回", getReferUrl());
      }
    }
    return EXECUTE_RESULT;
  }

  @ResponseBody
  @RequestMapping("doAdminHandleJGAUTO")
  public String doAdminHandleJgAUTO(JudgeMent jg, @RequestParam MultipartFile[] Auto_JG, @RequestParam String courseId, HttpServletResponse response,
    HttpServletRequest request) throws Exception {
    JSONObject obj = new JSONObject();
    System.out.println("上传文件");
    Calendar now = Calendar.getInstance();
    System.out.println(now);
    System.out.println(courseId);
    String OriName = "";
    String DocName = "";
    String Docpath = "";
    String path = "";

    for (int i = 0; i < Auto_JG.length; i++) {
      System.out.println(Auto_JG[i]);
      OriName = Auto_JG[i].getOriginalFilename();
      System.out.println(OriName);
      DocName = "" + now.get(Calendar.YEAR) + (now.get(Calendar.MONTH) + 1) + now.get(Calendar.DAY_OF_MONTH) + OriName;
      System.out.println(DocName);
      Docpath = "/doc/" + DocName;
      System.out.println(Docpath);
      path = request.getServletContext().getRealPath(Docpath);
      System.out.println(path);
      File fileservlet = new File(path);
      System.out.println(fileservlet);
      String filelocal = "D://j2ee-project//Online_Examing_System//WebContent//doc//" + DocName;
      try {
        Auto_JG[i].transferTo(fileservlet);
        FileInputStream fi = new FileInputStream(fileservlet);
        FileOutputStream fo = new FileOutputStream(filelocal);
        byte[] b = new byte[fi.available()];//创建byte类型的数组
        fi.read(b);//将图形文件读入b数组    
        fo.write(b);//将b数组的数据写入新文件'copyScenery.jpg'
        fi.close();
        fo.close();
        System.out.println("上传文件成功，准备分析文件");
        //操作word
        if (OriName.endsWith("docx") || OriName.endsWith("doc")) {
          System.out.println("解析word开始--------");
          HandleWord hw = new HandleWord();

          //          for (XWPFParagraph para : hw.getAllParagraph(hw.readWord(fileservlet))) {
          //            System.out.println(hw.getParaText(para));
          //          }

          if (hw.getQuestionDataJG(hw.getAllParagraph(hw.readWord(fileservlet))).size() != 0) {
            System.out.println("word分析完成-----");
            List<List<Map<String, String>>> jgdata = hw.getQuestionDataJG(hw.getAllParagraph(hw.readWord(fileservlet)));
            System.out.println(jgdata);
            System.out.println("开始添加数据库------");
            for (int j = 0; j < jgdata.size(); j++) {
              System.out.println("第" + (j + 1) + "道判断题-----");

              //题目 题型 答案_ _ _ 分值 试题分类  
              jg.setJgName(jgdata.get(j).get(0).get("题目"));
              jg.setJgResult(jgdata.get(j).get(0).get("答案"));
              jg.setJgScore(Integer.parseInt(jgdata.get(j).get(0).get("分值")));
              //添加课程分类有问题 待解决  需要自动分配？ 缺失自动添加？ 目前是固定为前端传递的值

              //end
              try {
                this.getServMgr().getJudgeMentService().addJG(jg);
              } catch (Exception e) {
                e.printStackTrace();
              }

            }
          }

        } else {

        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    obj.put("status", "1");
    obj.put("id", "1");

    return obj.toString();

  }

  @RequestMapping("delJG")
  public String delJG(String jgId) {
    try {
      this.getServMgr().getJudgeMentService().delJG(jgId);
      this.addMessage("删除多选题成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("删除多选题失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;
  }

  @RequestMapping("adminHandleJGTOEQ")
  public String adminHandleSCTOEQ(String jgId, Map<String, Object> m) {
    m.put("jgId", jgId);
    m.put("EQ", this.getServMgr().getEquestionService().getEQINList());
    return "/admin/adminSubject/adminJudgeMent/adminHandleJGTOEQ";

  }

  @RequestMapping("doAdminHandleJGTOEQ")
  public String doAdminHandleJGTOEQ(Judge_Eq jgeq) {
    try {
      System.out.println(jgeq);
      this.getServMgr().getJudge_EqService().saveJG(jgeq);
      this.addMessage("添加单选题到题库成功");
      this.addRedirURL("返回", getReferUrl());
    } catch (Exception e) {
      e.printStackTrace();
      this.addMessage("添加单选题到题库失败");
      this.addRedirURL("返回", getReferUrl());
    }
    return EXECUTE_RESULT;

  }

}
