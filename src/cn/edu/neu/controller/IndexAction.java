package cn.edu.neu.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexAction extends BaseAction {

  @RequestMapping({ "index", "/" })
  public String index() {
    setReferUrl();
    return "/index";
  }

  @RequestMapping({ "admin/index", "admin/" })
  public String adminIndex() {
    setReferUrl();
    System.out.println(getReferUrl());
    return "admin/index";
  }

  @RequestMapping("logout")
  public String logout(HttpSession session) {
    session.invalidate();
    return "redirect:/index";
  }
}
