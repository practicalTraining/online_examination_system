package cn.edu.neu.model;

import java.util.List;

public class ClassInfo {
  private int classId;

  private int counsellorId;

  private String className;

  private Course_Class course_Class;

  private Counsellor counsellor;

  private List<Course> course;

  private Exam_Class exam_Class;

  private Teacher_Class tc;

  private List<Exam> exam;

  private List<Student> student;

  private List<Teacher> teacher;

  public ClassInfo() {
    super();
  }

  public ClassInfo(int classId, int counsellorId, String className, Course_Class course_Class, Counsellor counsellor, List<Course> course,
    Exam_Class exam_Class, Teacher_Class tc, List<Exam> exam, List<Student> student, List<Teacher> teacher) {
    super();
    this.classId = classId;
    this.counsellorId = counsellorId;
    this.className = className;
    this.course_Class = course_Class;
    this.counsellor = counsellor;
    this.course = course;
    this.exam_Class = exam_Class;
    this.tc = tc;
    this.exam = exam;
    this.student = student;
    this.teacher = teacher;
  }

  public int getClassId() {
    return classId;
  }

  public void setClassId(int classId) {
    this.classId = classId;
  }

  public int getCounsellorId() {
    return counsellorId;
  }

  public void setCounsellorId(int counsellorId) {
    this.counsellorId = counsellorId;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public List<Course> getCourse() {
    return course;
  }

  public void setCourse(List<Course> course) {
    this.course = course;
  }

  public Course_Class getCourse_Class() {
    return course_Class;
  }

  public void setCourse_Class(Course_Class course_Class) {
    this.course_Class = course_Class;
  }

  public Counsellor getCounsellor() {
    return counsellor;
  }

  public void setCounsellor(Counsellor counsellor) {
    this.counsellor = counsellor;
  }

  public Exam_Class getExam_Class() {
    return exam_Class;
  }

  public void setExam_Class(Exam_Class exam_Class) {
    this.exam_Class = exam_Class;
  }

  public List<Exam> getExam() {
    return exam;
  }

  public void setExam(List<Exam> exam) {
    this.exam = exam;
  }

  public List<Student> getStudent() {
    return student;
  }

  public void setStudent(List<Student> student) {
    this.student = student;
  }

  public List<Teacher> getTeacher() {
    return teacher;
  }

  public void setTeacher(List<Teacher> teacher) {
    this.teacher = teacher;
  }

  public Teacher_Class getTc() {
    return tc;
  }

  public void setTc(Teacher_Class tc) {
    this.tc = tc;
  }

  @Override
  public String toString() {
    return "ClassInfo [classId=" + classId + ", counsellorId=" + counsellorId + ", className=" + className + ", course_Class=" + course_Class
      + ", counsellor=" + counsellor + ", course=" + course + ", exam_Class=" + exam_Class + ", tc=" + tc + ", exam=" + exam + ", student=" + student
      + ", teacher=" + teacher + "]";
  }

}
