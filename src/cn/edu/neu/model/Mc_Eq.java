package cn.edu.neu.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Mc_Eq {
  private int mcId;

  private int eqId;

  @DateTimeFormat(pattern = " yyyy-MM-dd HH:mm:ss")
  private Date cTime;

  public Mc_Eq(int mcId, int eqId, Date cTime) {
    super();
    this.mcId = mcId;
    this.eqId = eqId;
    this.cTime = cTime;
  }

  public Mc_Eq() {
    super();
  }

  public int getMcId() {
    return mcId;
  }

  public void setMcId(int mcId) {
    this.mcId = mcId;
  }

  public int getEqId() {
    return eqId;
  }

  public void setEqId(int eqId) {
    this.eqId = eqId;
  }

  public Date getcTime() {
    return cTime;
  }

  public void setcTime(Date cTime) {
    this.cTime = cTime;
  }

  @Override
  public String toString() {
    return "Mc_Eq [mcId=" + mcId + ", eqId=" + eqId + ", cTime=" + cTime + "]";
  }

}
