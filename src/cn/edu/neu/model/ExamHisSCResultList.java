package cn.edu.neu.model;

import java.util.List;

public class ExamHisSCResultList {

  private List<SingleChoice> sc;

  public List<SingleChoice> getSc() {
    return sc;
  }

  public void setSc(List<SingleChoice> sc) {
    this.sc = sc;
  }

  @Override
  public String toString() {
    return "ExamHisSCResultList [sc=" + sc + "]";
  }

}
