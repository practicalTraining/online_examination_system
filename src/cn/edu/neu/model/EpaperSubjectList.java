package cn.edu.neu.model;

import java.util.List;

public class EpaperSubjectList {
  private List<EpaperSubject> epaperSubject;

  public List<EpaperSubject> getEpaperSubject() {
    return epaperSubject;
  }

  public void setEpaperSubject(List<EpaperSubject> epaperSubject) {
    this.epaperSubject = epaperSubject;
  }

  @Override
  public String toString() {
    return "EpaperSubjectList [epaperSubject=" + epaperSubject + "]";
  }

}
