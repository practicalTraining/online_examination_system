package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class ExamPaper {
  private int epaperId;

  private int examId;

  private int courseId;

  private String epaperName;

  private int epaperScore;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date epaperCtime;

  private Course course;

  private List<Exam> exam;

  private List<EpaperSubject> epaperSubject;

  private List<Equestion> equestion;

  public ExamPaper() {
    super();
  }

  public ExamPaper(int epaperId, int examId, int courseId, String epaperName, int epaperScore, Date epaperCtime, Course course, List<Exam> exam,
    List<EpaperSubject> epaperSubject, List<Equestion> equestion) {
    super();
    this.epaperId = epaperId;
    this.examId = examId;
    this.courseId = courseId;
    this.epaperName = epaperName;
    this.epaperScore = epaperScore;
    this.epaperCtime = epaperCtime;
    this.course = course;
    this.exam = exam;
    this.epaperSubject = epaperSubject;
    this.equestion = equestion;
  }

  public int getEpaperId() {
    return epaperId;
  }

  public void setEpaperId(int epaperId) {
    this.epaperId = epaperId;
  }

  public int getExamId() {
    return examId;
  }

  public void setExamId(int examId) {
    this.examId = examId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public String getEpaperName() {
    return epaperName;
  }

  public void setEpaperName(String epaperName) {
    this.epaperName = epaperName;
  }

  public int getEpaperScore() {
    return epaperScore;
  }

  public void setEpaperScore(int epaperScore) {
    this.epaperScore = epaperScore;
  }

  public Date getEpaperCtime() {
    return epaperCtime;
  }

  public void setEpaperCtime(Date epaperCtime) {
    this.epaperCtime = epaperCtime;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public List<Exam> getExam() {
    return exam;
  }

  public void setExam(List<Exam> exam) {
    this.exam = exam;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public List<EpaperSubject> getEpaperSubject() {
    return epaperSubject;
  }

  public void setEpaperSubject(List<EpaperSubject> epaperSubject) {
    this.epaperSubject = epaperSubject;
  }

  @Override
  public String toString() {
    return "ExamPaper [epaperId=" + epaperId + ", examId=" + examId + ", courseId=" + courseId + ", epaperName=" + epaperName + ", epaperScore="
      + epaperScore + ", epaperCtime=" + epaperCtime + ", course=" + course + ", exam=" + exam + ", epaperSubject=" + epaperSubject + ", equestion="
      + equestion + "]";
  }

}
