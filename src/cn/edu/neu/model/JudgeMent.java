package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class JudgeMent {
  private int jgId;

  private String jgName;

  private String jgResult;

  private int jgScore;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date jgEntrydate;

  private int subjectTypeId;

  private int courseId;

  private List<Equestion> equestion;

  private Judge_Eq jg_eq;

  public JudgeMent() {
    super();
  }

  public int getJdgId() {
    return jgId;
  }

  public JudgeMent(int jgId, String jgName, String jgResult, int jgScore, Date jgEntrydate, int subjectTypeId, int courseId,
    List<Equestion> equestion, Judge_Eq jg_eq) {
    super();
    this.jgId = jgId;
    this.jgName = jgName;
    this.jgResult = jgResult;
    this.jgScore = jgScore;
    this.jgEntrydate = jgEntrydate;
    this.subjectTypeId = subjectTypeId;
    this.courseId = courseId;
    this.equestion = equestion;
    this.jg_eq = jg_eq;
  }

  public void setJgId(int jgId) {
    this.jgId = jgId;
  }

  public int getJgId() {
    return jgId;
  }

  public String getJgName() {
    return jgName;
  }

  public void setJgName(String jgName) {
    this.jgName = jgName;
  }

  public String getJgResult() {
    return jgResult;
  }

  public void setJgResult(String jgResult) {
    this.jgResult = jgResult;
  }

  public int getJgScore() {
    return jgScore;
  }

  public void setJgScore(int jgScore) {
    this.jgScore = jgScore;
  }

  public Date getJgEntrydate() {
    return jgEntrydate;
  }

  public void setJgEntrydate(Date jgEntrydate) {
    this.jgEntrydate = jgEntrydate;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public Judge_Eq getJg_eq() {
    return jg_eq;
  }

  public void setJg_eq(Judge_Eq jg_eq) {
    this.jg_eq = jg_eq;
  }

  @Override
  public String toString() {
    return "JudgeMent [jgId=" + jgId + ", jgName=" + jgName + ", jgResult=" + jgResult + ", jgScore=" + jgScore + ", jgEntrydate=" + jgEntrydate
      + ", subjectTypeId=" + subjectTypeId + ", courseId=" + courseId + ", equestion=" + equestion + ", jg_eq=" + jg_eq + "]";
  }

}
