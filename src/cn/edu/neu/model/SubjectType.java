package cn.edu.neu.model;

public class SubjectType {
  private int subjectTypeId;

  private String subjectTypeName;

  public SubjectType(int subjectTypeId, String subjectTypeName) {
    super();
    this.subjectTypeId = subjectTypeId;
    this.subjectTypeName = subjectTypeName;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public String getSubjectTypeName() {
    return subjectTypeName;
  }

  public void setSubjectTypeName(String subjectTypeName) {
    this.subjectTypeName = subjectTypeName;
  }

  @Override
  public String toString() {
    return "SubjectType [subjectTypeId=" + subjectTypeId + ", subjectTypeName=" + subjectTypeName + "]";
  }

}
