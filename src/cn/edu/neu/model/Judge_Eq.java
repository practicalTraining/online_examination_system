package cn.edu.neu.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Judge_Eq {
  private int jgId;

  private int equestionId;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date cTime;

  public Judge_Eq() {
    super();
  }

  public Judge_Eq(int jgId, int equestionId, Date cTime) {
    super();
    this.jgId = jgId;
    this.equestionId = equestionId;
    this.cTime = cTime;
  }

  public int getJgId() {
    return jgId;
  }

  public void setJgId(int jgId) {
    this.jgId = jgId;
  }

  public int getEquestionId() {
    return equestionId;
  }

  public void setEquestionId(int equestionId) {
    this.equestionId = equestionId;
  }

  public Date getcTime() {
    return cTime;
  }

  public void setcTime(Date cTime) {
    this.cTime = cTime;
  }

  @Override
  public String toString() {
    return "Judge_Eq [jgId=" + jgId + ", equestionId=" + equestionId + ", cTime=" + cTime + "]";
  }

}
