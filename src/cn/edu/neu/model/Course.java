package cn.edu.neu.model;

import java.util.List;

public class Course {
  private int courseId;

  private String courseName;

  private Course_Class course_Class;

  private List<ExamPaper> epaper;

  private List<Equestion> equestion;

  private List<ClassInfo> classinfo;

  public Course() {
    super();
  }

  public Course(int courseId, String courseName) {
    super();
    this.courseId = courseId;
    this.courseName = courseName;
  }

  public Course(int courseId, String courseName, Course_Class course_Class, List<ExamPaper> epaper, List<Equestion> equestion,
    List<ClassInfo> classinfo) {
    super();
    this.courseId = courseId;
    this.courseName = courseName;
    this.course_Class = course_Class;
    this.epaper = epaper;
    this.equestion = equestion;
    this.classinfo = classinfo;
  }

  public List<ExamPaper> getEpaper() {
    return epaper;
  }

  public void setEpaper(List<ExamPaper> epaper) {
    this.epaper = epaper;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public List<ClassInfo> getClassinfo() {
    return classinfo;
  }

  public void setClassinfo(List<ClassInfo> classinfo) {
    this.classinfo = classinfo;
  }

  public Course_Class getCourse_Class() {
    return course_Class;
  }

  public void setCourse_Class(Course_Class course_Class) {
    this.course_Class = course_Class;
  }

  @Override
  public String toString() {
    return "Course [courseId=" + courseId + ", courseName=" + courseName + ", course_Class=" + course_Class + ", epaper=" + epaper + ", equestion="
      + equestion + ", classinfo=" + classinfo + "]";
  }

}
