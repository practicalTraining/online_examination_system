package cn.edu.neu.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Sc_Eq {

  private int scId;

  private int equestionId;

  @DateTimeFormat(pattern = " yyyy-MM-dd HH:mm:ss")
  private Date ctime;

  public Sc_Eq() {
    super();
  }

  public Sc_Eq(int scId, int equestionId, Date cTime) {
    super();
    this.scId = scId;
    this.equestionId = equestionId;
    this.ctime = cTime;
  }

  public int getScId() {
    return scId;
  }

  public void setScId(int scId) {
    this.scId = scId;
  }

  public int getEquestionId() {
    return equestionId;
  }

  public void setEquestionId(int equestionId) {
    this.equestionId = equestionId;
  }

  public Date getcTime() {
    return ctime;
  }

  public void setcTime(Date cTime) {
    this.ctime = cTime;
  }

  @Override
  public String toString() {
    return "Sc_Eq [scId=" + scId + ", equestionId=" + equestionId + ", cTime=" + ctime + "]";
  }

}
