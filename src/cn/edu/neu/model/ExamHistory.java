package cn.edu.neu.model;

public class ExamHistory {
  private int examHisId;

  private int studentId;

  private int examId;

  private int epaperId;

  private String examHisContent;

  private int examHisScore;

  private Student student;

  private Exam exam;

  private ExamPaper examPaper;

  public ExamHistory(int examHisId, int studentId, int examId, int epaperId, String examHisContent, int examHisScore, Student student, Exam exam,
    ExamPaper examPaper) {
    super();
    this.examHisId = examHisId;
    this.studentId = studentId;
    this.examId = examId;
    this.epaperId = epaperId;
    this.examHisContent = examHisContent;
    this.examHisScore = examHisScore;
    this.student = student;
    this.exam = exam;
    this.examPaper = examPaper;
  }

  public ExamHistory() {
    super();
  }

  public int getExamHisId() {
    return examHisId;
  }

  public void setExamHisId(int examHisId) {
    this.examHisId = examHisId;
  }

  public int getStudentId() {
    return studentId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }

  public int getExamId() {
    return examId;
  }

  public void setExamId(int examId) {
    this.examId = examId;
  }

  public int getEpaperId() {
    return epaperId;
  }

  public void setEpaperId(int epaperId) {
    this.epaperId = epaperId;
  }

  public String getExamHisContent() {
    return examHisContent;
  }

  public void setExamHisContent(String examHisContent) {
    this.examHisContent = examHisContent;
  }

  public int getExamHisScore() {
    return examHisScore;
  }

  public void setExamHisScore(int examHisScore) {
    this.examHisScore = examHisScore;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Exam getExam() {
    return exam;
  }

  public void setExam(Exam exam) {
    this.exam = exam;
  }

  public ExamPaper getExamPaper() {
    return examPaper;
  }

  public void setExamPaper(ExamPaper examPaper) {
    this.examPaper = examPaper;
  }

  @Override
  public String toString() {
    return "ExamHistory [examHisId=" + examHisId + ", studentId=" + studentId + ", examId=" + examId + ", epaperId=" + epaperId + ", examHisContent="
      + examHisContent + ", examHisScore=" + examHisScore + ", student=" + student + ", exam=" + exam + ", examPaper=" + examPaper + "]";
  }

}
