package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

public class MultipleChoice {

  private int mcId;

  private String mcName;

  private String optionA;

  private String optionB;

  private String optionC;

  private String optionD;

  private String optionE;

  private String mcResult1;

  private String mcResult2;

  private String mcResult3;

  private int mcScore;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date mcEntryDate;

  private int subjectTypeId;

  private int courseId;

  private MultipartFile McDocFile;

  private List<Equestion> equestion;

  private Mc_Eq mc_eq;

  public MultipleChoice(int mcId, String mcName, String optionA, String optionB, String optionC, String optionD, String optionE, String mcResult1,
    String mcResult2, String mcResult3, int mcScore, Date mcEntryDate, int subjectTypeId, int courseId, MultipartFile mcDocFile,
    List<Equestion> equestion, Mc_Eq mc_eq) {
    super();
    this.mcId = mcId;
    this.mcName = mcName;
    this.optionA = optionA;
    this.optionB = optionB;
    this.optionC = optionC;
    this.optionD = optionD;
    this.optionE = optionE;
    this.mcResult1 = mcResult1;
    this.mcResult2 = mcResult2;
    this.mcResult3 = mcResult3;
    this.mcScore = mcScore;
    this.mcEntryDate = mcEntryDate;
    this.subjectTypeId = subjectTypeId;
    this.courseId = courseId;
    McDocFile = mcDocFile;
    this.equestion = equestion;
    this.mc_eq = mc_eq;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public MultipleChoice() {
    super();
  }

  public int getMcId() {
    return mcId;
  }

  public void setMcId(int mcId) {
    this.mcId = mcId;
  }

  public String getMcName() {
    return mcName;
  }

  public void setMcName(String mcName) {
    this.mcName = mcName;
  }

  public String getOptionA() {
    return optionA;
  }

  public void setOptionA(String optionA) {
    this.optionA = optionA;
  }

  public String getOptionB() {
    return optionB;
  }

  public void setOptionB(String optionB) {
    this.optionB = optionB;
  }

  public String getOptionC() {
    return optionC;
  }

  public void setOptionC(String optionC) {
    this.optionC = optionC;
  }

  public String getOptionD() {
    return optionD;
  }

  public void setOptionD(String optionD) {
    this.optionD = optionD;
  }

  public String getOptionE() {
    return optionE;
  }

  public void setOptionE(String optionE) {
    this.optionE = optionE;
  }

  public String getMcResult1() {
    return mcResult1;
  }

  public void setMcResult1(String mcResult1) {
    this.mcResult1 = mcResult1;
  }

  public String getMcResult2() {
    return mcResult2;
  }

  public void setMcResult2(String mcResult2) {
    this.mcResult2 = mcResult2;
  }

  public String getMcResult3() {
    return mcResult3;
  }

  public void setMcResult3(String mcResult3) {
    this.mcResult3 = mcResult3;
  }

  public int getMcScore() {
    return mcScore;
  }

  public void setMcScore(int mcScore) {
    this.mcScore = mcScore;
  }

  public Date getMcEntryDate() {
    return mcEntryDate;
  }

  public void setMcEntryDate(Date mcEntryDate) {
    this.mcEntryDate = mcEntryDate;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public MultipartFile getMcDocFile() {
    return McDocFile;
  }

  public void setMcDocFile(MultipartFile mcDocFile) {
    McDocFile = mcDocFile;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public Mc_Eq getMc_eq() {
    return mc_eq;
  }

  public void setMc_eq(Mc_Eq mc_eq) {
    this.mc_eq = mc_eq;
  }

  @Override
  public String toString() {
    return "MultipleChoice [mcId=" + mcId + ", mcName=" + mcName + ", optionA=" + optionA + ", optionB=" + optionB + ", optionC=" + optionC
      + ", optionD=" + optionD + ", optionE=" + optionE + ", mcResult1=" + mcResult1 + ", mcResult2=" + mcResult2 + ", mcResult3=" + mcResult3
      + ", mcScore=" + mcScore + ", mcEntryDate=" + mcEntryDate + ", subjectTypeId=" + subjectTypeId + ", courseId=" + courseId + ", McDocFile="
      + McDocFile + ", equestion=" + equestion + ", mc_eq=" + mc_eq + "]";
  }

}
