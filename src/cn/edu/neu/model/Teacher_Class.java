package cn.edu.neu.model;

public class Teacher_Class {
  int teacherId;

  int classId;

  public Teacher_Class() {
    super();
  }

  public Teacher_Class(int teacherId, int classId) {
    super();
    this.teacherId = teacherId;
    this.classId = classId;
  }

  public int getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(int teacherId) {
    this.teacherId = teacherId;
  }

  public int getClassId() {
    return classId;
  }

  public void setClassId(int classId) {
    this.classId = classId;
  }

  @Override
  public String toString() {
    return "Teacher_Class [teacherId=" + teacherId + ", classId=" + classId + "]";
  }

}
