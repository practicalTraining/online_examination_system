package cn.edu.neu.model;

public class Exam_Class {
  private int examId;

  private int classId;

  public Exam_Class() {
    super();
  }

  public Exam_Class(int examId, int classId) {
    super();
    this.examId = examId;
    this.classId = classId;
  }

  public int getExamId() {
    return examId;
  }

  public void setExamId(int examId) {
    this.examId = examId;
  }

  public int getClassId() {
    return classId;
  }

  public void setClassId(int classId) {
    this.classId = classId;
  }

  @Override
  public String toString() {
    return "Exam_Class [examId=" + examId + ", classId=" + classId + "]";
  }

}
