package cn.edu.neu.model;

public class Course_Class {
  private int courseId;

  private int classId;

  public Course_Class() {
    super();
  }

  public Course_Class(int courseId, int classId) {
    super();
    this.courseId = courseId;
    this.classId = classId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public int getClassId() {
    return classId;
  }

  public void setClassId(int classId) {
    this.classId = classId;
  }

  @Override
  public String toString() {
    return "Course_Class [courseId=" + courseId + ", classId=" + classId + "]";
  }

}
