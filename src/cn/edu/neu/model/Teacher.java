package cn.edu.neu.model;

import java.util.List;

public class Teacher {
  private int teacherId;

  private int courseId;

  private String teacherName;

  private String teacherPass;

  private String teacherEmail;

  private Course course;

  private Teacher_Class tc;

  private List<ClassInfo> classinfo;

  public Teacher(int teacherId, int courseId, String teacherName, String teacherPass, String teacherEmail, Course course, Teacher_Class tc,
    List<ClassInfo> classinfo) {
    super();
    this.teacherId = teacherId;
    this.courseId = courseId;
    this.teacherName = teacherName;
    this.teacherPass = teacherPass;
    this.teacherEmail = teacherEmail;
    this.course = course;
    this.tc = tc;
    this.classinfo = classinfo;
  }

  public int getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(int teacherId) {
    this.teacherId = teacherId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public String getTeacherName() {
    return teacherName;
  }

  public void setTeacherName(String teacherName) {
    this.teacherName = teacherName;
  }

  public String getTeacherPass() {
    return teacherPass;
  }

  public void setTeacherPass(String teacherPass) {
    this.teacherPass = teacherPass;
  }

  public String getTeacherEmail() {
    return teacherEmail;
  }

  public void setTeacherEmail(String teacherEmail) {
    this.teacherEmail = teacherEmail;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Teacher_Class getTc() {
    return tc;
  }

  public void setTc(Teacher_Class tc) {
    this.tc = tc;
  }

  public List<ClassInfo> getClassinfo() {
    return classinfo;
  }

  public void setClassinfo(List<ClassInfo> classinfo) {
    this.classinfo = classinfo;
  }

  @Override
  public String toString() {
    return "Teacher [teacherId=" + teacherId + ", courseId=" + courseId + ", teacherName=" + teacherName + ", teacherPass=" + teacherPass
      + ", teacherEmail=" + teacherEmail + ", course=" + course + ", tc=" + tc + ", classinfo=" + classinfo + "]";
  }

}
