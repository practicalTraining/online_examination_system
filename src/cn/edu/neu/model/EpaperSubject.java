package cn.edu.neu.model;

public class EpaperSubject {
  private int epsId;

  private int subjectId;

  private int equestionId;

  private int subjectTypeId;

  private int epaperId;

  private SubjectType subjectType;

  private Equestion equestion;

  private SingleChoice singlechoice;

  private MultipleChoice multiplechoice;

  private FillAnswer fillanswer;

  private JudgeMent judgement;

  private ExamPaper epaper;

  public EpaperSubject() {
    super();
  }

  public EpaperSubject(int epsId, int subjectId, int equestionId, int subjectTypeId, int epaperId, SubjectType subjectType, Equestion equestion,
    SingleChoice singlechoice, MultipleChoice multiplechoice, FillAnswer fillanswer, JudgeMent judgement, ExamPaper epaper) {
    super();
    this.epsId = epsId;
    this.subjectId = subjectId;
    this.equestionId = equestionId;
    this.subjectTypeId = subjectTypeId;
    this.epaperId = epaperId;
    this.subjectType = subjectType;
    this.equestion = equestion;
    this.singlechoice = singlechoice;
    this.multiplechoice = multiplechoice;
    this.fillanswer = fillanswer;
    this.judgement = judgement;
    this.epaper = epaper;
  }

  public int getEpsId() {
    return epsId;
  }

  public void setEpsId(int epsId) {
    this.epsId = epsId;
  }

  public int getSubjectId() {
    return subjectId;
  }

  public void setSubjectId(int subjectId) {
    this.subjectId = subjectId;
  }

  public int getEquestionId() {
    return equestionId;
  }

  public void setEquestionId(int equestionId) {
    this.equestionId = equestionId;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public int getEpaperId() {
    return epaperId;
  }

  public void setEpaperId(int epaperId) {
    this.epaperId = epaperId;
  }

  public SubjectType getSubject() {
    return subjectType;
  }

  public void setSubject(SubjectType subjectType) {
    this.subjectType = subjectType;
  }

  public SubjectType getSubjectType() {
    return subjectType;
  }

  public void setSubjectType(SubjectType subjectType) {
    this.subjectType = subjectType;
  }

  public Equestion getEquestion() {
    return equestion;
  }

  public void setEquestion(Equestion equestion) {
    this.equestion = equestion;
  }

  public SingleChoice getSinglechoice() {
    return singlechoice;
  }

  public void setSinglechoice(SingleChoice singlechoice) {
    this.singlechoice = singlechoice;
  }

  public ExamPaper getEpaper() {
    return epaper;
  }

  public void setEpaper(ExamPaper epaper) {
    this.epaper = epaper;
  }

  public MultipleChoice getMultiplechoice() {
    return multiplechoice;
  }

  public void setMultiplechoice(MultipleChoice multiplechoice) {
    this.multiplechoice = multiplechoice;
  }

  public FillAnswer getFillanswer() {
    return fillanswer;
  }

  public void setFillanswer(FillAnswer fillanswer) {
    this.fillanswer = fillanswer;
  }

  public JudgeMent getJudgement() {
    return judgement;
  }

  public void setJudgement(JudgeMent judgement) {
    this.judgement = judgement;
  }

  @Override
  public String toString() {
    return "EpaperSubject [epsId=" + epsId + ", subjectId=" + subjectId + ", equestionId=" + equestionId + ", subjectTypeId=" + subjectTypeId
      + ", epaperId=" + epaperId + ", subjectType=" + subjectType + ", equestion=" + equestion + ", singlechoice=" + singlechoice
      + ", multiplechoice=" + multiplechoice + ", fillanswer=" + fillanswer + ", judgement=" + judgement + ", epaper=" + epaper + "]";
  }

}
