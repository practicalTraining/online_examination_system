package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

public class SingleChoice {
  private int scId;

  private String scName;

  private String optionA;

  private String optionB;

  private String optionC;

  private String optionD;

  private String scResult;

  private int scScore;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date scEntrydate;

  private int subjectTypeId;

  private int courseId;

  private MultipartFile ScDocFile;

  private List<Equestion> equestion;

  private Sc_Eq sc_eq;

  public SingleChoice() {
    super();
  }

  public SingleChoice(int scId, String scName, String optionA, String optionB, String optionC, String optionD, String scResult, int scScore,
    Date scEntrydate, int subjectTypeId, int courseId, MultipartFile scDocFile, List<Equestion> equestion, Sc_Eq sc_eq) {
    super();
    this.scId = scId;
    this.scName = scName;
    this.optionA = optionA;
    this.optionB = optionB;
    this.optionC = optionC;
    this.optionD = optionD;
    this.scResult = scResult;
    this.scScore = scScore;
    this.scEntrydate = scEntrydate;
    this.subjectTypeId = subjectTypeId;
    this.courseId = courseId;
    ScDocFile = scDocFile;
    this.equestion = equestion;
    this.sc_eq = sc_eq;
  }

  public Sc_Eq getSc_eq() {
    return sc_eq;
  }

  public void setSc_eq(Sc_Eq sc_eq) {
    this.sc_eq = sc_eq;
  }

  public int getScId() {
    return scId;
  }

  public void setScId(int scId) {
    this.scId = scId;
  }

  public String getScName() {
    return scName;
  }

  public void setScName(String scName) {
    this.scName = scName;
  }

  public String getOptionA() {
    return optionA;
  }

  public void setOptionA(String optionA) {
    this.optionA = optionA;
  }

  public String getOptionB() {
    return optionB;
  }

  public void setOptionB(String optionB) {
    this.optionB = optionB;
  }

  public String getOptionC() {
    return optionC;
  }

  public void setOptionC(String optionC) {
    this.optionC = optionC;
  }

  public String getOptionD() {
    return optionD;
  }

  public void setOptionD(String optionD) {
    this.optionD = optionD;
  }

  public String getScResult() {
    return scResult;
  }

  public void setScResult(String scResult) {
    this.scResult = scResult;
  }

  public int getScScore() {
    return scScore;
  }

  public void setScScore(int scScore) {
    this.scScore = scScore;
  }

  public Date getScEntrydate() {
    return scEntrydate;
  }

  public void setScEntrydate(Date scEntrydate) {
    this.scEntrydate = scEntrydate;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public MultipartFile getScDocFile() {
    return ScDocFile;
  }

  public void setScDocFile(MultipartFile scDocFile) {
    ScDocFile = scDocFile;
  }

  @Override
  public String toString() {
    return "SingleChoice [scId=" + scId + ", scName=" + scName + ", optionA=" + optionA + ", optionB=" + optionB + ", optionC=" + optionC
      + ", optionD=" + optionD + ", scResult=" + scResult + ", scScore=" + scScore + ", scEntrydate=" + scEntrydate + ", subjectTypeId="
      + subjectTypeId + ", courseId=" + courseId + ", ScDocFile=" + ScDocFile + ", equestion=" + equestion + ", sc_eq=" + sc_eq + "]";
  }

}