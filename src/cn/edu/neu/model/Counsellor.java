package cn.edu.neu.model;

public class Counsellor {
  private int counsellorId;

  private String counsellorName;

  public Counsellor() {
    super();
  }

  public Counsellor(int counsellorId, String counsellorName) {
    super();
    this.counsellorId = counsellorId;
    this.counsellorName = counsellorName;
  }

  public int getCounsellorId() {
    return counsellorId;
  }

  public void setCounsellorId(int counsellorId) {
    this.counsellorId = counsellorId;
  }

  public String getCounsellorName() {
    return counsellorName;
  }

  public void setCounsellorName(String counsellorName) {
    this.counsellorName = counsellorName;
  }

  @Override
  public String toString() {
    return "Counsellor [counsellorId=" + counsellorId + ", counsellorName=" + counsellorName + "]";
  }

}
