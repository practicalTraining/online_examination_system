package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class Exam {
  private int examId;

  private int courseId;

  private int teacherId;

  private String examName;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date examDate;

  private int examDuration;

  private String examDesci;

  private int examStatus;

  private Course course;

  private Teacher teacher;

  private Exam_Class exam_Class;

  private ExamPaper epaper;

  private List<ClassInfo> classInfo;

  public Exam() {
    super();
  }

  public Exam(int examId, int courseId, int teacherId, String examName, Date examDate, int examDuration, String examDesci, int examStatus,
    Course course, Teacher teacher, Exam_Class exam_Class, ExamPaper epaper, List<ClassInfo> classInfo) {
    super();
    this.examId = examId;
    this.courseId = courseId;
    this.teacherId = teacherId;
    this.examName = examName;
    this.examDate = examDate;
    this.examDuration = examDuration;
    this.examDesci = examDesci;
    this.examStatus = examStatus;
    this.course = course;
    this.teacher = teacher;
    this.exam_Class = exam_Class;
    this.epaper = epaper;
    this.classInfo = classInfo;
  }

  public int getExamId() {
    return examId;
  }

  public void setExamId(int examId) {
    this.examId = examId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public int getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(int teacherId) {
    this.teacherId = teacherId;
  }

  public String getExamName() {
    return examName;
  }

  public void setExamName(String examName) {
    this.examName = examName;
  }

  public Date getExamDate() {
    return examDate;
  }

  public void setExamDate(Date examDate) {
    this.examDate = examDate;
  }

  public String getExamDesci() {
    return examDesci;
  }

  public void setExamDesci(String examDesci) {
    this.examDesci = examDesci;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Teacher getTeacher() {
    return teacher;
  }

  public void setTeacher(Teacher teacher) {
    this.teacher = teacher;
  }

  public int getExamDuration() {
    return examDuration;
  }

  public void setExamDuration(int examDuration) {
    this.examDuration = examDuration;
  }

  public int getExamStatus() {
    return examStatus;
  }

  public void setExamStatus(int examStatus) {
    this.examStatus = examStatus;
  }

  public Exam_Class getExam_Class() {
    return exam_Class;
  }

  public void setExam_Class(Exam_Class exam_Class) {
    this.exam_Class = exam_Class;
  }

  public List<ClassInfo> getClassInfo() {
    return classInfo;
  }

  public void setClassInfo(List<ClassInfo> classInfo) {
    this.classInfo = classInfo;
  }

  public ExamPaper getEpaper() {
    return epaper;
  }

  public void setEpaper(ExamPaper epaper) {
    this.epaper = epaper;
  }

  @Override
  public String toString() {
    return "Exam [examId=" + examId + ", courseId=" + courseId + ", teacherId=" + teacherId + ", examName=" + examName + ", examDate=" + examDate
      + ", examDuration=" + examDuration + ", examDesci=" + examDesci + ", examStatus=" + examStatus + ", course=" + course + ", teacher=" + teacher
      + ", exam_Class=" + exam_Class + ", epaper=" + epaper + ", classInfo=" + classInfo + "]";
  }

}
