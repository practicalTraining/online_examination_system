package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class Equestion {
  private int equestionId;

  private int epaperId;

  private int courseId;

  private String equestionName;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date equestionCtime;

  private Course course;

  private List<SingleChoice> singlechoice;

  private List<MultipleChoice> multiplechoice;

  private List<FillAnswer> fillanswer;

  private List<JudgeMent> judgement;

  private Sc_Eq sc_eq;

  private Mc_Eq mc_eq;

  private Fa_Eq fa_eq;

  private Judge_Eq judge_eq;

  private EpaperSubject epaperSubject;

  public Equestion() {
    super();
  }

  public Equestion(int equestionId, int epaperId, int courseId, String equestionName, Date equestionCtime, Course course,
    List<SingleChoice> singlechoice, List<MultipleChoice> multiplechoice, List<FillAnswer> fillanswer, List<JudgeMent> judgement, Sc_Eq sc_eq,
    Mc_Eq mc_eq, Fa_Eq fa_eq, Judge_Eq judge_eq, EpaperSubject epaperSubject) {
    super();
    this.equestionId = equestionId;
    this.epaperId = epaperId;
    this.courseId = courseId;
    this.equestionName = equestionName;
    this.equestionCtime = equestionCtime;
    this.course = course;
    this.singlechoice = singlechoice;
    this.multiplechoice = multiplechoice;
    this.fillanswer = fillanswer;
    this.judgement = judgement;
    this.sc_eq = sc_eq;
    this.mc_eq = mc_eq;
    this.fa_eq = fa_eq;
    this.judge_eq = judge_eq;
    this.epaperSubject = epaperSubject;
  }

  public List<MultipleChoice> getMultiplechoice() {
    return multiplechoice;
  }

  public void setMultiplechoice(List<MultipleChoice> multiplechoice) {
    this.multiplechoice = multiplechoice;
  }

  public List<FillAnswer> getFillanswer() {
    return fillanswer;
  }

  public void setFillanswer(List<FillAnswer> fillanswer) {
    this.fillanswer = fillanswer;
  }

  public List<JudgeMent> getJudgement() {
    return judgement;
  }

  public void setJudgement(List<JudgeMent> judgement) {
    this.judgement = judgement;
  }

  public int getEquestionId() {
    return equestionId;
  }

  public void setEquestionId(int equestionId) {
    this.equestionId = equestionId;
  }

  public int getEpaperId() {
    return epaperId;
  }

  public void setEpaperId(int epaperId) {
    this.epaperId = epaperId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public String getEquestionName() {
    return equestionName;
  }

  public void setEquestionName(String equestionName) {
    this.equestionName = equestionName;
  }

  public Date getEquestionCtime() {
    return equestionCtime;
  }

  public void setEquestionCtime(Date equestionCtime) {
    this.equestionCtime = equestionCtime;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public List<SingleChoice> getSinglechoice() {
    return singlechoice;
  }

  public void setSinglechoice(List<SingleChoice> singlechoice) {
    this.singlechoice = singlechoice;
  }

  public Sc_Eq getSc_eq() {
    return sc_eq;
  }

  public void setSc_eq(Sc_Eq sc_eq) {
    this.sc_eq = sc_eq;
  }

  public EpaperSubject getEpaperSubject() {
    return epaperSubject;
  }

  public void setEpaperSubject(EpaperSubject epaperSubject) {
    this.epaperSubject = epaperSubject;
  }

  public Mc_Eq getMc_eq() {
    return mc_eq;
  }

  public void setMc_eq(Mc_Eq mc_eq) {
    this.mc_eq = mc_eq;
  }

  public Fa_Eq getFa_eq() {
    return fa_eq;
  }

  public void setFa_eq(Fa_Eq fa_eq) {
    this.fa_eq = fa_eq;
  }

  public Judge_Eq getJudge_eq() {
    return judge_eq;
  }

  public void setJudge_eq(Judge_Eq judge_eq) {
    this.judge_eq = judge_eq;
  }

  @Override
  public String toString() {
    return "Equestion [equestionId=" + equestionId + ", epaperId=" + epaperId + ", courseId=" + courseId + ", equestionName=" + equestionName
      + ", equestionCtime=" + equestionCtime + ", course=" + course + ", singlechoice=" + singlechoice + ", multiplechoice=" + multiplechoice
      + ", fillanswer=" + fillanswer + ", judgement=" + judgement + ", sc_eq=" + sc_eq + ", mc_eq=" + mc_eq + ", fa_eq=" + fa_eq + ", judge_eq="
      + judge_eq + ", epaperSubject=" + epaperSubject + "]";
  }

}
