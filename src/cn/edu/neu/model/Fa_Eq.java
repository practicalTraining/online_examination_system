package cn.edu.neu.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Fa_Eq {
  private int faId;

  private int equestionId;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date cTime;

  public Fa_Eq(int faId, int equestionId, Date cTime) {
    super();
    this.faId = faId;
    this.equestionId = equestionId;
    this.cTime = cTime;
  }

  public Fa_Eq() {
    super();
  }

  public int getEquestionId() {
    return equestionId;
  }

  public void setEquestionId(int equestionId) {
    this.equestionId = equestionId;
  }

  public int getFaId() {
    return faId;
  }

  public void setFaId(int faId) {
    this.faId = faId;
  }

  public Date getcTime() {
    return cTime;
  }

  public void setcTime(Date cTime) {
    this.cTime = cTime;
  }

  @Override
  public String toString() {
    return "Fa_Eq [faId=" + faId + ", equestionId=" + equestionId + ", cTime=" + cTime + "]";
  }

}
