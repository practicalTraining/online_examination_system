package cn.edu.neu.model;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

public class FillAnswer {
  private int faId;

  private String faName;

  private String faResult1;

  private String faResult2;

  private int faScore;

  @DateTimeFormat(pattern = " yyyy-MM-dd HH:mm:ss")
  private Date faEntrydate;

  private int subjectTypeId;

  private int courseId;

  private List<Equestion> equestion;

  private MultipartFile FaDocFile;

  private Fa_Eq fa_eq;

  public FillAnswer(int faId, String faName, String faResult1, String faResult2, int faScore, Date faEntrydate, int subjectTypeId, int courseId,
    List<Equestion> equestion, MultipartFile faDocFile, Fa_Eq fa_eq) {
    super();
    this.faId = faId;
    this.faName = faName;
    this.faResult1 = faResult1;
    this.faResult2 = faResult2;
    this.faScore = faScore;
    this.faEntrydate = faEntrydate;
    this.subjectTypeId = subjectTypeId;
    this.courseId = courseId;
    this.equestion = equestion;
    FaDocFile = faDocFile;
    this.fa_eq = fa_eq;
  }

  public FillAnswer() {
    super();
  }

  public int getFaId() {
    return faId;
  }

  public void setFaId(int faId) {
    this.faId = faId;
  }

  public String getFaName() {
    return faName;
  }

  public void setFaName(String faName) {
    this.faName = faName;
  }

  public String getFaResult1() {
    return faResult1;
  }

  public void setFaResult1(String faResult1) {
    this.faResult1 = faResult1;
  }

  public String getFaResult2() {
    return faResult2;
  }

  public void setFaResult2(String faResult2) {
    this.faResult2 = faResult2;
  }

  public int getFaScore() {
    return faScore;
  }

  public void setFaScore(int faScore) {
    this.faScore = faScore;
  }

  public Date getFaEntrydate() {
    return faEntrydate;
  }

  public void setFaEntrydate(Date faEntrydate) {
    this.faEntrydate = faEntrydate;
  }

  public int getSubjectTypeId() {
    return subjectTypeId;
  }

  public void setSubjectTypeId(int subjectTypeId) {
    this.subjectTypeId = subjectTypeId;
  }

  public int getCourseId() {
    return courseId;
  }

  public void setCourseId(int courseId) {
    this.courseId = courseId;
  }

  public List<Equestion> getEquestion() {
    return equestion;
  }

  public void setEquestion(List<Equestion> equestion) {
    this.equestion = equestion;
  }

  public MultipartFile getFaDocFile() {
    return FaDocFile;
  }

  public void setFaDocFile(MultipartFile faDocFile) {
    FaDocFile = faDocFile;
  }

  public Fa_Eq getFa_eq() {
    return fa_eq;
  }

  public void setFa_eq(Fa_Eq fa_eq) {
    this.fa_eq = fa_eq;
  }

  @Override
  public String toString() {
    return "FillAnswer [faId=" + faId + ", faName=" + faName + ", faResult1=" + faResult1 + ", faResult2=" + faResult2 + ", faScore=" + faScore
      + ", faEntrydate=" + faEntrydate + ", subjectTypeId=" + subjectTypeId + ", courseId=" + courseId + ", equestion=" + equestion + ", FaDocFile="
      + FaDocFile + ", fa_eq=" + fa_eq + "]";
  }

}
