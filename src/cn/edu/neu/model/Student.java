package cn.edu.neu.model;

public class Student {
  private int studentId;

  private int classId;

  private String studentName;

  private String studentPass;

  private String studentEmail;

  private ClassInfo classInfo;

  public Student(int studentId, int classId, String studentName, String studentPass, String studentEmail, ClassInfo classInfo) {
    super();
    this.studentId = studentId;
    this.classId = classId;
    this.studentName = studentName;
    this.studentPass = studentPass;
    this.studentEmail = studentEmail;
    this.classInfo = classInfo;
  }

  public Student() {
    super();
  }

  public int getStudentId() {
    return studentId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }

  public int getClassId() {
    return classId;
  }

  public void setClassId(int classId) {
    this.classId = classId;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getStudentPass() {
    return studentPass;
  }

  public void setStudentPass(String studentPass) {
    this.studentPass = studentPass;
  }

  public String getStudentEmail() {
    return studentEmail;
  }

  public void setStudentEmail(String studentEmail) {
    this.studentEmail = studentEmail;
  }

  public ClassInfo getClassInfo() {
    return classInfo;
  }

  public void setClassInfo(ClassInfo classInfo) {
    this.classInfo = classInfo;
  }

  @Override
  public String toString() {
    return "Student [studentId=" + studentId + ", classId=" + classId + ", studentName=" + studentName + ", studentPass=" + studentPass
      + ", studentEmail=" + studentEmail + ", classInfo=" + classInfo + "]";
  }

}
