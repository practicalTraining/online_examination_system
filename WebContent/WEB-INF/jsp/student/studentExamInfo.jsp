<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>考试信息</title>
<%@ include file="../common/jscss.jsp" %>
<style type="text/css">
.border_bottom_display{
border-bottom:1px solid #aaa;
}
.ul_exam{
padding: 5px 5px;

}
.ul_exam>li{
padding: 5px 5px;
list-style-type: none;
font-size:16px;
}
.ul_exam>li>input{
margin-right:10px;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	if($("#sc_subject")){
		$("#nav_sc_subject").show();
	}else{
		$("#nav_sc_subject").hide();
	}
	
	
	//试题监听
	$(document).click(function (e) {
	var v_name = $(e.target).attr('name');
	var id =v_name.substring(3,4);
	$("#sc_num_"+id).addClass('btn-success')
	})
	
})
</script>
</head>
<body>
	<div class="container-fluid">
		
		<div class="row">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div  class="container">
			<div class="row">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand">在线考试系统</a>
				</div>
			
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav navbar-right">
						<li >
							 <a  >${student_Name}</a>
						</li>
					</ul>
				</div>		
			</div>		
		</div>
	</nav>
		<div class="col-md-12" style="margin-top:70px;">
		<div class="col-md-8" >
			<div class="panel panel-default">
				<div class="panel-heading" id="sc_subject">
				 	<h5>单选题</h5>				 	
				</div>
				<div class="panel-body">
					<form action="${pageContext.request.contextPath}/examHis/handleExamHis" method="post">
					<div id="To_exam_content">
					<c:forEach items="${examSC.epaper.epaperSubject}" var="eesc" varStatus="eescc">						
						<div id="count_sc">
						<div class="panel-heading">
							<p>${eescc.count}、${eesc.singlechoice.scName }</p>
							<input type="hidden" name="T_scResult" value="${eesc.singlechoice.scResult }">
						</div>
						<div class="panel-body border_bottom_display">
							<div class="from-group">
							<ul class="ul_exam">
							<li><input name="sc[${eescc.count-1}].scResult" type="radio" value="A" >A.<span>${eesc.singlechoice.optionA }</span></li>
							<li><input name="sc[${eescc.count-1}].scResult" type="radio" value="B">B.<span>${eesc.singlechoice.optionB }</span></li>
							<li><input name="sc[${eescc.count-1}].scResult" type="radio" value="C">C.<span>${eesc.singlechoice.optionC }</span></li>
							<li><input name="sc[${eescc.count-1}].scResult" type="radio" value="D">D.<span>${eesc.singlechoice.optionD }</span></li>			
							</ul>
							</div>
						</div>
					</div>
					
					<c:set var="sc_num" value="${eescc.count}"/>
					</c:forEach>
					<input type="hidden" name="studentId" value="${param.studentId }">
					<input type="hidden" name="examId" value="${param.examId }">
					<input type="hidden" name="epaperId" value="${examSC.epaper.epaperId }">
					</div>
					<textarea rows="100" cols="100" name ="examHisContent" id="exam_content" style="display:none">
					
					</textarea>
							<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-bottom">
			<div style="margin:20px;">
				<button id="exam_submit" class="btn btn-default btn-primary pull-right" style="margin-bottom:10px;">提交</button>
			</div>
			</nav>
		</div>
					</form>
				</div>
			</div>
		</div>
		<!-- 右侧导航栏 -->
		<div class="col-md-3">
			<div id="nav_sc_subject">
			<div class="panel panel-default">
				<div class="panel-heading" >
					 	<h5>单选题</h5>				 	
					</div>
				<div class="panel-body">
					<c:forEach var="i" begin="0" end="${sc_num-1}">
						<input type="button" value="${i+1 }" id="sc_num_${i }" class="btn btn-default">
					</c:forEach>
				</div>
			</div>
			</div>
			
			<div id="nav_mc_subject">
			<div class="panel panel-default">
				<div class="panel-heading" >
					 	<h5>多选题</h5>				 	
					</div>
				<div class="panel-body">
					<c:forEach var="i" begin="1" end="${sc_num}">
						<input type="button" value="${i }"  class="btn btn-default">
					</c:forEach>
				</div>
			</div>
			</div>
			
			<div id="nav_TF_subject">
			<div class="panel panel-default">
				<div class="panel-heading" >
					 	<h5>判断题</h5>				 	
					</div>
				<div class="panel-body">
					<c:forEach var="i" begin="1" end="${sc_num}">
						<input type="button"  value="${i }" class="btn btn-default">
					</c:forEach>
				</div>
			</div>
			</div>
			
			<div id="nav_fillAnswers_subject">
			<div class="panel panel-default">
				<div class="panel-heading" >
					 	<h5>填空题</h5>				 	
					</div>
				<div class="panel-body">
					<c:forEach var="i" begin="1" end="${sc_num}">
						<input type="button" value="${i }" class="btn btn-default">
					</c:forEach>
				</div>
			</div>
			</div>
		</div>
		
		</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			
			$("#exam_submit").click(function(){
				$("#exam_content").html($("#To_exam_content").html());
			});
		})
	</script>
</body>
</html>