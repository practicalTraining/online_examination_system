<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生首页</title>
<%@ include file="../common/jscss.jsp" %>
<style type="text/css">
.border_bottom_display{
border-bottom:1px solid #eee;
}
</style>

<script type="text/javascript">

	function joinTest(title,url,examDuration){
		layer.open({
			  type: 1
			  ,title:'开始<'+title+'>考试 '//不显示标题栏
			  ,closeBtn: false
			  ,area: '300px;'
			  ,shade: 0.8
			  ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
			  ,resize: false
			  ,btn: ['确定', '取消']
			  ,btnAlign: 'c'
			  ,moveType: 1 //拖拽模式，0或者1
			  ,content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">考试人:'+"${stuinfo.studentName}"+ '<br>考试时长:'+examDuration+'</div>'
			  ,success: function(layero){
			    var btn = layero.find('.layui-layer-btn');
			    btn.find('.layui-layer-btn0').attr({
			      href: url
			    });
			  }
			});
		
	
	}
</script>
</head>
<body>
		<div class="container-fluid">
		<div class="row">
	<%@include file="../common/studentTopNav.jsp" %>
		
		<div class="container" style="margin-top:70px;">
			<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body border_bottom_display">
						<h5>学生信息</h5>
					</div>
					<div class="panel-body">
						<h5>姓名:<span>${stuinfo.studentName }</span></h5>
						<h5>班级:<span>${stuinfo.classInfo.className }</span></h5>
					</div>
				</div>
			</div>
				
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
					
					</div>
					<div class="panel-body">
							<div class="tabbable" id="tabs-7293">
				<ul class="nav nav-tabs">
					<li  >
						 <a href="#panel-600557" data-toggle="tab">概览</a>
					</li>
					<li  class="active">
						 <a href="#panel-947070" data-toggle="tab">近期考试</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane " id="panel-600557">
						<p>
							I'm in Section 1.
						</p>
					</div>
					<div class="tab-pane active" id="panel-947070">
						<table class="table table-hover ">
						<thead >
						<tr>
							<th>
								编号
							</th>
							<th>
								考试名称
							</th>
							<th>
								考试时间
							</th>
							<th>
								考试时长
							</th>
							<th>
								操作
							</th>
						</tr>
						</thead>
						<tbody>
						
					
							<c:if test="${!empty examinfo}">
							<c:forEach items="${examinfo }" var="exami" varStatus="examic">
					 		<c:forEach items="${exami.exam }" var="e" varStatus="ec">
					 		<c:set var="Continue" value="0"/>
					 		<c:if test="${!empty existExaminfo }">
					 			<c:forEach items="${existExaminfo }" var="ee" varStatus="eec">
					 			<c:if test="${ee.examId == e.examId }">
					 			<c:if test="${Continue==0}">
					 			<c:set var="Continue" value="1"/>
					 					<tr>
					 					<td>${ec.count }</td>
					 					<td>${e.examName }</td>
					 					<td><fmt:formatDate value='${e.examDate }' pattern='yyyy-MM-dd HH:mm:ss'/></td>
					 					<td>${e.examDuration}</td>
					 					<td><a class="btn btn-xs btn-info" >已经参加考试</a></td>
					 					</tr>
					 				</c:if>	
					 				</c:if>
					 					<c:if test="${ee.examId != e.examId }">
					 					<c:if test="${Continue==0}">
					 					<c:set var="Continue" value="1"/>
					 					<tr>
					 					<td>${ec.count }</td>
					 					<td>${e.examName }</td>
					 					<td><fmt:formatDate value='${e.examDate }' pattern='yyyy-MM-dd HH:mm:ss'/></td>
					 					<td>${e.examDuration}</td>
					 					<td><a class="btn btn-xs btn-info" onclick="joinTest('${e.examName }','handelStuExam?studentId='+${stuinfo.studentId} +'&examId='+${e.examId},'${e.examDuration }')">参加考试</a></td>
					 					</tr>
					 				</c:if>
					 				</c:if>	
					 				</c:forEach>
					 				</c:if>
					 				<c:if test="${empty existExaminfo }">
					 							<tr>
					 					<td>${ec.count }</td>
					 					<td>${e.examName }</td>
					 					<td><fmt:formatDate value='${e.examDate }' pattern='yyyy-MM-dd HH:mm:ss'/></td>
					 					<td>${e.examDuration}</td>
					 					<td><a class="btn btn-xs btn-info" onclick="joinTest('${e.examName }','handelStuExam?studentId='+${stuinfo.studentId} +'&examId='+${e.examId},'${e.examDuration }')">参加考试</a></td>
					 					</tr>
					 				</c:if>
					 			</c:forEach>
					 		</c:forEach>
							</c:if>
							
							<c:if test="${empty examinfo}">
								<h5>近期没有相关考试</h5>	
							</c:if>

						</tbody>
						</table>
					</div>
				</div>
					</div>
				</div>
			</div>
			</div>
		
		</div>
	
	</div>
</div>
</div>		
</body>
</html>