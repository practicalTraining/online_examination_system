<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>    
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div  class="container">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="${pageContext.request.contextPath}/index">在线考试系统</a>
				</div>
			
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							 <a href="${pageContext.request.contextPath}/index">首页</a>
						</li>
						<li>
							 <a href="#">相关</a>
						</li>
						<li class="dropdown">
							 <a href="#" >帮助</a>
						</li>
						</ul>

					<ul class="nav navbar-nav navbar-right">
					<c:if test="${student_Name ==null }">
						<li>
							 <a href="#">注册</a>
						</li>
						<li >
							 <a href="${pageContext.request.contextPath}/login/stuloginIndex" >登陆</a>
						</li>
					</c:if>	
					<c:if test="${student_Name !=null }">
					<li >
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown">${student_Name}<strong class="caret"></strong></a>
					  <ul class="dropdown-menu">
					 			 <li>
									 <a href="${pageContext.request.contextPath}/student/studentIndex?studentId=${student_Id}">个人中心</a>
								</li>
								<li class="divider">
								</li>
								<li >
									 <a href="${pageContext.request.contextPath}/logout">退出登录</a>
								</li>
						</ul>
					</li>
					</c:if>
					</ul>
				</div>
				</div>
				
			
			</nav>