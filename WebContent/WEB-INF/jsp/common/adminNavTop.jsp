<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>   
<script type="text/javascript">
 $(document).ready(function(){
	 $("#breadcrumb").children("li:last-child").addClass("breadcrumb_active");
	 
 })
</script>
<c:set var="reqPath" value="${pageContext.request.servletPath}"/> 
			<div class="col-md-10 col-md-offset-2 column" id="nav_left">
			<nav class="navbar navbar-default " role="navigation">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-left:0px;">		
					<ul class="nav navbar-nav" >
						<li id="nav_left_li">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-user"></i>
							欢迎${admin_Name}<strong class="caret">
							</strong></a>
							<ul class="dropdown-menu" >
								<li>
									 <a href="#">Action</a>
								</li>
								<li>
									 <a href="#">Another action</a>
								</li>
								<li>
									 <a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									 <a href="logout">退出</a>
								</li>
							</ul>
						</li>
						
						<li class="nav_left_li">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							 <i class="glyphicon glyphicon-envelope"></i>
							 消息
							 <strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									 <a href="#">Action</a>
								</li>
								<li>
									 <a href="#">Another action</a>
								</li>
								<li>
									 <a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									 <a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			
			<ul class="breadcrumb" id="breadcrumb">
    		<li><a href="home"><i class="glyphicon glyphicon-home"></i> Home</a></li>
    		<c:if test="${breadcrumb_1!=null && breadcrumb_1!='' }">
    			<li ><a href="#">${breadcrumb_1}</a></li>
    		</c:if>
    		<c:if test="${breadcrumb_2!=null && breadcrumb_2!='' }">
    			<li ><a href="#">${breadcrumb_2}</a></li>
    		</c:if>
			</ul>
		</div>