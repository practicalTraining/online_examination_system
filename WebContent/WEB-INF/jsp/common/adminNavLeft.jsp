<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>    
 <script type="text/javascript">
	$(document).ready(function(){
		$("#leftNav").children("li").addClass("li-style");
		$("#leftNav").children("li:last-child").addClass("li-style-last");
		
	});
</script>
<!-- submenu_li_acitve -->
<c:set var="reqPath" value="${pageContext.request.servletPath}"/> 
		<div class="col-md-2 fix-left">
			<div id="sign_H">
				<h3	style="color:white;padding:5%">在线考试系统</h3>
			</div>
			
			
			
			<ul id="leftNav">
				<li ${fn:contains(reqPath,"/admin/home")?"class='li_active'":""}>
				<a href="home">
				<i class="glyphicon glyphicon-home"></i>
					首页
				</a>
				</li>
				<li class="submenu" >
				
				<a href="#collapse-0" class="${fn:contains(reqPath,'adminClass')?'':'collapsed' || fn:contains(reqPath,'adminStudent')?'':'collapsed'}"	data-toggle="collapse" >
				<i class="glyphicon glyphicon-user"></i>
					学生班级管理
				</a>
					<ul id="collapse-0"  class="${fn:contains(reqPath,'adminClass')?'':'collapse' || fn:contains(reqPath,'adminStudent')?'':'collapse'}" role="menu">
					<li ${fn:contains(reqPath,"/adminClass")?"class='submenu_li_acitve'":""}>
						<a href="adminClass">班级管理</a>
					</li>
					<li ${fn:contains(reqPath,"/adminStudent")?"class='submenu_li_acitve'":""}>
						<a href="adminStudent" >学生管理</a>
						
					</li>
					</ul>
				</li>
				
				<li class="submenu" >
				
				<a href="#collapse-1" class="${fn:contains(reqPath,'adminCourse')?'':'collapsed' || fn:contains(reqPath,'adminTeacher')?'':'collapsed'}"	data-toggle="collapse" >
				<i class="glyphicon glyphicon-user"></i>
					课程教师管理
				</a>
					<ul id="collapse-1"  class="${fn:contains(reqPath,'adminCourse')?'':'collapse' || fn:contains(reqPath,'adminTeacher')?'':'collapse'}" role="menu">
					<li ${fn:contains(reqPath,"/adminCourse")?"class='submenu_li_acitve'":""}>
						<a href="adminCourse">课程管理</a>
					</li>
					<li ${fn:contains(reqPath,"/adminTeacher")?"class='submenu_li_acitve'":""}>
						<a  href="adminTeacher">老师管理</a>
						
					</li>
					</ul>
				</li>
				
				<li class="submenu" >
				
				<a  href="#collapse-2" class="${fn:contains(reqPath,'adminEquestion')?'':'collapsed' || fn:contains(reqPath,'adminEpaper')?'':'collapsed' || fn:contains(reqPath,'adminExamIndex')?'':'collapsed'} " data-toggle="collapse">
				<i class="glyphicon glyphicon-tasks"></i>
					考试管理
				</a>
					<ul id="collapse-2" class="${fn:contains(reqPath,'adminEquestion')?'':'collapse' || fn:contains(reqPath,'adminEpaper')?'':'collapse' || fn:contains(reqPath,'adminExamIndex')?'':'collapse'}} " >
					<li ${fn:contains(reqPath,"/adminExamIndex")?"class='submenu_li_acitve'":""}>
						<a href="adminExamIndex">考试信息管理</a>
					</li>
					<li ${fn:contains(reqPath,"/adminEpaper")?"class='submenu_li_acitve'":""}>
						<a href="adminEpaper">试卷管理</a>
					</li>
					<li ${fn:contains(reqPath,"/adminEquestion")?"class='submenu_li_acitve'":""}>
						<a href="adminEquestion">题库管理</a>
					</li>
					</ul>
				</li>
				
				<li class="submenu" >
				<a  href="#collapse-3" class="${fn:contains(reqPath,'adminSingleChoice')?'':'collapsed' || fn:contains(reqPath,'adminMultipleChoice')?'':'collapse' || fn:contains(reqPath,'adminFillAnswer')?'':'collapse'  || fn:contains(reqPath,'adminJudgeMent')?'':'collapse'}" data-toggle="collapse">
				<i class="glyphicon glyphicon-list-alt"></i>
					试题
				</a>
					<ul id="collapse-3" class="${fn:contains(reqPath,'adminSingleChoice')?'':'collapse' || fn:contains(reqPath,'adminMultipleChoice')?'':'collapse' || fn:contains(reqPath,'adminFillAnswer')?'':'collapse'  || fn:contains(reqPath,'adminJudgeMent')?'':'collapse'}" >
					<!-- .submenu_li_acitve_a -->
					<li ${fn:contains(reqPath,"/adminSingleChoice")?"class='submenu_li_acitve'":""}>
						<a href="adminSingleChoice" >单选题</a>
					</li>
					<li ${fn:contains(reqPath,"/adminMultipleChoice")?"class='submenu_li_acitve'":""}>
						<a href="adminMultipleChoice">多选题</a>
					</li>
					<li ${fn:contains(reqPath,"/adminFillAnswer")?"class='submenu_li_acitve'":""}> 
						<a href="adminFillAnswer">填空题</a>
					</li>
					<li ${fn:contains(reqPath,"/adminJudgeMent")?"class='submenu_li_acitve'":""}>
						<a href="adminJudgeMent">判断题</a>
					</li>
					</ul>
				</li>
				<li>
				</li>			
			</ul>
		</div>
