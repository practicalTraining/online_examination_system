<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="./common/jscss.jsp" %>
<title>在线考试系统</title>
<style type="text/css">
.padding-col{
padding-left:0px;
padding-right:0px;
}
.index-jumbotron {

    padding: 140px 0 124px 0;
     position: relative;
    text-align: center;
    background-image: url("images/index/jumbotron.png");
    background-repeat:no-repeat;
    background-size:100% 100%;
    -moz-background-size:100% 100%;
   
     color:white;
}
.container .jumbotron, .container-fluid .jumbotron{
	border-radius:0px;
}
.ui_container {
    padding: 0 0px;
    margin: 0 auto;
    width: 1240px
}
.featuretes{
 padding:42px 0;
   text-align: center;
}

.featuretes .featurete{
text-align: center;
    height: 306px;
    padding: 47px 0 0 0;
}    
.featuretes .featurete .icon{
    height: 68px;
    font-size: 84px;
    line-height: 68px;
    color: #1c80d9;

}

.featuretes .featurete .icon h3{
    font-size: 24px;
    margin: 48px 0 15px  0;
    color:#000000cc;
    font-weight: bold;
}

.text-content{
	color: #8c92a4;
    font-size: 14px;
	line-height: 2.33;
	 
}


.column{
	position: relative;
    display: inline-block;
    text-align: left;
    font-size: 1rem;
    width: 24%;
    padding-left: 1rem;
    padding-right: 1rem;
    vertical-align: top;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		
			<%@include file="common/indexTopNav.jsp" %>
		
			<div class="col-md-12 padding-col " style="margin-top:50px">
			<div class="jumbotron index-jumbotron " >
				<div class="ui_container">
				<h1 >
					你好, 欢迎使用在线考试系统!
				</h1>
				<p >
					我们致力于提供便捷、快速、简单的在线考试，创建一场考试，安排考试时间，学生人数检查等多方面严格把控。合理的安排试题，提供自动导入试题，导出试卷等功能。
				</p>
				<p >
					 <a class="btn btn-primary btn-large" href="#">开始吧</a>
				</p>
				</div>
			</div>
			</div>
			<div class="col-md-12 ">
				<div class="featuretes">
					<div class="ui_container">
						<div class="column">
							<div class="featurete">
							 	<div class="icon">
							 		<i class="glyphicon glyphicon-th"></i>
							 		<h3>模块化考试</h3>
							 		<div class="text-content">
							 			模块化的考试，便于学生进行方便快捷的考试，可以放心，直观和准确的看到题目信息！
							 		</div>
							 	</div>
							</div>
						</div>
							<div class="column">
							<div class="featurete">
							 	<div class="icon">
							 		<i class="glyphicon glyphicon-object-align-bottom"></i>
							 		<h3>智能式阅卷</h3>
							 		<div class="text-content">
							 			智能化的批卷，提高老师的工作效率，并且大大的降低批卷错误率！
							 		</div>
							 	</div>
							</div>
						</div>
							<div class="column">
							<div class="featurete">
							 	<div class="icon">
							 		<i class="glyphicon glyphicon-signal"></i>
							 		<h3>个性化分析</h3>
							 		<div class="text-content">
							 			个性化的数据分析，根据考生的考试数据分析准确的考试各个方面，提供完整的分析报告！
							 		</div>
							 	</div>
							</div>
						</div>
							<div class="column">
							<div class="featurete">
							 	<div class="icon">
							 		<i class="glyphicon glyphicon-cog"></i>
							 		<h3>方便式管理</h3>
							 		<div class="text-content">
							 			提供简洁方便的后台功能，提高试题考试以及题目录入等功能的维护，管理！
							 		</div>
							 	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>