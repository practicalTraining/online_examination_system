<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " style="margin-top:3%" >
				<div class="col-md-2">
				<c:if test="${empty fa.faId }">
					<input   class="btn btn-info " type="button" id="Manual_FA" value="手动添加填空题">				
					<input  style="margin-top:10px;" class="btn btn-primary " type="button" id="Automatic_FA" value="自动添加填空题">
				</c:if>
				</div>
				<div class="col-md-9 ">
			
				<div class="panel panel-default">
				<div class="panel-heading">
					<h5>${empty fa.faId?'添加':'修改'}填空选题</h5>
				</div>
				<div class="panel-body" id="Manual_FA_Panel">
					
					<form action="doAdminHandleFA" method="post" role="form">
						<input type="hidden" name="faId" value="${empty fa.faId?'0':fa.faId }" />
							<div class="form-group"> 
							<label for="fa_course">所属课程</label>
							<select id="fa_course" name="courseId"class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group"> 
							<label for="faName">填空题名称 （题目最多两个空 用三个英文的下划线标志___）</label>
							<input id="faName" name="faName" type="text" class="form-control"  value="${fa.faName}"/>
						</div>
						
						<div class="form-group"> 
							<label for="faResult1">填空题答案--1</label>							
							<input id="faResult1" name="faResult1" class="form-control" value="${fa.faResult1 }" >
						</div>
						<div class="form-group"> 
							<label for="faResult2">填空题答案--2 可以为空（如果填空题题目有两个空，需要填上 ）</label>							
							<input id="faResult2" name="faResult2" class="form-control" value="${fa.faResult2 }">
						</div>
						<div class="form-group"> 
							<label for="faScore">单选题分值</label>
							<input id="faScore" name="faScore" type="text" class="form-control"  value="${fa.faScore}"/>
						</div>						
						<input class="btn btn-defalut btn-primary" type="submit" value="提交" />
						<input class="btn btn-defalut" type="reset" value="重置"/>
					</form>
					</div>
					
				<div class="panel-body" id="Automatic_FA_Panel">
					
					<form  method="post" role="form">
						<input type="hidden" name="faId" value="${empty fa.faId?'0':fa.faId }" />
						<div class="form-group" > 
							<label for="fa_course">所属课程</label>
							<select id="fa_courseId_Auto" class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group" > 
							<label for="Auto_FA">判断题文件</label>
							<input id="Auto_FA" name="Auto_FA" type="file"  multiple  />
						</div>					
					</form>
					</div>
				
				
				</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#Automatic_FA_Panel").hide();
	$("#Manual_FA_Panel").hide();
	$("#Manual_FA_Panel").fadeIn();
	$("#Manual_FA").click(function(){
		$("#Automatic_FA_Panel").fadeOut();
		$("#Manual_FA_Panel").fadeIn();
	})
	$("#Automatic_FA").click(function(){
		$("#Manual_FA_Panel").fadeOut();
		$("#Automatic_FA_Panel").fadeIn();
		
	});
	
	$("#Auto_FA").fileinput({
		language:'zh',
		uploadUrl:"${pageContext.request.contextPath}/admin/doAdminHandleFAAUTO",
		uploadAsync:true,
		allowedFileExtensions: ['docx', 'txt', 'xlsx','doc','xls'],//接收的文件后缀
		showUpload: true, //是否显示上传按钮
		showPreview: true,
        showCaption: true,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式 
        maxFileCount: 10, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        textEncoding:'UTF-8',
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        initialPreviewAsData: true,
        uploadExtraData: function () {   //额外参数的关键点
        	
            var obj = {courseId:$("#fa_courseId_Auto").val()};
            console.log(obj);
            return obj;
        }
	}).on("fileuploaded", function (event, data){
		var result = data.response; //后台返回的json
		if(result){
			
		}
	});;
	
});



</script>
</html>