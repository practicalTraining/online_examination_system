<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

	<div class="col-md-12">
		<div class="panel panel-default">
			<table class="table table-striped table-hover table-condensed">
				<thead  >
					<tr >
						<th>
							#
						</th>
						<th>
							填空名称
						</th>
						<th>
							所属课程
						</th>
						<th>
						添加时间
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${!empty fa}">
				<c:forEach items="${fa}" var="fa" varStatus="fas">
					<tr>
						<td>
						${fas.count }
						</td>
						<td>
						${fa.faName }
						</td>
						<td>
						<c:forEach var="c"  items="${course }">
							<c:if test="${fa.courseId ==c.courseId }">
								${c.courseName }
							</c:if>
						</c:forEach>
						</td>
						<td>
						<fmt:formatDate value='${fa.faEntrydate }' pattern='yyyy-MM-dd HH:mm:ss'/>	
						</td>
						<td>
							<a class=" btn btn-xs btn-primary" href="adminHandleFA?faId=${fa.faId} ">修改</a>
							<a class=" btn btn-xs btn-info" href="delFA?faId=${fa.faId}" onclick="return confirm('确定要删除这个单选题吗？')">删除</a>
							<a class=" btn btn-xs btn-warning" href="adminHandleFATOEQ?faId=${fa.faId} ">添加到题库</a>
						</td>
					</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
</div>