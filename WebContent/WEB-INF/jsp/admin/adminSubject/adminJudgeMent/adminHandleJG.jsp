<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " style="margin-top:3%" >
				<div class="col-md-2">
				<c:if test="${empty sc.scId }">
					<input   class="btn btn-info " type="button" id="Manual_JG" value="手动添判断选题">				
					<input  style="margin-top:10px;" class="btn btn-primary " type="button" id="Automatic_JG" value="自动添加判断题">
				</c:if>
				</div>
				<div class="col-md-9 ">
			
				<div class="panel panel-default">
				<div class="panel-heading">
					<h5>${empty sc.scId?'添加':'修改'}判断题</h5>
				</div>
				<div class="panel-body" id="Manual_JG_Panel">
					
					<form action="doAdminHandleJG" method="post" role="form">
						<input type="hidden" name="jgId" value="${empty jg.jgId?'0':jg.jgId }" />
							<div class="form-group"> 
							<label for="jg_course">所属课程</label>
							<select id="jg_course" name="courseId"class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group"> 
							<label for="jgName">判断名称</label>
							<input id="jgName" name="jgName" type="text" class="form-control"  value="${jg.jgName}"/>
						</div>
					
						<div class="form-group"> 
							<label for="jgResult">单选题答案</label>							
							<select id="jgResult" name="jgResult" class="form-control">
								<option value="A" ${jg.jgResult =='A'?'selected':''}>A</option>
								<option value="B" ${jg.jgResult =='B'?'selected':''}>B</option>
							</select>
						</div>
						<div class="form-group"> 
							<label for="jgScore">判断题分值</label>
							<input id="jgScore" name="jgScore" type="text" class="form-control"  value="${jg.jgScore}"/>
						</div>						
						<input class="btn btn-defalut btn-primary" type="submit" value="提交" />
						<input class="btn btn-defalut" type="reset" value="重置"/>
					</form>
					</div>
					
				<div class="panel-body" id="Automatic_JG_Panel">
					
					<form  method="post" role="form">
						<input type="hidden" name="jgId" value="${empty jg.jgId?'0':jg.jgId }" />
						<div class="form-group" > 
							<label for="jg_course">所属课程</label>
							<select id="jg_courseId_Auto" class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group" > 
							<label for="Auto_JG">判断题文件</label>
							<input id="Auto_JG" name="Auto_JG" type="file"  multiple  />
						</div>					
					</form>
					</div>
				
				
				</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#Automatic_JG_Panel").hide();
	$("#Manual_JG_Panel").hide();
	$("#Manual_JG_Panel").fadeIn();
	$("#Manual_JG").click(function(){
		$("#Automatic_JG_Panel").fadeOut();
		$("#Manual_JG_Panel").fadeIn();
	})
	$("#Automatic_JG").click(function(){
		$("#Manual_JG_Panel").fadeOut();
		$("#Automatic_JG_Panel").fadeIn();
		
	});
	
	$("#Auto_JG").fileinput({
		language:'zh',
		uploadUrl:"${pageContext.request.contextPath}/admin/doAdminHandleJGAUTO",
		uploadAsync:true,
		allowedFileExtensions: ['docx', 'txt', 'xlsx','doc','xls'],//接收的文件后缀
		showUpload: true, //是否显示上传按钮
		showPreview: true,
        showCaption: true,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式 
        maxFileCount: 10, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        textEncoding:'UTF-8',
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        initialPreviewAsData: true,
        uploadExtraData: function () {   //额外参数的关键点
        	
            var obj = {courseId:$("#jg_courseId_Auto").val()};
            console.log(obj);
            return obj;
        }
	}).on("fileuploaded", function (event, data){
		var result = data.response; //后台返回的json
		if(result){
			
		}
	});;
	
});



</script>
</html>