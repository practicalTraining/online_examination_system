<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " >
				<div class="col-md-4 col-md-offset-3">
					<h5>添加单选题到题库</h5>
					<form action="doAdminHandleMCTOEQ" method="post" role="form">
						<input type="hidden" name="mcId" value="${mcId}" />
						<div class="form-group"> 
							<label for="eqId">题库</label>							
							<select id="eqId" name="eqId" class="form-control">
							<c:forEach items="${EQ }" var="e" >
								<option value="${e.equestionId }">${e.equestionName }</option>
							</c:forEach>
							</select>
						</div>				
						<input class="btn btn-defalut btn-primary" type="submit" value="提交" />
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>