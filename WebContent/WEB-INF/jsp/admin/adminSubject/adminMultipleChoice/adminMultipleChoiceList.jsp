<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

	<div class="col-md-12">
		<div class="panel panel-default">
			<table class="table table-striped table-hover table-condensed">
				<thead  >
					<tr >
						<th>
							#
						</th>
						<th>
							多选题名称
						</th>
						<th>
							所属课程
						</th>
						<th>
						添加时间
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${!empty mc}">
				<c:forEach items="${mc}" var="mc" varStatus="mcs">
					<tr>
						<td>
						${mcs.count }
						</td>
						<td>
						${mc.mcName }
						</td>
												<td>
						<fmt:formatDate value='${mc.mcEntryDate }' pattern='yyyy-MM-dd HH:mm:ss'/>	
						</td>
						<td>
						<c:forEach var="c"  items="${course }">
							<c:if test="${mc.courseId ==c.courseId }">
								${c.courseName }
							</c:if>
						</c:forEach>
						</td>

						<td>
							<a class=" btn btn-xs btn-primary" href="adminHandleMC?mcId=${mc.mcId} ">修改</a>
							<a class=" btn btn-xs btn-info" href="delMC?mcId=${mc.mcId}" onclick="return confirm('确定要删除这个多选题吗？')">删除</a>
							<a class=" btn btn-xs btn-warning" href="adminHandleMCTOEQ?mcId=${mc.mcId} ">添加到题库</a>
						</td>
					</tr>
					</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
</div>