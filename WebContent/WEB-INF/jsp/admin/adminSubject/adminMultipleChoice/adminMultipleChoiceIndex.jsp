<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>多选题首页</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/adminscindex.css">
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " >
					<div class="col-md-10">
						<!-- 图表 -->	
						
					 </div>
				
					 
					 <div class="col-md-2">
					 	<ul class="right_nav_ul">
					 		<li >
					 		 	<a href="adminHandleMC"> 添加多选题   </a>
					 		</li>
					 		<li >
					 		 	<a>查看最近添加多选题</a>
					 		</li >
					 		<li >
					 		 	<a>查看错误率高多选题</a>
					 		</li>
					 	</ul>
					 </div>
					 
					 	<div style="margin-top:20px;" id="adminMCList">
				</div>
			
				</div>
			
			</div>
			</div>
			
			
<script type="text/javascript">
	$(document).ready(function(){
		li_color();
		
		$.ajax({
            type: "POST",
            url: "getAdminMultipleChoiceList",
            data: "",
            success: function(data){
            /* adminMCList */
           	 $("#adminMCList").append(data);
          	}
         
      });
		
	
	});
	
	
	function li_color(){
		/* 颜色数组    浅蓝色, 深橙色, 粉红色*/
		var a=new Array("bg_color_s_blue","bg_color_d_orange","bg_color_s_pink");
		var i=0;
		$(".right_nav_ul li").each(function(){
			$(this).addClass(a[i]);
			$(this).addClass("btn btn-default");
			i++;
		});
		
	}
</script>

</body>
</html>