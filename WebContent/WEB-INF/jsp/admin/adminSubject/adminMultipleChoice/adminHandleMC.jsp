<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " style="margin-top:3%" >
				<div class="col-md-2">
				<c:if test="${empty mc.mcId }">
					<input   class="btn btn-info " type="button" id="Manual_MC" value="手动添加单选题">				
					<input  style="margin-top:10px;" class="btn btn-primary " type="button" id="Automatic_MC" value="自动添加多选题">
				</c:if>
				</div>
				<div class="col-md-9 ">
			
				<div class="panel panel-default">
				<div class="panel-heading">
					<h5>${empty mc.mcId?'添加':'修改'}多选题</h5>
				</div>
				<div class="panel-body" id="Manual_MC_Panel">
					
					<form action="doAdminHandleMC" method="post" role="form">
						<input type="hidden" name="mcId" value="${empty mc.mcId?'0':mc.mcId }" />
							<div class="form-group"> 
							<label for="mc_course">所属课程</label>
							<select id="mc_courseId" name="courseId"class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group"> 
							<label for="mcName">多选题名称</label>
							<input id="mcName" name="mcName" type="text" class="form-control"  value="${mc.mcName}"/>
						</div>
						
						<div class="form-group">
							<label for="mcOptionA">多选题选项A</label>
							<input id="mcOptionA" name="optionA" type="text" class="form-control" value="${mc.optionA}" />
						</div>
						<div class="form-group">	
							<label for="mcOptionB">多选题选项B</label>
							<input id="mcOptionB" name="optionB" type="text" class="form-control"  value="${mc.optionB}"/>
						</div>
						<div class="form-group">
							<label for="mcOptionC">多选题选项C</label>
							<input id="mcOptionC" name="optionC" type="text" class="form-control"  value="${mc.optionC}"/>
						</div>
						<div class="form-group">
							<label for="mcOptionD">多选题选项D</label>
							<input id="mcOptionD" name="optionD" type="text" class="form-control"  value="${mc.optionD}"/>
						</div>
						<div class="form-group">
							<label for="mcOptionE">多选题选项E(可以为空)</label>
							<input id="mcOptionE" name="optionE" type="text" class="form-control"  value="${mc.optionE}"/>
						</div>
						<div class="form-group"> 
							<label for="mcResult1">多选题答案——1</label>							
							<select id="mcResult1" name="mcResult1" class="form-control">
								<option value="A" ${mc.mcResult1=='A'?'selected':''}>A</option>
								<option value="B" ${mc.mcResult1=='B'?'selected':''}>B</option>
								<option value="C" ${mc.mcResult1=='C'?'selected':''}>C</option>
								<option value="D" ${mc.mcResult1=='D'?'selected':''}>D</option>
							</select>
						</div>
							<div class="form-group"> 
							<label for="mcResult2">多选题答案——2</label>							
							<select id="mcResult2" name="mcResult2" class="form-control">
								<option value="A" ${mc.mcResult2=='A'?'selected':''}>A</option>
								<option value="B" ${mc.mcResult2=='B'?'selected':''}>B</option>
								<option value="C" ${mc.mcResult2=='C'?'selected':''}>C</option>
								<option value="D" ${mc.mcResult2=='D'?'selected':''}>D</option>
							</select>
						</div>
							<div class="form-group"> 
							<label for="mcResult3">多选题答案——3(可以为空)</label>							
							<select id="mcResult3" name="mcResult3" class="form-control">
								<option value="" ${mc.mcResult3==' '?'selected':''}></option>
								<option value="A" ${mc.mcResult3=='A'?'selected':''}>A</option>
								<option value="B" ${mc.mcResult3=='B'?'selected':''}>B</option>
								<option value="C" ${mc.mcResult3=='C'?'selected':''}>C</option>
								<option value="D" ${mc.mcResult3=='D'?'selected':''}>D</option>
							</select>
						</div>
						<div class="form-group"> 
							<label for="mcScore">单选题分值</label>
							<input id="mcScore" name="mcScore" type="text" class="form-control"  value="${mc.mcScore}"/>
						</div>						
						<input class="btn btn-defalut btn-primary" type="submit" value="提交" />
						<input class="btn btn-defalut" type="reset" value="重置"/>
					</form>
					</div>
					
				<div class="panel-body" id="Automatic_MC_Panel">
					
					<form  method="post" role="form">
						<input type="hidden" name="mcId" value="${empty sc.scId?'0':sc.scId }" />
						<div class="form-group" > 
							<label for="mc_course">所属课程</label>
							<select id="mc_courseId_Auto" class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group" > 
							<label for="Auto_MC">单选题文件</label>
							<input id="Auto_MC" name="Auto_MC" type="file"  multiple  />
						</div>					
					</form>
					</div>
				
				
				</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#Automatic_MC_Panel").hide();
	$("#Manual_MC_Panel").hide();
	$("#Manual_MC_Panel").fadeIn();
	$("#Manual_MC").click(function(){
		$("#Automatic_MC_Panel").fadeOut();
		$("#Manual_MC_Panel").fadeIn();
	})
	$("#Automatic_MC").click(function(){
		$("#Manual_MC_Panel").fadeOut();
		$("#Automatic_MC_Panel").fadeIn();
		
	});
	
	$("#Auto_MC").fileinput({
		language:'zh',
		uploadUrl:"${pageContext.request.contextPath}/admin/doAdminHandleMCAUTO",
		uploadAsync:true,
		allowedFileExtensions: ['docx', 'txt', 'xlsx','doc','xls'],//接收的文件后缀
		showUpload: true, //是否显示上传按钮
		showPreview: true,
        showCaption: true,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式 
        maxFileCount: 10, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        textEncoding:'UTF-8',
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        initialPreviewAsData: true,
        uploadExtraData: function () {   //额外参数的关键点
        	
            var obj = {courseId:$("#mc_courseId_Auto").val()};
            console.log(obj);
            return obj;
        }
	}).on("fileuploaded", function (event, data){
		var result = data.response; //后台返回的json
		if(result){
			
		}
	});;
	
});



</script>
</html>