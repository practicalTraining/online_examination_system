<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " style="margin-top:3%" >
				<div class="col-md-2">
				<c:if test="${empty sc.scId }">
					<input   class="btn btn-info " type="button" id="Manual_SC" value="手动添加单选题">				
					<input  style="margin-top:10px;" class="btn btn-primary " type="button" id="Automatic_SC" value="自动添加单选题">
				</c:if>
				</div>
				<div class="col-md-9 ">
			
				<div class="panel panel-default">
				<div class="panel-heading">
					<h5>${empty sc.scId?'添加':'修改'}单选题</h5>
				</div>
				<div class="panel-body" id="Manual_SC_Panel">
					
					<form action="doAdminHandleSC" method="post" role="form">
						<input type="hidden" name="scId" value="${empty sc.scId?'0':sc.scId }" />
							<div class="form-group"> 
							<label for="sc_course">所属课程</label>
							<select id="sc_courseId" name="	courseId"class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group"> 
							<label for="scName">单选题名称</label>
							<input id="scName" name="scName" type="text" class="form-control"  value="${sc.scName}"/>
						</div>
						
						<div class="form-group">
							<label for="scOptionA">单选题选项A</label>
							<input id="scOptionA" name="optionA" type="text" class="form-control" value="${sc.optionA}" />
						</div>
						<div class="form-group">	
							<label for="scOptionB">单选题选项B</label>
							<input id="scOptionB" name="optionB" type="text" class="form-control"  value="${sc.optionB}"/>
						</div>
						<div class="form-group">
							<label for="scOptionC">单选题选项C</label>
							<input id="scOptionC" name="optionC" type="text" class="form-control"  value="${sc.optionC}"/>
						</div>
						<div class="form-group">
							<label for="scOptionD">单选题选项D</label>
							<input id="scOptionD" name="optionD" type="text" class="form-control"  value="${sc.optionD}"/>
						</div>
						<div class="form-group"> 
							<label for="scResult">单选题答案</label>							
							<select id="scResult" name="scResult" class="form-control">
								<option value="A" ${sc.scResult=='A'?'selected':''}>A</option>
								<option value="B" ${sc.scResult=='B'?'selected':''}>B</option>
								<option value="C" ${sc.scResult=='C'?'selected':''}>C</option>
								<option value="D" ${sc.scResult=='D'?'selected':''}>D</option>
							</select>
						</div>
						<div class="form-group"> 
							<label for="scScore">单选题分值</label>
							<input id="scScore" name="scScore" type="text" class="form-control"  value="${sc.scScore}"/>
						</div>						
						<input class="btn btn-defalut btn-primary" type="submit" value="提交" />
						<input class="btn btn-defalut" type="reset" value="重置"/>
					</form>
					</div>
					
				<div class="panel-body" id="Automatic_SC_Panel">
					
					<form  method="post" role="form">
						<input type="hidden" name="scId" value="${empty sc.scId?'0':sc.scId }" />
						<div class="form-group" > 
							<label for="sc_course">所属课程</label>
							<select id="sc_courseId_Auto" class="form-control">
								<c:forEach items="${course }" var="c">
									<option value="${c.courseId }">${c.courseName }</option>
								</c:forEach>
							</select>
						</div>	
						<div class="form-group" > 
							<label for="Auto_SC">单选题文件</label>
							<input id="Auto_SC" name="Auto_SC" type="file"  multiple  />
						</div>					
					</form>
					</div>
				
				
				</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#Automatic_SC_Panel").hide();
	$("#Manual_SC_Panel").hide();
	$("#Manual_SC_Panel").fadeIn();
	$("#Manual_SC").click(function(){
		$("#Automatic_SC_Panel").fadeOut();
		$("#Manual_SC_Panel").fadeIn();
	})
	$("#Automatic_SC").click(function(){
		$("#Manual_SC_Panel").fadeOut();
		$("#Automatic_SC_Panel").fadeIn();
		
	});
	
	$("#Auto_SC").fileinput({
		language:'zh',
		uploadUrl:"${pageContext.request.contextPath}/admin/doAdminHandleSCAUTO",
		uploadAsync:true,
		allowedFileExtensions: ['docx', 'txt', 'xlsx','doc','xls'],//接收的文件后缀
		showUpload: true, //是否显示上传按钮
		showPreview: true,
        showCaption: true,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式 
        maxFileCount: 10, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        textEncoding:'UTF-8',
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        initialPreviewAsData: true,
        uploadExtraData: function () {   //额外参数的关键点
        	
            var obj = {courseId:$("#sc_courseId_Auto").val()};
            console.log(obj);
            return obj;
        }
	}).on("fileuploaded", function (event, data){
		var result = data.response; //后台返回的json
		if(result){
			
		}
	});;
	
});



</script>
</html>