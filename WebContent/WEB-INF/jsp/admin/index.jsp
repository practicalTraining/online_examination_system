<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../common/jscss.jsp" %>
<%-- ${pageContext.request.contextPath} --%>
<title>index</title>
<script type="text/javascript">
	$(document).ready(function(){
		var winHeight=$(document).outerHeight()/2 - $("#div_login").height();
		$("#div_login").css("margin-top",winHeight);
	});
	
</script>

<style type="text/css">
body{
	font-family: 'Droid Sans', sans-serif;
	font-size:100%;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
	background-size: cover;
	background-color:#eee;
}
</style>
</head>
<body >
<div class="container-fuild">
	<div class="row">
		<div id="div_login" class="col-md-3 col-md-offset-4" >
			<form  role="form" action="login" method="post">
					<div class="form-group">
						 <label for="adminName">管理员名称</label>
						 <input type="text" class="form-control" id="adminName" name="adminName"/>
					</div>
					<div class="form-group">
						 <label for="adminPass">管理员密码</label>
						 <input type="password" class="form-control" id="adminPass" name="adminPass" />
					</div>
					
					<button type="submit" class="btn btn-default">登陆</button>
					<button type="reset" class="btn btn-default">重置</button>
			</form>
		
		</div>	
	</div>
</div>		
</body>
</html>