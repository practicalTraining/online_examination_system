<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../common/jscssAdmin.jsp" %>
<%-- ${pageContext.request.contextPath} --%>
<title>管理员首页</title>
</head>
<body>
	<div class="container-fluid">
	<div class="row clearfix">
		<%@ include file="../common/adminNavTop.jsp" %>
		
		
		
		<div class="col-md-10 col-md-offset-2 padding_right"  style="font-size:12px;" >
				<ul class="ul_dashbord">
					<li class="li_dashbord" style="background-color:rgb(104,184,40)">
					
						<div class="li_title">
						<h3>老师人数</h3>
						</div>
						
						<div class="li_content">
							<i class="glyphicon glyphicon-user"></i>
						</div>
						
						<div class="li_foot"><a> More info</a></div>
					</li>
				
					<li class="li_dashbord" style="background-color:rgb(198,55,56)">
						<div class="li_title">
						<h3>学生人数</h3>
						</div>
						
						<div class="li_content">
							<i class="glyphicon glyphicon-user"></i>
						</div>
						
						<div class="li_foot"><a> More info</a></div>
					</li>
					
					<li class="li_dashbord" style="background-color:rgb(247,170,71)">
						<div class="li_title">
						<h3>试卷数量</h3>
						</div>
						
						<div class="li_content">
							<i class="glyphicon glyphicon-user"></i>
						</div>
						
						<div class="li_foot"><a> More info</a></div>
					</li>
					<li class="li_dashbord" style="background-color:rgb(14,98,199)">
						<div class="li_title">
						<h3>题库信息</h3>
						</div>
						
						<div class="li_content">
							<i class="glyphicon glyphicon-user"></i>
						</div>
						
						<div class="li_foot"><a> More info</a></div>
					</li>
				
				</ul>
			
			</div>
		<%@ include file="../common/adminNavLeft.jsp" %>
	</div>
</div>		
</body>
</html>