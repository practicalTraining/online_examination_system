<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
<script type="text/javascript">
	$(document).ready(function(){
		button_color(".color_button");	
	});
	
	function submitExaminfo(examId){
		$.post("doAdminHandleExam",$("#examInfo").serialize(),function(date){
			if(data=="success"){
				alert("成功");
			}else{
				alert("失败");
			}
		});
	}

</script>
<style type="text/css">
a{
color:white;
}
.class_top{
margin-top:3%;
margin-left:5%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
}
.color_button:hover{
	color:white;
}
.button_margin{
margin:0px 5px;
}
.border_bottom_display{
border-bottom:1px solid #eee;
}

.navbar-form{
margin-top:0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">		
			<!-- 操作 -->
			<div class="col-md-5 " style="margin-top:2%;margin-left:2%">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5>考试</h5>
					</div>
					<div class="panel-body">
					<form action="doAdminHandleExam" method="post" id="examInfo">
					<div class="form-group">
						<label for="examName">	${!empty examId?'修改':'添加' }考试名称</label>
						<input id="examName" type="text" name="examName" value="${exam.examName }"  class="form-control" >
						<input id="examId" type="hidden" name="examId" value="${exam.examId}"  class="form-control" >
					</div>
					<div class="form-group">						
						<label for="examCourse">${!empty examId?'修改':'添加' }考试课程类型</label>					
							<select class="form-control" name="courseId" id="courseId">			
								<c:forEach items="${course }" var="c">							
									<option  value="${c.courseId }"  ${exam.courseId==c.courseId?'selected':'' }>${c.courseName }</option>									
								</c:forEach>
							</select>						
					</div>
					<div class="form-group">	
						<label for="examDesci">考试描述</label>
						<textarea  rows="4" cols="3" id="examDesci" name="examDesci" class="form-control">${exam.examDesci }</textarea>				
					</div>
					<div class="form-group">
						<label for="examDuration">考试时长</label>
						<input  class="form-control" type="text" name="examDuration" id="examDuration" value="${exam.examDuration }"> 
					</div>
					<div class="form-group">
						<label for="examDate">考试日期时间(yyyy-MM-dd HH:mm:ss)(例如:2018-03-03 10:00:00)</label>
						<input  class="form-control" type="datetime" name="examDate" id="examDate" 	value="<fmt:formatDate value='${exam.examDate }' pattern='yyyy-MM-dd HH:mm:ss'/>"	>
					</div>
						<button type="button" class="btn btn-default btn-success " onclick="">提交</button>
						</form>
					</div>
				</div>	
			</div>
			
				<div class="col-md-11 " style="margin-top:2%;margin-left:2%">
				</div>
		</div>
	</div>
</body>
</html>