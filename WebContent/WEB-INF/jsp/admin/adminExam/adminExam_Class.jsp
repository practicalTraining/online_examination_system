<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
    <div class="panel panel-default">
    
						<div class="panel-heading">							
							<h5>考试班级</h5>
						</div>
						<div class="panel-body border_bottom_display">
							<h4>考试课程：${classinfo.courseName }</h4>
						</div>
						<div class="panel-body border_bottom_display">
							<h5>可添加班级</h5>							
								<table  class="table table-hover ">
									<thead>
									<tr>
									<th>编号</th>
									<th>名称</th>
									<th>操作</th>
									</tr>
									</thead>
									<tbody>
									<c:if test="${!empty unAddClass }">
									<c:forEach items="${unAddClass}" var="c" varStatus="cc">
										<tr>
											<td>${cc.count}</td>
											<td>${c.className }</td>
											<td>
												<button class="btn btn-xs btn-success" onclick="addExam_Class('${param.examId}','${c.classId }')">添加考试</button>
											</td>
										</tr>
									</c:forEach>
									</c:if>
									</tbody>
								</table>						
						</div>
						<div class="panel-body">
							<h5>已添加班级</h5>
							<table  class="table table-hover ">
									<thead>
									<tr>
									<th>编号</th>
									<th>名称</th>
									<th>操作</th>
									</tr>
									</thead>
									<tbody>
								<c:forEach items="${existClass}" var="c" varStatus="cc">																							
										<tr>
											<td>${cc.count}</td>
											<td>${c.className }</td>
											<td>
												<a class="btn btn-xs btn-warning" onclick="delExam_Class('${param.examId}','${c.classId}')">取消考试</a>
											</td>
										</tr>								
									</c:forEach>
						</tbody>
						</table>
						
						</div>
						
					</div>