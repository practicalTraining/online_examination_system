<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
   <div class="panel panel-default">
						<c:if test="${epaperinfo !=null }">
						<div class="panel-heading">
							<h5>已添加试卷信息</h5>
						</div>
						<div class="panel-body">
								<table class="table table-hover "> 
									<thead>
										<tr>
											<th>试卷编号</th>
											<th>试卷名称</th>
											<th>试卷分值</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>${epaperinfo.epaper.epaperName}</td>
											<td>${epaperinfo.epaper.epaperScore}</td>
											<td>
												<button type="button" class="btn btn-xs btn-info">查看</button>
												<button type="button" class="btn btn-xs btn-danger" onclick="delExam_Epaper('${epaperinfo.epaper.epaperId }')">删除</button>
											</td>
										</tr>
									</tbody>
									</table>

						</div>
						</c:if>
							<c:if test="${epaperinfo ==null }">
							<div class="panel-heading">
							<h5>可添加试卷信息</h5>
							</div>
							<div class="panel-body">
								<table class="table table-hover "> 
									<thead>
										<tr>
											<th>试卷编号</th>
											<th>试卷名称</th>
											<th>试卷分值</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${CourseEpaperInfo.epaper }" var="ep" varStatus="epc">	
										<tr>
											<td>${epc.count }</td>
											<td>${ep.epaperName}</td>
											<td>${ep.epaperScore}</td>
											<td>
												<button class="btn btn-xs btn-info">查看</button>
												<button class="btn btn-xs btn-success" onclick="addExam_Epaper('${param.examId}','${ep.epaperId }')">添加</button>
											</td>
										</tr>
										</c:forEach>
									</tbody>
									</table>

						</div>
						</c:if>
					</div>