<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员考试首页</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
<script type="text/javascript">

$(document).ready(function(){
	button_color(".button_color");
})
	function handleExam(title,url){
		var index=  layer.open({
			type: 2,
			title:'查看'+title+'考试',
			content: url,
			area: ['70%', '70%'],
			maxmin: true
			});
		
		layer.full(index);	
	};
	
	function checkExam(title,url){	

	var index=  layer.open({
		type: 2,
		title:title,
		content: url,
		area: ['70%', '70%'],
		maxmin: true
		});
	
	layer.full(index);
	};
	
	


</script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../common/adminNavTop.jsp" %>
			<%@ include file="../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " >
					<div class="col-md-9" style="margin-top:2%">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5>图表</h5>
							</div>
							<div class="panel-body">
							
							</div>
						</div>
					</div>
					
					<div class="col-md-3" style="margin-top:2%">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5>操作</h5>
							</div>
							<div class="panel-body">
								<a class="btn btn-default btn-primary" onclick="handleExam('添加考试','adminHandleExam')">添加考试</a>
							</div>
						</div>
					</div>
			
			</div>
			
			
			<div  class="col-md-10 col-md-offset-2 " style="margin-top:2%">
				<div>
					<div class="col-md-9">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5>考试列表</h5>
							</div>
							<div class="panel-body">
								<table class="table table-hover "> 
									<thead>
										<tr>
											<th>考试编号</th>
											<th>考试名称</th>
											<th>考试时间</th>
											<th>考试时长</th>
											<th>考试所属课程</th>
											<th>考试状态</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
									<c:if test="${!empty exam }">
										<c:forEach items="${exam }" var="e" varStatus="ec">
											<tr>
											<td>${ec.count }</td>
											<td>${e.examName }</td>
											<td><fmt:formatDate value="${e.examDate }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td>${e.examDuration }</td>
											<td>${e.course.courseName }</td>
											<td>
											<c:if test="${e.examStatus== 0}">
											未开始考试
											</c:if>
											<c:if test="${e.examStatus== 2}">
											考试已结束
											</c:if>
											<c:if test="${e.examStatus==1}">
											正在进行考试
											</c:if>
											
											</td>
											<td>
												<a class="btn btn-xs btn-primary" onclick="checkExam('${e.examName }','checkExamDetail?examId=${e.examId }'+'&'+'courseId=${e.courseId }')">查看</a>
												<a   class="btn btn-xs btn-danger" onclick="checkExam('修改${e.examName }','adminHandleExam?examId=${e.examId}')">修改</a>
											
											</td>
											</tr>
										</c:forEach>
									</c:if>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5>课程列表</h5>
							</div>
							<div class="panel-body">
							<c:forEach items="${course }" var="c">
								<a class="btn btn-default button_color" style="margin-top:2%;margin-left:2%">${c.courseName}</a>
							</c:forEach>
							</div>
						</div>
					</div>
				</div>
			
			</div>
			</div>
			</div>
</body>
</html>