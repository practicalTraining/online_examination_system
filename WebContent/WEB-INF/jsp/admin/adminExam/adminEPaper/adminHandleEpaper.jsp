<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
		button_color(".color_button");
		$.post("adminHandleEpaper?courseId="+courseId,function(data){
			$("#exam_div").html(data);
		  });
		$.post("adminSubjectList?epaperId="+"${param.epaperId}",function(data){
			$("#subject_div").html(data);
		  });
		
	});
	
var courseId=${!empty param.epaperId?ep.courseId:"1"};

$(document).on("change",'select#epaperCourse',function(){
	courseId =$(this).val();	
	$("#exam_div").empty();
	$.post("adminHandleEpaper?courseId="+courseId,function(data){
		$("#exam_div").html(data);
	  });
	
	
	
});

function submitEpaperInfo(){
	$.post("doAdminHandleEpaper",$("#epaperInfo").serialize(),function(data){
		if(date ="success" ){
			
			alert("${!empty param.epaperId?'修改':'添加' }"+"试卷信息成功");
			$.post("adminHandleEpaper?courseId="+courseId,function(data){
				$("#exam_div").html(data);
			  });
			$.post("adminSubjectList?epaperId="+"${param.epaperId}",function(data){
				$("#subject_div").html(data);			
		 });

		}else{
			alert("${!empty param.epaperId?'修改':'添加' }"+"试卷信息失败");
		}
	});
}

function handleEpaper(title,url){	

	var index=  layer.open({
		type: 2,
		title:title,
		content: url+'&courseId='+courseId,
		area: ['70%', '70%'],
		maxmin: true,
		end:function(index){ 
			$.post("adminHandleEpaper?courseId="+courseId,function(data){
				$("#exam_div").html(data);
			  });
			$.post("adminSubjectList?epaperId="+"${param.epaperId}",function(data){
				$("#subject_div").html(data);
			  });
			
        	}
		});
	
		layer.full(index);
		
	};
	
	
	function delSubject(subjectId,equestionId,subjectTypeId){
		$.post(getContextPath()+"/admin/delEpaperSubject?subjectId="+subjectId+"&equestionId="+equestionId+"&subjectTypeId="+subjectTypeId,function(data){
				if(data == "success"){
					alert("删除成功");
					$.post("adminSubjectList?epaperId="+"${param.epaperId}",function(data){
						$("#subject_div").html(data);
					  });

				}else{
					alert("删除失败");
				}
		  });	
	}


</script>
<style type="text/css">
a{
color:white;
}
.class_top{
margin-top:3%;
margin-left:3%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
}
.color_button:hover{
	color:white;
}
.button_margin{
margin:0px 5px;
}
.border_bottom_display{
border-bottom:1px solid #eee;
}

.navbar-form{
margin-top:0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		<form action="doAdminHandleEpaper" method="post" id="epaperInfo">
		<div class="col-md-11"  style="margin-top:2%;margin-left:2%">
			<button class="btn btn-default btn-danger" type="button" onclick="submitEpaperInfo()"> 提交</button>
		</div>
			<!-- 操作 -->
			<div class="col-md-4 " style="margin-top:2%;margin-left:2%">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5>试卷</h5>
						<input type="hidden" name="epaperId" value="${empty param.epaperId?'0':param.epaperId }">
					</div>
					<div class="panel-body" >
						<label for="epaperName">	${!empty param.epaperId?'修改':'添加' }试卷名称</label>
						<input id="epaperName" type="text" name="epaperName" value="${ep.epaperName }"  class="form-control" required="required">
						
						
						<label for="epaperCourse">	${!empty param.epaperId?'修改':'添加' }试卷课程类型</label>
						<select id="epaperCourse" name="courseId" class="form-control">
							<c:forEach items="${course }" var="c" >
							 	<option value="${c.courseId }" ${ep.courseId==c.courseId?'selected':'' }>${c.courseName }</option>
							</c:forEach>
						</select>
						
						<label for="epaperScore">	${!empty param.epaperId?'修改':'添加' }试卷分值</label>
						<input id="epaperScore" type="text" name="epaperScore" value="${ep.epaperScore}" class="form-control" required="required">
						
					
					</div>
				</div>	
			</div>
			<div class="col-md-7 " style="margin-top:2%;">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5>${!empty param.epaperId?'修改':'添加' }试卷到考试</h5>
					</div>
					<div class="panel-body" id="exam_div">
	
						
						
					</div>
			</div>
			
		
			</div>	
			
				<div class="col-md-11 " style="margin-top:2%;margin-left:2%" id="subject_div">
					<!-- ajax导入数据 -->
				</div>
			
			
			
		</form>
		</div>
	</div>
</body>
</html>