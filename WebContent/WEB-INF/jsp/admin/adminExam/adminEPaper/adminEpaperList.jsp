<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 	
		
			<c:if test="${!empty Epaper}">
									<c:forEach items="${Epaper.list }" var="ep" varStatus="epc">
										<tr>
											<td>${epc.count }</td>
											<td>${ep.epaperName }</td>
											<td>${ep.course.courseName }</td>
											<td>
											<a class="btn btn-xs btn-primary">查看</a>
											<a class="btn btn-xs btn-danger" onclick="handleEpaper('修改试卷','adminHandleEpaper?epaperId=${ep.epaperId}')">修改</a>
											<a class="btn btn-xs btn-success" onclick="delEpaper(${ep.epaperId})">删除</a>
											</td>
										</tr>
										</c:forEach>
											</c:if>