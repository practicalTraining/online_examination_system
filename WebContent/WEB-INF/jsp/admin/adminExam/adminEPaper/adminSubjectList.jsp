<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

			<div >
					<div>
							<div class="alert alert-info">
								<strong>试卷总分：${ep.epaperScore }<br></strong>
  								<strong>单选题：${scn } 分数：${scScoreSUM}  <br></strong>
  								<strong>多选题：${mcn } 分数：${mcScoreSUM} <br></strong>
  								<strong>判断题：${jgn } 分数：${jgScoreSUM} <br></strong>
  								<strong>填空题：${fan } 分数：${faScoreSUM} <br></strong>
  								<strong>题目总分:${scScoreSUM+mcScoreSUM+jgScoreSUM+faScoreSUM }<br></strong>
							</div>		
							<c:if test="${ep.epaperScore == scScoreSUM+mcScoreSUM+jgScoreSUM+faScoreSUM}">
							<div class="alert alert-success">
  								<strong>试卷分数正常!</strong>适合添加！
							</div>	
							</c:if>	
							<c:if test="${ep.epaperScore < (scScoreSUM+mcScoreSUM+jgScoreSUM+faScoreSUM) || ep.epaperScore > (scScoreSUM+mcScoreSUM+jgScoreSUM+faScoreSUM) }">
							<div class="alert alert-danger">
  								<strong>请检查!</strong> 试卷总分值 和存在题目分数；
							</div>	
							</c:if>	
					</div>
				</div>

<div class="panel panel-default">
					<div class="panel-heading">
					<h5>${!empty param.epaperId?'修改':'添加' }试卷内容
					<span>
						<a class="btn btn-xs button_margin pull-right btn-success" role="button" onclick="handleEpaper('添加单选题','adminHandleEpaperSC?epaperId=${param.epaperId}')">添加单选题</a>
						<a class="btn btn-xs btn-info  button_margin pull-right" role="button">添加试卷内容</a>
					</span>
					</h5>						
						
					</div>
					<div class="panel-body">
					<div class="panel-body border_bottom_display">
							<h4 class=" border_bottom_display" style="padding:10px;">单选题</h4>
							<div class="pull-right"> 
								单选数量：${ scn }
							</div>
							<br>
							<div class="pull-right"> 
								单选分值：${scScoreSUM }
							</div>
							<c:if test="${!empty epsc }">
									<table class="table table-hover ">
									<thead>
									<tr>
										<th>编号</th>
										<th>所属题库</th>
										<th>单选题名称</th>
										<th>单选题选项</th>
										<th>正确答案</th>
										<th>分值</th>
										<th>操作</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach items="${epsc }" var="SC" varStatus="scc">
										<tr>
											<td>${scc.count }</td>
											<td>${SC.equestion.equestionName }</td>
											<td>${SC.singlechoice.scName }</td>
											<td>
											<xmp>A.${SC.singlechoice.optionA }</xmp>
											<xmp>B.${SC.singlechoice.optionB }</xmp>
											<xmp>C.${SC.singlechoice.optionC }</xmp>
											<xmp>D.${SC.singlechoice.optionD }</xmp>
											</td>
											<td>${SC.singlechoice.scResult }</td>
											<td>${SC.singlechoice.scScore }</td>
											<td>
											<button type="button" class="btn btn-xs btn-primary">查看</button>
											<button type="button" class="btn btn-xs btn-danger">修改</button>
											<button type="button" class="btn btn-xs btn-success" onclick="delSubject(${SC.singlechoice.scId},${SC.equestion.equestionId},1)" >删除</button>
											</td>
											
										</tr>							
										</c:forEach>
									</tbody>
								</table>
							</c:if>	
							<c:if test="${empty epsc }">
								暂无单选题，请添加！
							</c:if>
					</div>
					</div>
					</div>
					
					
					<!--多选题  -->
					<div class="panel panel-default">
					<div class="panel-heading">
					<h5>${!empty param.epaperId?'修改':'添加' }试卷内容
					<span>
						<a class="btn btn-xs button_margin pull-right btn-success" role="button" onclick="handleEpaper('添加多选题','adminHandleEpaperMC?epaperId=${param.epaperId}')">添加多选题</a>
						<a class="btn btn-xs btn-info  button_margin pull-right" role="button">添加试卷内容</a>
					</span>
					</h5>						
						
					</div>
					<div class="panel-body">
					<div class="panel-body border_bottom_display">
							<h4 class=" border_bottom_display" style="padding:10px;">多选题</h4>
							<c:if test="${!empty epmc }">
									<table class="table table-hover ">
									<thead>
									<tr>
										<th>编号</th>
										<th>所属题库</th>
										<th>多选题名称</th>
										<th>多选题选项</th>
										<th>正确答案</th>
										<th>分值</th>
										<th>操作</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach items="${epmc }" var="MC" varStatus="mcc">
										<tr>
											<td>${mcc.count }</td>
											<td>${MC.equestion.equestionName }</td>
											<td>${MC.multiplechoice.mcName }</td>
											<td>
											<xmp>A.${MC.multiplechoice.optionA }</xmp>
											<xmp>B.${MC.multiplechoice.optionB }</xmp>
											<xmp>C.${MC.multiplechoice.optionC }</xmp>
											<xmp>D.${MC.multiplechoice.optionD }</xmp>
											<xmp>E.${MC.multiplechoice.optionE }</xmp>
											</td>
											<td>
											<h4>
											1--${MC.multiplechoice.mcResult1 }
											</h4>
											<h4>
											2--${MC.multiplechoice.mcResult2 }
											</h4>
											<h4>
											3--${MC.multiplechoice.mcResult3 }
											</h4>
											</td>
											<td>${MC.multiplechoice.mcScore }</td>
											<td>
											<a type="button" class="btn btn-xs btn-primary">查看</a>
											<a type="button" class="btn btn-xs btn-danger">修改</a>
											<a type="button" class="btn btn-xs btn-success" onclick="delSubject(${MC.multiplechoice.mcId},${MC.equestion.equestionId},2)" >删除</a>
											</td>
											
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>	
							<c:if test="${empty epmc }">
								暂无多选题，请添加！
							</c:if>
					</div>
					</div>
					</div>
					
					
					<!-- 判断题 -->					
					<div class="panel panel-default">
					<div class="panel-heading">
					<h5>${!empty param.epaperId?'修改':'添加' }试卷内容
					<span>
						<a class="btn btn-xs button_margin pull-right btn-success" role="button" onclick="handleEpaper('添加判断题','adminHandleEpaperJG?epaperId=${param.epaperId}')">添加判断选题</a>
						<a class="btn btn-xs btn-info  button_margin pull-right" role="button">添加试卷内容</a>
					</span>
					</h5>						
					</div>
					<div class="panel-body">
					<div class="panel-body border_bottom_display">
							<h4 class=" border_bottom_display" style="padding:10px;">判断题</h4>
							<c:if test="${!empty epjg }">
									<table class="table table-hover ">
									<thead>
									<tr>
										<th>编号</th>
										<th>所属题库</th>
										<th>判断题名称</th>
										<th>判断题选项</th>
										<th>正确答案</th>
										<th>分值</th>
										<th>操作</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach items="${epjg }" var="JG" varStatus="jgc">
										<tr>
											<td>${jgc.count }</td>
											<td>${JG.equestion.equestionName }</td>
											<td>${JG.judgement.jgName }</td>
											<td>
											<xmp>A.正确</xmp>
											<xmp>B.错误</xmp>
											</td>
											<td>
											<h4>
											1--${JG.judgement.jgResult }
											</h4>
											</td>
											<td>${JG.judgement.jgScore }</td>
											<td>
											<a class="btn btn-xs btn-primary">查看</a>
											<a class="btn btn-xs btn-danger">修改</a>
											<a class="btn btn-xs btn-success" onclick="delSubject(${JG.judgement.jgId},${JG.equestion.equestionId},3)" >删除</a>
											</td>
											
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>	
							<c:if test="${empty epjg }">
								暂无判断题，请添加！
							</c:if>
					</div>
					</div>
					</div>
					
					<!-- 填空题 -->					
					<div class="panel panel-default">
					<div class="panel-heading">
					<h5>${!empty param.epaperId?'修改':'添加' }试卷内容
					<span>
						<a class="btn btn-xs button_margin pull-right btn-success" role="button" onclick="handleEpaper('添加填空题','adminHandleEpaperFA?epaperId=${param.epaperId}')">添加填空选题</a>
						<a class="btn btn-xs btn-info  button_margin pull-right" role="button">添加试卷内容</a>
					</span>
					</h5>						
					</div>
					<div class="panel-body">
					<div class="panel-body border_bottom_display">
							<h4 class=" border_bottom_display" style="padding:10px;">填空题</h4>
							<c:if test="${!empty epfa }">
									<table class="table table-hover ">
									<thead>
									<tr>
										<th>编号</th>
										<th>所属题库</th>
										<th>判断题名称</th>
										<th>正确答案</th>
										<th>分值</th>
										<th>操作</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach items="${epfa }" var="FA" varStatus="fac">
										<tr>
											<td>${fac.count }</td>
											<td>${FA.equestion.equestionName }</td>
											<td>${FA.fillanswer.faName }</td>
											<td>
											<h4>
											1--${FA.fillanswer.faResult1 }
											</h4>
											<h4>
											2(可以为空)--${FA.fillanswer.faResult2 }
											</h4>
											</td>
											<td>${FA.fillanswer.faScore }</td>
											<td>
											<a type="button" class="btn btn-xs btn-primary">查看</a>
											<a type="button" class="btn btn-xs btn-danger">修改</a>
											<a type="button" class="btn btn-xs btn-success" onclick="delSubject(${FA.fillanswer.faId},${FA.equestion.equestionId},4)" >删除</a>
											</td>
											
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:if>	
							<c:if test="${empty epfa }">
								暂无填空题，请添加！
							</c:if>
					</div>
					</div>
					</div>
					