<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
<script type="text/javascript">
	$(document).ready(function(){
		button_color(".color_button");	
		$.post("adminGetEpaperList",function(data){
			$("#epaperList").html(data)
		})
		
	});
	
	function handleEpaper(title,url){
		var index=  layer.open({
			type: 2,
			title:title,
			content: url,
			area: ['70%', '70%'],
			maxmin: true,
			end:function(index){
				$.post("adminGetEpaperList",function(data){
					$("#epaperList").html(data)
				})
			}
			});
		
		layer.full(index);	
		
	}
	
	function delEpaper(epaperId){
		$.post("adminDelEpaper",{epaperId:epaperId},function(data){
			if(data=="success"){
				alert("删除成功");
				history.go(0);
			}else{
				alert("删除失败")
			}
			
		});
		
	}
	

</script>
<style type="text/css">
.class_top{
margin-top:3%;
margin-left:5%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
margin-bottom: 3%;
}

.ul_li>li>button{
margin:5px 0px;
}
.border_bottom_display{
border-bottom:1px solid #eee;
}

.navbar-form{
margin-top:0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2" style="margin-top:2%">
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								课程列表	
							</div>
							<div class="panel-body">
								<ul class="ul_left_padding ul_li">
								<c:if test="${!empty course }">
									<c:forEach items="${course }" var="c" >
									<li>
									<a href="getAdminEpaperByCourseId?courseId=${c.courseId }" class="btn btn-default color_button">
										${c.courseName }
									</a>
									</li>
									</c:forEach>
								</c:if>
								</ul>
							</div>
						</div>
					</div>
					
						<div class="col-md-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								试卷列表	
								
							</div>
							<div class="panel-body border_bottom_display">					
									<button class="btn btn-default btn-primary pull-left" onclick="handleEpaper('添加试卷','adminHandleEpaper')" >添加试卷</button>
									
										<form class="navbar-form navbar-left" role="search" action="getAdminEpaperByKeyword">
											<div class="form-group">
											<input class="form-control " type="text" name="keyword" value="${param.keyword}"/>
										</div> <button type="submit" class="btn btn-default">Search</button>
										</form>
									
									<div class="dropdown pull-right">
									 <button id="dLabel" type="button" class="btn btn-default " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											   排列
											    <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu" aria-labelledby="dLabel">
											  	<c:if test="${param.keyword == null }">
											   		<li><a href="getAdminEpaperByCourseId?courseId=${param.courseId}&sort=1">按日期升序排列</a></li>
											   		<li><a href="getAdminEpaperByCourseId?courseId=${param.courseId}&sort=2">按日期降序排列</a></li>
											   	</c:if>
											   	<c:if test="${param.keyword != null }">
											   		<li><a href="getAdminEpaperByKeyword?keyword=${param.keyword}&sort=1">按日期升序排列</a></li>
											   		<li><a href="getAdminEpaperByKeyword?keyword=${param.keyword}&sort=2">按日期降序排列</a></li>
											   	</c:if>
											  </ul>
									</div>
							</div>
							<div class="panel-body">
						
								
								<table class="table table-hover ">
									<thead>
									<tr>
										<th>编号</th>
										<th>试卷名称</th>
										<th>所属课程</th>
										<th>操作</th>
									</tr>
									</thead>
									<tbody id="epaperList">
							
									<%@ include file="../../../common/pageList.jsp" %>
									</tbody>
								</table>
							
						
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</body>
</html>