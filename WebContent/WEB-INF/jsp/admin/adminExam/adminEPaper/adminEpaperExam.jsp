<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
					<c:if test="${!empty exam }">
						<label for="examId">考试信息</label>
						<select id="examId" name="examId" class="form-control">
							<option value="0">暂时不添加考试</option>
							<c:forEach items="${exam }" var="e" >
							 	<option value="${e.examId }">${e.examName }</option>
							</c:forEach>
						</select>
						</c:if>
						<c:if test="${empty exam }">
							<h5>暂时没有考试相关信息；</h5>
						</c:if>