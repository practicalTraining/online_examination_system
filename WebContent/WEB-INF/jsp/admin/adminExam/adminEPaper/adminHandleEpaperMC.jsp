<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
<script type="text/javascript">
	$(document).ready(function(){
		button_color(".color_button");	
	});
	


</script>
<style type="text/css">
a{
color:white;
}
.class_top{
margin-top:3%;
margin-left:5%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
}
.color_button:hover{
	color:white;
}
.button_margin{
margin:0px 5px;
}
.border_bottom_display{
border-bottom:1px solid #eee;
}

.navbar-form{
margin-top:0px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	//获取未添加的多选题		

	$.post(getContextPath()+"/admin/adminHandleUnAddMc",{courseId:"${param.courseId }",epaperId:"${param.epaperId }"},function(data){
		$("#UnAddMcList").html(data);
	});

})

	//添加题目
	function addSubject(equestionId,subjectId){
		$.post(getContextPath()+"/admin/doAdminHandleEpaperMC",{equestionId:equestionId,subjectId:subjectId,epaperId:"${param.epaperId }" },function(data){
			if(data =="success"){
				alert("添加成功")
				$.post(getContextPath()+"/admin/adminHandleUnAddMc",{courseId:"${param.courseId }",epaperId:"${param.epaperId }"},function(data){
					$("#UnAddMcList").html(data);
				});

			}else{
				alert("添加失败")
			}
		});
	}
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">		
			<!-- 操作 -->
			<div class="col-md-11 " style="margin-top:2%;margin-left:2%">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5>试卷</h5>
					</div>
					<div class="panel-body">
						<label for="epaperName">	${!empty epaperId?'修改':'添加' }试卷名称</label>
						<input id="epaperName" type="text" name="epaperName" value="${ep.epaperName }"  class="form-control" readonly="readonly">						
						<label for="epaperCourse">	${!empty epaperId?'修改':'添加' }试卷课程类型</label>
						<input type="text" id="epaperCourse" name="courseName" class="form-control" value="${ep.course.courseName }" readonly="readonly" >					
					</div>
				</div>	
			</div>
			
				<div class="col-md-11 " style="margin-top:2%;margin-left:2%;margin-bottom:50%">
					<div class="panel panel-default">
					<div class="panel-heading">
					<h5>多选题
					</h5>		
					</div>
					<div class="panel-body border_bottom_display">
						<form class="navbar-form navbar-right" role="search" action="">
					<div class="form-group">
						<input class="form-control " type="text" name="keyword" value="${param.keyword}"/>
						</div> <button type="submit" class="btn btn-default">Search</button>
					</form>
									
					<div class="dropdown pull-right">
					 <button id="dLabel" type="button" class="btn btn-default " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								 排列
							<span class="caret"></span>
							</button>
							 <ul class="dropdown-menu" aria-labelledby="dLabel">
								<c:if test="${param.keyword == null }">
									<li><a href="adminEpaperSC?couresId=${param.courseId}&sort=1">按日期升序排列</a></li>
									<li><a href="adminEpaperSC?couresId=${param.courseId}&sort=2">按日期降序排列</a></li>
								</c:if>
								<c:if test="${param.keyword != null }">
									<li><a href="getAdminEpaperSCByKeyword?keyword=${param.keyword}&sort=1">按日期升序排列</a></li>
									<li><a href="getAdminEpaperSCByKeyword?keyword=${param.keyword}&sort=2">按日期降序排列</a></li>
								</c:if>
							 </ul>
						</div>			
					</div>
					<div class="panel-body">

							<div class="col-md-12">
							<div class="panel panel-default">
							<div class="panel-body border_bottom_display ">
							<h4></h4>
							</div>

							<div class="panel-body " id="UnAddMcList">
							</div>

							</div>
							</div>				
					</div>
					</div>
				</div>
			
			
			
		
		</div>
	</div>
</body>
</html>