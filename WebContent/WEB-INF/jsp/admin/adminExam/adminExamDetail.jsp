<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>

<style type="text/css">
a{
color:white;
}
.class_top{
margin-top:3%;
margin-left:3%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
}
.color_button:hover{
	color:white;
}
.button_margin{
margin:0px 5px;
}
.border_bottom_display{
border-bottom:1px solid #eee;
}

.navbar-form{
margin-top:0px;
}
</style>

<script type="text/javascript">

$(document).ready(function(){
	$.post("getAdminExam_Class",{examId:"${param.examId}",courseId:"${param.courseId}"},function(result){
		$("#exam_class").html(result);
	})
	
	$.post("getAdminExam_Epaper",{examId:"${param.examId}",courseId:"${param.courseId}"},function(result){
		$("#exam_epaper").html(result);
	})
})
	function delExam_Class(examId,classId){
		$.post("adminDelExam_Class",{examId:examId,classId:classId,courseId:"${param.courseId}"},function(data){
			if(data =="success"){
				$.post("getAdminExam_Class",{examId:examId,courseId:"${param.courseId}"},function(result){
					$("#exam_class").html(result);
				})
				alert("取消考试成功")
			}else{
				alert("取消考试失败")
			}
		})
		
	}
	
	function delExam_Epaper(epaperId){
			$.post("adminDelExam_Epaper",{epaperId:epaperId},function(data){
				if(data =="success"){
					$.post("getAdminExam_Epaper",{examId:"${param.examId}",courseId:"${param.courseId}"},function(result){
						$("#exam_epaper").html(result);
					})
					alert("删除试卷成功")
				}else{
					alert("删除试卷失败")
				}
			})
		
	}
	
	
	function addExam_Class(examId,classId){
		$.post("adminHandleExam_Class",{examId:examId,classId:classId,courseId:"${param.courseId}"},function(data){
			$.post("getAdminExam_Class",{examId:"${param.examId}",courseId:"${param.courseId}"},function(result){
				$("#exam_class").html(result);
			})
		})
	}
	
	function addExam_Epaper(examId,epaperId){
		 $.post("adminHandleExam_Epaper",{examId:examId,epaperId:epaperId},function(data){
			 if(data == 'success'){
				 alert("添加成功")
				 	$.post("getAdminExam_Epaper",{examId:"${param.examId}",courseId:"${param.courseId}"},function(result){
						$("#exam_epaper").html(result);
					})
			 }else{
				 alert("添加失败")
			 }
		 })
	}
	
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-11" style="margin-top:2%;margin-left:2%" >
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
						<h5>考试信息</h5>
						</div>
						<div class="panel-body">
							<h5>考试名称：${examinfo.examName }</h5>
							<h5>考试时间：<fmt:formatDate value="${examinfo.examDate }" pattern="yyyy-MM-dd HH:mm:ss"/></h5>
							<h5>考试时长：${examinfo.examDuration }</h5>
							<h5>考试状态：	<c:if test="${examinfo.examStatus== 0}">
											未开始考试
											</c:if>
											<c:if test="${examinfo.examStatus== 2}">
											考试已结束
											</c:if>
											<c:if test="${examinfo.examStatus==1}">
											正在进行考试
											</c:if></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="exam_class">
				
				</div>
				
				<div class="col-md-12" style="margin-top:2%;" id="exam_epaper">
				
				</div>
			</div>
		</div>
	</div>
</body>
</html>