<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

	<div class="col-md-8" >
						<div class="panel panel-default">
					<div class="panel-heading">
						<h5>
							题库填空题列表
							<span>
							<a class="btn btn-xs btn-success margin_left_a">查看统计信息</a>
							<a class="btn btn-xs btn-warning margin_left_a">添加填空题信息</a>	
							</span>
						</h5>
					</div>
					<div class=" panel-body">
						<table class="table table-hover ">
						  <thead>
						    <tr>
						      <th>名称</th>
						      <th>加入时间</th>
						      <th>所属课程</th>
						      <th>从题库中删除</th>
						    </tr> 
						  </thead>
						  <tbody>
						  <c:if test="${!empty eqallfa}">
						  <c:forEach items="${eqallfa }" var="ef" varStatus="efl">
						  <c:forEach items="${ef.fillanswer }" var="effa"  varStatus="eflfa">
						   <tr>
						      <td>${eflfa.count}</td>
						      <td>${effa.faName}</td>
						      <td><fmt:formatDate value='${effa.fa_eq.cTime}' pattern='yyyy-MM-dd HH:mm:ss'/></td>
						      <td>${ef.course.courseName}</td>
						      <td><button class="btn btn-xs btn-danger" onclick="delFaInEQ(${effa.faId},${param.equestionId})">删除</button></td>
						    </tr>
						    </c:forEach>
						    </c:forEach>
						    </c:if>
						    <c:forEach items="${eqallfa }" var="ef" varStatus="efl">
						    <c:if test="${empty ef.fillanswer }">
						    	<tr>
							    	<td>
							    	暂无相关信息
							    	</td>
						    	</tr>
						    </c:if>
						    </c:forEach>
						  </tbody>
						</table>
					</div>
				</div>
			
			</div>