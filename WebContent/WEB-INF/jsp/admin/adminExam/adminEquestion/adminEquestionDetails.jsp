<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>

<style type="text/css">
.class_top{
margin-top:3%;
margin-left:3%;
}

.ul_left_padding{
padding-left:0px;
}
.ul_li{

}
.ul_li>li{
float:left;
margin-right:3%;
}
.margin_left_a{
float:right;
margin-right:2%;
}

</style>

<script type="text/javascript">

$(document).ready(function(){
	$.post("getEQSCList",{equestionId:"${param.equestionId}"},function(data){
		$("#scList").html(data);
	});
	$.post("getEQMCList",{equestionId:"${param.equestionId}"},function(data){
		$("#mcList").html(data);
	});
	$.post("getEQFAList",{equestionId:"${param.equestionId}"},function(data){
		$("#faList").html(data);
	});
	$.post("getEQJGList",{equestionId:"${param.equestionId}"},function(data){
		$("#jgList").html(data);
	});
});
	
	function delScInEQ(scId,eqId){
		$.post("delScInEQ",{scId:scId,equestionId:eqId},function(data){
			if(data ="success"){				
				$.post("getEQSCList",{equestionId:"${param.equestionId}"},function(data){
					$("#scList").html(data);
				})
			}else{
				alert("删除失败")
			}
			
		});
	}
	function delMcInEQ(mcId,eqId){
		$.post("delMcInEQ",{mcId:mcId,equestionId:eqId},function(data){
			if(data ="success"){				
				$.post("getEQMCList",{equestionId:"${param.equestionId}"},function(data){
					$("#mcList").html(data);
				})
			}else{
				alert("删除失败")
			}
		
		});
	}
	function delFaInEQ(faId,eqId){
		$.post("delFaInEQ",{faId:faId,equestionId:eqId},function(data){
			if(data ="success"){				
				$.post("getEQFAList",{equestionId:"${param.equestionId}"},function(data){
					$("#faList").html(data);
				})
			}else{
				alert("删除失败")
			}
		
		});
	}
	function delJgInEQ(jgId,eqId){
		$.post("delJgInEQ",{jgId:jgId,equestionId:eqId},function(data){
			if(data ="success"){				
				$.post("getEQJGList",{equestionId:"${param.equestionId}"},function(data){
					$("#jgList").html(data);
				})
			}else{
				alert("删除失败")
			}

		});
	}
</script>
</head>
<body>
	<div class="container-fluid class_top">
		<div class="row" >
			<div class="col-md-4"  >
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5>可办事项 </h5>
					</div>
					<div class=" panel-body">
						<ul class="ul_left_padding ul_li">
							<li><button class="btn btn-default">添加内容</button></li>
							<li><button class="btn btn-default">查看题库信息</button></li>							
						</ul>
					</div>
				</div>
			</div>
			<div id="scList">
					
			</div>
		</div>
		<div class="row" >
			<div class="col-md-4" >
			</div>
			<div id="mcList">
			
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4" >
			</div>
			
			<div id="jgList">
			
			</div>
		</div>
		
				<div class="row">
			<div class="col-md-4" >
			</div>
			<div id="faList">
			
			</div>
		</div>
	</div>
</body>
</html>