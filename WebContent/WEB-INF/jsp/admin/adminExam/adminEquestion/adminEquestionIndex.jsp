<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/adminequestion.css">
<script type="text/javascript">
function click_eqName(title,url) {
		var index=  layer.open({
			type: 2,
			title:title,
			content: url,
			area: ['70%', '70%'],
			maxmin: true
			});
		
		layer.full(index);	
		
}

	$(document).ready(function(){
		li_color(".nav_ul")			
	});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		<%@ include file="../../../common/adminNavTop.jsp" %>
		<%@ include file="../../../common/adminNavLeft.jsp" %>
		<div class="col-md-10 col-md-offset-2">
			
			<div class="col-md-2 col-md-offset-10"   >
				<div class="btn-group btn-group-sm"  >
				 <button class="btn btn-default" type="button"><em class="glyphicon glyphicon-align-left"></em> 左</button> 
				 <button class="btn btn-default" type="button"><em class="glyphicon glyphicon-align-center"></em> 中</button> 
				 <button class="btn btn-default" type="button"><em class="glyphicon glyphicon-align-right"></em> 右</button>
				</div>	
			
			</div>
		</div>
	</div>
	<div class="row">		
			<div class="col-md-10 col-md-offset-2"  id="nav_ul_div">
					 <ul class="nav_ul">
					 	<li>
					 		<a>
					 			课程
					 		</a>
					 		<p>
					 			题库数量
					 		</p>
					 	</li>
					 	
					 	<li>
					 	 	<a>
					 			1
					 		</a>
					 	</li>
					 	<li>
					 		<a>
					 			1
					 		</a>
					 	</li>
					 	<li>
					 		<a>
					 			1
					 		</a>
					 	</li>
					 </ul>
				</div>
</div>

	<div class="row">			
			<div class="col-md-10 col-md-offset-2" >
				<div class="col-md-6">
				
				</div>
				
				
				<div class="col-md-5" id="equestion-panel">
					<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						题库
						<span><a href="adminHandleEQ" class="btn btn-sm btn-success" style="margin-left:80%">添加</a></span>
					</h3>
					
					
					</div>
					<div class="panel-body">
					<table class="table table-hover ">
						<thead >
						<tr>
							<th>
								编号
							</th>
							<th>
								名称
							</th>
							<th>
								所属课程
							</th>
							<th>
								操作
							</th>
						</tr>
					</thead>
					<tbody >
					<c:if test="${!empty EQ.list }">
					<c:forEach items="${EQ.list }" var="EQ" varStatus="e">
						<tr>
							<td>
								${e.count }
							</td>
							<td class="eqDetails_id">
							  <c:if test="${fn:length(EQ.equestionName)>15 }">
			                   <a href="javascript:void(0);"  onclick="click_eqName('${EQ.equestionName}','adminEquestionDetails?equestionId='+${EQ.equestionId}')"> ${fn:substring(EQ.equestionName, 0, 15)}... </a> 
			                   </c:if>
			                  <c:if test="${fn:length(EQ.equestionName)<=15 }">
			                    <a href="javascript:void(0);"  onclick="click_eqName('${EQ.equestionName}','adminEquestionDetails?equestionId=${EQ.equestionId}')" >    ${EQ.equestionName} </a>
			                   </c:if>
							</td>
							<td>							
								${EQ.course.courseName }							
							</td>
							
							<td> 
								<a class=" btn btn-xs btn-primary" href="adminHandleEQ?equestionId=${EQ.equestionId} ">修改</a>
								<a class=" btn btn-xs btn-danger" href="delEQ?equestionId=${EQ.equestionId}" onclick="return confirm('确定要删除这个题库吗？')">删除</a>
							</td>
						</tr>
						</c:forEach>
						</c:if>	
					</tbody>
				</table>
	<%-- <%@ include file="../../../common/pageList.jsp" %> --%>
			</div>
		</div>
	</div>
</div>
</div>			
</div>
</body>

</html>