<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 

	<div class="col-md-8">
						<div class="panel panel-default">
					<div class="panel-heading">
						<h5>
							题库单选题列表
							<span>
							<a class="btn btn-xs btn-success margin_left_a">查看统计信息</a>
							<a class="btn btn-xs btn-warning margin_left_a">添加单选题信息</a>	
							</span>
						</h5>
					</div>
					<div class=" panel-body">
						<table class="table table-hover ">
						  <thead>
						    <tr>
						      <th>编号</th>
						      <th>名称</th>
						      <th>加入时间</th>
						      <th>所属课程</th>
						      <th>从题库中删除</th>
						    </tr> 
						  </thead>
						  <tbody>
						  <c:if test="${!empty eqallsc }">
						  <c:forEach items="${eqallsc }" var="es" varStatus="esl">
						    <c:forEach items="${es.singlechoice }" var="essc"  varStatus="eslsc">
						     <tr>
						   		<td>${eslsc.count }</td>
					 		 	<td>${essc.scName}</td>
					 		 	<td><fmt:formatDate value='${essc.sc_eq.cTime}' pattern='yyyy-MM-dd HH:mm:ss'/></td>
					 		 	 <td>${es.course.courseName}</td>	
					 		 	 <td><button class="btn btn-xs btn-danger" onclick="delScInEQ(${essc.scId},${param.equestionId})">删除</button></td>
					 		 	</tr>
						      </c:forEach>						      			      					      
						    </c:forEach>
						    </c:if>
						  
						    <c:forEach items="${eqallsc }" var="es" varStatus="esl">
						      <c:if test="${empty es.singlechoice }">
						    	<tr>
							    	<td>
							    	暂无相关信息!
							    	</td>
						    	</tr>
						    	</c:if>
						    	</c:forEach>
						    
						  </tbody>
						</table>
					</div>
				</div>
			</div>