<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row"> 
			<%@ include file="../../../common/adminNavTop.jsp" %>
			<%@ include file="../../../common/adminNavLeft.jsp" %>
			<div class="col-md-10 col-md-offset-2 " style="margin-top:5%">
				<div class="col-md-6 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>${empty EQ.equestionId?'添加':'修改'}题库</h5>
						</div>
						<div class="panel-body">
							<form role="form" action="doAdminHandleEQ" method="post">
							<input type="hidden" value="${empty EQ.equestionId?'0':EQ.equestionId }" name="equestionId">
								<div class="form-group">
									<label for="eqName">题库名称</label>
									<input class="form-control" type="text" name="equestionName" id="eqName" value="${EQ.equestionName }"/>
								</div>
								<div class="form-group">
									<label for="courseId">题库所属课程</label>
									<select class="form-control" name="courseId" id="courseId">										
											<c:forEach items="${course}" var="eqc">
											<option  value="${eqc.courseId }"  ${EQ.courseId==eqc.courseId?'selected':'' }>${eqc.courseName }</option>
											</c:forEach>
									</select>
								</div>
								<button class="btn btn-default" type="submit">提交</button>	
								<button class="btn btn-default btn-primary" type="reset">重置</button>
							</form>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</body>
</html>