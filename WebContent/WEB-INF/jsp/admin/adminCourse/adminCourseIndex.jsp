<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/admincourse.css">
<script type="text/javascript">
	$(document).ready(function(){
		li_color(".course_f_ul");
	})
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../common/adminNavTop.jsp" %>
			<%@ include file="../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2" style="margin-top:5%">
			</div>
			
			<div class="col-md-10 col-md-offset-2">
				<div class="col-md-2 col-md-offset-1">
					<div class="course_f_div">
						<ul class="course_f_ul">
							<li><a href="adminHandleCourse">添加课程</a></li>
							<li><a >查看课程</a></li>
							<li><a >查看课程题库</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9 ">
						<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						课程
					</h3>
									
					</div>
					<div class="panel-body">
					<table class="table table-hover ">
						<thead >
						<tr>
							<th>
								编号
							</th>
							<th>
								名称
							</th>
							<th>
								操作
							</th>
						</tr>
					</thead>
					<tbody >
					<c:if test="${!empty course}">
					<c:forEach items="${course }" var="c" varStatus="e">
						<tr>
							<td>
								${e.count }
							</td>
							<td>							
								${c.courseName }							
							</td>
							<td> 
								<a class=" btn btn-xs btn-primary" href="adminHandleCourse?courseId=${c.courseId} ">修改</a>
								<a class=" btn btn-xs btn-danger" href="delCourse?courseId=${c.courseId}" onclick="return confirm('确定要删除这个课程吗？')">删除</a>
							</td>
						</tr>
						</c:forEach>
						</c:if>	
					</tbody>
				</table>
				<%-- <%@ include file="../../../common/pageList.jsp" %> --%>
			</div>
		</div>
				</div>
			</div>
			
		</div>
	</div>
</body>
</html>