<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../common/adminNavTop.jsp" %>
			<%@ include file="../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2">
				<div class="col-md-6 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>${empty course.courseId?'添加':'修改'}课程</h5>
						</div>
						<div class="panel-body">
							<form role="form" action="doAdminHandleCourse" method="post">
							<input type="hidden" value="${empty course.courseId?'0':course.courseId }" name="courseId">
								<div class="form-group">
									<label for="courseName">课程名称</label>
									<input class="form-control" type="text" name="courseName" id="courseName" value="${course.courseName }"/>
								</div>
								<button class="btn btn-default" type="submit">提交</button>	
								<button class="btn btn-default btn-primary" type="reset">重置</button>
							</form>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</body>
</html>