<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	button_color(".a_color");
});

	function classinfoDetail(title,url){
		var index =layer.open({
			type: 2,
			title:title,
			content: url,
			area: ['70%', '70%'],
			maxmin: true
			});
		
		layer.full(index);	
	}

</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../common/adminNavTop.jsp" %>
			<%@ include file="../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-10 col-md-offset-2 " style="margin-top:2%;" >
			<div class="col-md-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						课程
					</div>
					<c:if test="${!empty course }">					
					<ul class="list-group">
					<c:forEach items="${course }" var="c">					
	    				<li class="list-group-item">
	    				<a href="#" class="a_color btn btn-xs" style="color:white;text-decoration: none;" >${c.courseName }
	    				</a>
	    				</li>   			
					</c:forEach>
					</ul>						
					</c:if>
				</div>
			
			</div>
			
			<div class="col-md-9">
					<div class="panel panel-default">
					<div class="panel-heading">
						<span>
							班级
							<a href="adminHandleClass" class="btn btn-xs btn-danger pull-right">添加班级</a>
						</span>
					</div>
					
					<div class="panel-body ">
						<table class="table table-hover ">
						<thead >
						<tr>
							<th>
								编号
							</th>
							<th>
								名称
							</th>
							<th>
								班级负责老师
							</th>
							<th>
								操作
							</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty classinfo }">
							<c:forEach items="${classinfo }" var="ci" varStatus="cic"> 
								<tr>
									<td>${cic.count }</td>
									<td>${ci.className }</td>
									<td>${ci.counsellor.counsellorName }</td>
									<td>
									
										<a class="btn btn-xs btn-primary" onclick="classinfoDetail('查看班级信息','getAdminClassInfoDetail?classId='+${ci.classId})">查看</a>
										<a class="btn btn-xs btn-danger" >修改</a>
										<a class="btn btn-xs btn-success">删除</a>
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
						</table>
					</div>
					</div>
			</div>
				
				
					
				
			</div>
			</div>
		</div>
</body>
</html>