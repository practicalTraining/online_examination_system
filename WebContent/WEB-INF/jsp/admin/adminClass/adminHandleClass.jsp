<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="../../common/adminNavTop.jsp" %>
			<%@ include file="../../common/adminNavLeft.jsp" %>
			
			<div class="col-md-4 col-md-offset-4" style="margin-top:2%" >
					<h3>${!empty param.classId?'修改':'添加' }班级</h3>
					<form class="form" action="doAdminHandleClass" method="post">
					<input type="hidden" name="classId" value="${empty param.classId?'0':param.classId}">
						<div class="form-group">
							<label>${!empty param.classId?'修改':'添加' }班级名称</label>
							<input class="form-control" type="text" name="className" id="className" value="${classinfo.className }">
						</div>
						<div class="form-group">
							<label>${!empty param.classId?'修改':'添加' }班级辅导员</label>
							<select class="form-control"  name="counsellorId" id="counsellorId" >
									<c:forEach items="${counsellor }" var="c">
										<option value="${c.counsellorId }">${c.counsellorName }</option>
									</c:forEach>
							</select>
						</div>
						<button type="submit" class="btn btn-default btn-primary">添加</button>
					</form>
			
			</div>
			</div>
			</div>
</body>
</html>