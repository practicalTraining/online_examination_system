<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>

<script type="text/javascript">
function handleCourseToClass(title,url){
	var index = layer.open({
		type: 2,
		title:title,
		content: url,
		area: ['70%', '70%'],
		maxmin: true,
		success:function(result){	
		}
	
		});	

	layer.full(index);	
}

</script>
<style type="text/css">
.panel_thead{
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
 }
.border_bottom_display{
border-bottom:1px solid #eee;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">			
			<div class="col-md-11" style="margin-top:2%;margin-left:2%" >
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						班级信息
					</div>
					<div class="panel-body border_bottom_display">
						<h5>班级名称：${classinfo.className }</h5>
					</div>
					<div class="panel-body ">
						<h5>班级人数： ${fn:length(classinfo.student) }</h5>
					</div>
				</div>
			
			</div>
			
			<div class="col-md-8">
					<div class="panel panel-default">
					<div class="panel-heading">
						班级考试信息
					</div>
					<div class="panel-body">
					<table class="table table-hover ">
						<thead >
						<tr>
							<th>
								编号
							</th>
							<th>
								考试名称
							</th>
							<th>
								考试时间
							</th>
							<th>
								考试时长
							</th>
							<th>
								操作
							</th>
						</tr>
						</thead>
						<tbody>
							 <c:if test="${!empty examinfo }">
					 		<c:forEach items="${examinfo }" var="exami" varStatus="examic">
					 		<c:forEach items="${exami.exam }" var="e" varStatus="ei">
					 				<tr>
					 					<td>${ei.count }</td>
					 					<td>${e.examName }</td>
					 					<td><fmt:formatDate value='${e.examDate }' pattern='yyyy-MM-dd HH:mm:ss'/></td>
					 					<td>${e.examDuration}</td>
					 					<td><a class="btn btn-xs btn-info">查看</a></td>
					 				</tr>
					 			</c:forEach>
					 		</c:forEach>
					 		</c:if>
						</tbody>
						</table>					
					</div>
					</div>
			</div>				
			
			
			<div class="col-md-11" style="margin-top:2%;">
					<div class="panel panel-default">
					<div class="panel-heading">
						班级学生信息
					</div>					

					
					<div class="panel-body">
						<div class="col-md-3">
						<div class="panel panel-default">
						
						<table class="table table-hover ">
						<thead class="panel_thead" >
						<tr>
							<td>
								编号
							</td>
							<td>
								学生名称
							</td>
							<td>
								操作
							</td>
						</tr>
						</thead>
						<tbody>
							<c:forEach items="${classinfo.student }" var="cs" varStatus="csc">
								<tr>
									<td>${csc.count }</td>
									<td>${cs.studentName }</td>
									<td><a class="btn btn-xs btn-success">查看</a></td>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						</div>
					</div>
					</div>	
				</div>					
			</div>
			<div class="col-md-11" style="margin-top:2%;">
					<div class="panel panel-default">
					<div class="panel-heading">
						<span>班级课程信息
							<a id="addCourse" class="btn btn-xs btn-success pull-right" onclick="handleCourseToClass('添加课程','adminHandleCourseToClass?classId=${classinfo.classId}')">添加课程</a>
						</span>
					</div>					

					
					<div class="panel-body">
						<div class="col-md-3">
						<div class="panel panel-default">
						
						<table class="table table-hover ">
						<thead class="panel_thead" >
						<tr>
							<td>
								编号
							</td>
							<td>
								课程名称
							</td>
							<td>
								操作
							</td>
						</tr>
						</thead>
						<tbody>
							<c:forEach items="${classinfo.course }" var="cc" varStatus="ccc">
								<tr>
									<td>${ccc.count }</td>
									<td>${cc.courseName }</td>
									<td><a class="btn btn-xs btn-danger">查看</a></td>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						</div>
					</div>
					</div>
						
				</div>					
			</div>
			
			</div>		
			</div>
		</div>
</body>
</html>