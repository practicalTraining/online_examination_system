<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../common/jscssAdmin.jsp" %>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-11 " style="margin-top:2%;margin-left:2%">
				<div class="col-md-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>${classinfo.className }</h5>						
						</div>
						<div class="body">
						  	<h5>课程数量:${fn:length(ClassCourse)}</h5>
						</div>
					</div>
				</div>
				
				<div class="col-md-md-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>已添加课程列表</h5>						
						</div>
						<div class="body">
						 <table class="table table-hover ">
						<thead class="panel_thead" >
						<tr>
							<td>
								编号
							</td>
							<td>
								课程名称
							</td>
							<td>
								操作
							</td>
						</tr>
						</thead>
						<tbody>
							<c:forEach items="${course}" var="c" varStatus="cc">
							<c:set var="Continue" value="0"/> 
							<c:forEach items="${ClassCourse }" var="classc" varStatus="classcc">
							<c:if test="${Continue ==0 }">	
								<c:if test="${c.courseId == classc.courseId }">
								<c:set var="Continue" value="1"/> 
								<tr>
									<td>${cc.count }</td>
									<td>${c.courseName }</td>
									<td><a class="btn btn-xs btn-danger">查看</a></td>
								</tr>
								
								</c:if>
								</c:if>
							</c:forEach>
							</c:forEach>
						</tbody>
						</table>
						</div>
					</div>
				</div>
			
				
					<div class="col-md-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>可添加课程列表</h5>						
						</div>
						<div class="body">
						 <table class="table table-hover ">
						<thead class="panel_thead" >
						<tr>
							<td>
								编号
							</td>
							<td>
								课程名称
							</td>
							<td>
								操作
							</td>
						</tr>
						</thead>
						<tbody>
							<c:forEach items="${course}" var="c" varStatus="cc">
							<c:set var="Continue" value="0"/> 
							<c:if test="${!empty ClassCourse  }">
							<c:forEach items="${ClassCourse }" var="classc" varStatus="classcc">
							<c:if test="${Continue ==0 }">	
								<c:if test="${c.courseId != classc.courseId }">
								<c:set var="Continue" value="1"/> 
								<tr>
									<td>${cc.count }</td>
									<td>${c.courseName }</td>
									<td><a class="btn btn-xs btn-danger">添加</a></td>
								</tr>
								</c:if>
								</c:if>
							</c:forEach>
							</c:if>
							<c:if test="${empty ClassCourse  }">
								<tr>
									<td>${cc.count }</td>
									<td>${c.courseName }</td>
									<td><a class="btn btn-xs btn-danger" href="adminhandelClass_Course?courseId=${c.courseId }&classId=${classinfo.classId } ">添加</a></td>
								</tr>
							</c:if>
							</c:forEach>
						</tbody>
						</table>
						</div>
					</div>
				</div>
			
			</div>
		</div>
		</div>
</body>
</html>